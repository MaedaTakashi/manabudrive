﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Title
{
    public class OpeningManager : MonoBehaviour
    {

        #region Singleton
        private static OpeningManager instance;
        public static OpeningManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (OpeningManager)FindObjectOfType(typeof(OpeningManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        [SerializeField]
        private int NowImageIndex;

        public Sprite[] Sprites;
        public Image Image;
        public Button NextButton;

        void Start()
        {
            NowImageIndex = 0;
            SoundControl.PlayBGM("NISSAN_KIDSDRIVE_M01");
        }

        public void ClickButton(int value)
        {

            switch (value)
            {
                case 0:
                    Next();
                    break;
            }
        }
        
        private void Next()
        {
            if(NowImageIndex < Sprites.Length - 1)
            {
                NowImageIndex ++;
                Image.sprite = Sprites[NowImageIndex];
            }
            else
            {
                NextButton.interactable = false;
                GameManager.SaveData("Map");
                ScheneSwitcher.LoadScene("Map", 0.5f);
            }
        }

        void OnDestroy()
        {
            //Debug.Log("OnDestroy");
            Sprites = null;
            if (Image != null)
            {
                Image.sprite = null;
            }
        }
    }
}
