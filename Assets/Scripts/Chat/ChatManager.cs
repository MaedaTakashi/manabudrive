﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class ChatManager : MonoBehaviour
    {

        #region Singleton
        private static ChatManager instance;
        public static ChatManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (ChatManager)FindObjectOfType(typeof(ChatManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        public GameObject WebCamera;
        public TextMeshProUGUI txtTime;
        public Image ChatImage;
        public TextMeshProUGUI txtName;

        [SerializeField]
        private VideoPlayer videoPlayer;

        //[SerializeField]
        //private Sprite[] ChatSprites;

        private MapEventSpot spot;

        [SerializeField]
        private Button nexrButton;

        //[SerializeField]
        //private RenderTexture rt768;
        //[SerializeField]
        //private RenderTexture rt1536;
        [SerializeField]
        private RawImage rawImage;

        [SerializeField]
        private Text infoText;

        [SerializeField]
        private Image panelImage;

        private int spotID;
        void Start()
        {
            //videoPlayer.prepareCompleted += vpPrepareCompleted;
            //videoPlayer.loopPointReached += vpPlayEnd;
            //StartCoroutine(coStart());
        }


        IEnumerator coStart()
        {
            //Debug.Log("--SeachItem--");
            while(!MasterDataLoader.isLoaded)
            {
                //Debug.Log("LoadWait...");
                yield return null;
            }

            if (GameManager.ActiveMapEventSpot == null)
            {
                GameManager.ActiveMapEventSpot = MapEventSpot.Data[3];
            }

            spotID = GameManager.ActiveMapEventSpot.ID;
            spot = GameManager.ActiveMapEventSpot;
            Debug.Log("spotID:" + spotID);

            txtName.text = spot.TeacherName + "先生";
            txtName.enabled = true;

            if (spotID == 2)
            {
                nexrButton.gameObject.SetActive(false);
                RenderTexture rt = new RenderTexture(1536, 2048, 0);
                rawImage.texture = rt;
                videoPlayer.targetTexture = rt;
                videoPlayer.clip = Resources.Load<VideoClip>("Chat/cityguide_1536");
                videoPlayer.gameObject.SetActive(true);
                yield return null;
                videoPlayer.Prepare();
                rawImage.gameObject.SetActive(true);
            }
            //else if (spotID == 3)
            //{
            //    nexrButton.gameObject.SetActive(false);
            //    RenderTexture rt = new RenderTexture(768, 1024, 0);
            //    rawImage.texture = rt;
            //    videoPlayer.targetTexture = rt;
            //    videoPlayer.clip = Resources.Load<VideoClip>("Chat/cityguide_768");
            //    videoPlayer.gameObject.SetActive(true);
            //    yield return null;
            //    videoPlayer.Prepare();
            //    rawImage.gameObject.SetActive(true);
            //    string textStr = videoPlayer.clip.name + "を再生";
            //    infoText.text = textStr;
            //    DebugManager.PutLog(textStr);
            //}
            else
            {
                panelImage.gameObject.SetActive(true);
                string tmpPath = "Chat/Chat" + spot.SpotImageIndex.ToString("00");
                DebugManager.PutLog(tmpPath);
                ChatImage.sprite = Resources.Load<Sprite>(tmpPath);
                ChatImage.enabled = true;
                ChatImage.gameObject.SetActive(true);
            }

            if (spot.SpotKindCode == 3)
            {
                WebCamera.SetActive(true);
            }

        }

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    GotoNext();
                    break;
            }
        }

        private void GotoNext()
        {
            if (GameManager.ActiveMapEventSpot == null) return;

            switch (spot.SpotKindCode)
            {
                case 1:
                    GameManager.SaveData("ARKurabe");
                    ScheneSwitcher.LoadScene("ARKurabe",0.5f);
                    nexrButton.interactable = false;
                    break;

                case 2:
                    GameManager.SaveData("KokinSyasin");
                    ScheneSwitcher.LoadScene("KokinSyasin",0.5f);
                    nexrButton.interactable = false;
                    break;

                case 3:
                    GameManager.SaveData("KokinSyasin");
                    ScheneSwitcher.LoadScene("KokinSyasin");
                    nexrButton.interactable = false;
                    break;

                case 4:
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    nexrButton.interactable = false;
                    break;
            }
        }

        void vpPrepareCompleted(VideoPlayer vp)
        {
            Debug.Log("vpPrepareCompleted:");
            videoPlayer.Play();
        }

        void vpPlayEnd(VideoPlayer vp)
        {
            Debug.Log("vpPlayEnd:");
            //nexrButton.GetComponent<FadeEffect>().StartFadeIn();
            GotoNext();
        }

        void OnDestroy()
        {
            ChatImage = null;
        }
    }
}
