﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MonoSingleton<T> : MonoBehaviour where T :MonoSingleton<T>
{
	private static T _instance;

	public static T instance {
		get {
			if (_instance == null) {
				
				GameObject go = new GameObject (typeof(T).Name);
				_instance = go.AddComponent<T> ();
				DontDestroyOnLoad (go);
				_instance.OnInitialize ();
			}
			
			return _instance;
		}
	}

	protected virtual void OnInitialize ()
	{
	}
}