﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ManabuDriveApp.Common
{
    public class Animation_PlaySound : MonoBehaviour
    {
        public void PlaySound(int SoundIndex)
        {
            SoundControl.PlaySound(SoundIndex);
        }

        public void PlaySoundC1(int SoundIndex)
        {
            SoundControl.PlaySound(SoundIndex,1);
        }

        public void PlaySoundC2(int SoundIndex)
        {
            SoundControl.PlaySound(SoundIndex, 2);
        }

        public void PlaySoundC3(int SoundIndex)
        {
            SoundControl.PlaySound(SoundIndex, 3);
        }
    }
}
