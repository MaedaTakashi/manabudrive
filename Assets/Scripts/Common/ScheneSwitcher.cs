﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// シーン遷移
    /// </summary>
    public class ScheneSwitcher : SingletonMonoBehaviour<ScheneSwitcher>
    {
        [SerializeField]
        private FadeEffect fade;

        [SerializeField]
        private FadeEffect NowLoadingDispFade;

        private bool nowDoing = false;

        /// <summary>
        /// 画面遷移
        /// </summary>
        /// <param name='scene'>シーン名</param>
        public static void LoadScene(string sceneName, float fadeTime = -1f)
        {
            ScheneSwitcher.Instance._LoadScene(sceneName, fadeTime);
        }

        void _LoadScene(string scene, float fadeTime)
        {
            if (nowDoing) return;
            //if (IsSameScene(scene)) return;
            nowDoing = true;

            if (fadeTime > 0)
            {
                StartCoroutine(ScheneSwitcher.Instance.CoLoadScene(scene, fadeTime));
            }
            else
            {
                StartCoroutine(ScheneSwitcher.Instance.CoLoadSceneNoFade(scene));
            }
        }

        bool IsSameScene(string scene)
        {
            string now = SceneManager.GetActiveScene().name;
            if (now == scene)
            {
                Debug.Log("現在のシーンと同じ。(now : " + now + " select : " + scene + " )");
                return true;
            }

            return false;
        }

        IEnumerator CoLoadScene(string scene, float fadeTime)
        {
            //暗幕フェードイン開始
            //Debug.Log("暗幕フェードイン開始");
            if (fadeTime > 0)
            {
                NowLoadingDispFade.StartFadeIn();
                fade.StartFadeIn(fadeTime);
                yield return new WaitUntil(() => fade.State == FadeEffect.enState.End);
            }

            string tyuukan = "Empty";

            //中間シーン切替準備
            //Debug.Log("中間シーン切替準備");
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(tyuukan);
            asyncOperation.allowSceneActivation = false;

            while (asyncOperation.progress < 0.9f)
            {
                yield return null;
            }

            //中間シーン切替実行
            //Debug.Log("中間シーン切替実行");
            asyncOperation.allowSceneActivation = true;

            yield return null;

            //リソース解放
            //Debug.Log("リソース解放");
            Resources.UnloadUnusedAssets();
            yield return null;

            //シーン切替準備
            //Debug.Log("シーン切替準備");
            asyncOperation = SceneManager.LoadSceneAsync(scene);
            //Debug.Log("--シーン切替準備 1");
            asyncOperation.allowSceneActivation = false;
            //Debug.Log("--シーン切替準備 2");
            while (asyncOperation.progress < 0.9f)
            {
                yield return null;
            }

            //Debug.Log("リソース解放");
            Resources.UnloadUnusedAssets();
            yield return null;

            //シーン切替
            //Debug.Log("シーン切替");
            asyncOperation.allowSceneActivation = true;

            //暗幕フェードアウト開始
            //Debug.Log("暗幕フェードアウト開始");
            if (fadeTime > 0)
            {
                NowLoadingDispFade.StartFadeOut();
                fade.StartFadeOut(fadeTime);
                yield return new WaitUntil(() => fade.State == FadeEffect.enState.End);
            }

            nowDoing = false;

            GameManager.EventSystemEnable = true;
        }

        

        IEnumerator CoLoadSceneNoFade(string scene)
        {
            //シーン切替準備
            //Debug.Log("シーン切替準備");
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene);
            //Debug.Log("--シーン切替準備 1");
            asyncOperation.allowSceneActivation = false;
            //Debug.Log("--シーン切替準備 2");
            while (asyncOperation.progress < 0.9f)
            {
                yield return null;
            }

            //シーン切替
            //Debug.Log("シーン切替");
            asyncOperation.allowSceneActivation = true;

            yield return null;
            //Debug.Log("リソース解放");
            Resources.UnloadUnusedAssets();
            yield return null;
            nowDoing = false;

            GameManager.EventSystemEnable = true;
        }
    }
}