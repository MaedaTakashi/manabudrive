﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ManabuDriveApp.Common
{
    public class CameraImage : MonoBehaviour
    {

        private RawImage rawImg;
        private WebCamTexture m_webCamTexture = null;

        public int WebCamIndex = 0;
        //public bool IsSizeFromParent = true;

        void Start()
        {
            rawImg = this.GetComponent<RawImage>();
            rawImg.color = Color.white;
            rawImg.enabled = false;

            int width;
            int height;

            //if (IsSizeFromParent)
            //{
                RectTransform parentRectTran = this.transform.parent.GetComponent<RectTransform>();
                //Vector2 parentSize = parentRectTran.sizeDelta;
                Vector2 size = new Vector2(parentRectTran.rect.height, parentRectTran.rect.width);
                Debug.Log("size:" + size + " " + parentRectTran.rect + " " + parentRectTran.anchorMax + " " + parentRectTran.anchorMin);
                this.GetComponent<RectTransform>().sizeDelta = size;
                width = (int)size.x;
                height = (int)size.y;
            //}
            //else
            //{
            //    RectTransform rectTran = this.GetComponent<RectTransform>();
            //    Vector2 size = new Vector2(rectTran.rect.height, rectTran.rect.width);
            //    width = (int)size.x;
            //    height = (int)size.y;
            //}

            StartCoroutine(WebCamStart(width,height));
        }


        private IEnumerator WebCamStart(int width,int height)
        {
            yield return null;

            if (WebCamTexture.devices.Length == 0)
            {
                DebugManager.PutLog("カメラデバイスが見つからない");
                yield break;
            }

            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                DebugManager.PutLog("カメラ使用許可なし");
                yield break;
            }

            if (WebCamTexture.devices.Length < WebCamIndex + 1)
            {
                DebugManager.PutLog("対象カメラデバイス(index:" + WebCamIndex + ")が見つからない");
                yield break;
            }

            WebCamDevice userCameraDevice = WebCamTexture.devices[WebCamIndex];

            m_webCamTexture = new WebCamTexture(userCameraDevice.name, width, height);
            rawImg.texture = m_webCamTexture;

            DebugManager.PutLog("Webカメラ実行");
            m_webCamTexture.Play();
            StartCoroutine(waitWebCamFrame());
            yield return null;
        }


        //カメラの画像が読み込まれるのを待つ
        private IEnumerator waitWebCamFrame()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.2f);
                if (m_webCamTexture.isPlaying)
                {
                    if (m_webCamTexture.didUpdateThisFrame)
                    {
                        //rawImageの比率を修正
                        float width = m_webCamTexture.width;
                        float height = m_webCamTexture.height;

                        DebugManager.PutLog("Webカメラ サイズ w:" + width + " h:" + height);

                        RectTransform rawImageTransform = rawImg.GetComponent<RectTransform>();
                        Vector2 tmpSize = rawImageTransform.sizeDelta;
                        float rate = width / height;
                        tmpSize.x = tmpSize.y * rate;

                        DebugManager.PutLog("RawImage サイズ変更:" + tmpSize);

                        rawImageTransform.sizeDelta = tmpSize;
                        rawImg.enabled = true;
                        break;
                    }
                }
            }
        }



        public void WebCamStart()
        {
            if (m_webCamTexture)
            {
                m_webCamTexture.Play();
            }
        }

        public void WebCamStop()
        {
            if (m_webCamTexture)
            {
                if (m_webCamTexture.isPlaying)
                {
                    m_webCamTexture.Stop();
                }
            }
        }


        /// <summary>
        /// 再生/停止切り替え
        /// </summary>
        public void PlayStopToggle()
        {
            if (m_webCamTexture)
            {
                if (m_webCamTexture.isPlaying)
                {
                    m_webCamTexture.Stop();
                }
                else
                {
                    m_webCamTexture.Play();
                }
            }
        }
    }
}
