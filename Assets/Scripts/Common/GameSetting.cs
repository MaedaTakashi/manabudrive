﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [Serializable]
    public class GameSetting
    {
        const float SETTING_VER = 0.02f;
        public AnimationCurve[] Curve;
        public float EventLimitTime;
        public float SpotDispThreshold;
        public float SpotTyakusinThreshold;
        public float SpotReTyakusinTime;

        public int StopAutoUpdate;
        public float GPSY;
        public float GPSX;

        public void LoadSettingData()
        {
            float ver = PlayerPrefs.GetFloat("ver", 0f);
            if (ver != SETTING_VER)
            {
                PlayerPrefs.DeleteAll();
            }

            //DefaultUnitNum = PlayerPrefs.GetInt("DefaultUnitNum", 40);
            EventLimitTime = PlayerPrefs.GetFloat("EventLimitTime", 30f);
            SpotDispThreshold = PlayerPrefs.GetFloat("SpotDispThreshold", 0.01f);
            SpotTyakusinThreshold = PlayerPrefs.GetFloat("SpotTyakusinThreshold", 0.001f);
            SpotReTyakusinTime = PlayerPrefs.GetFloat("SpotReTyakusinTime", 10f);

            StopAutoUpdate = PlayerPrefs.GetInt("StopGPS", 0);
            Debug.Log("StopGPS:" + StopAutoUpdate);
            GPSY = PlayerPrefs.GetFloat("GPSY", 0f);
            GPSX = PlayerPrefs.GetFloat("GPSX", 0f);
        }

        public void SaveSettingData()
        {
            PlayerPrefs.SetFloat("ver", SETTING_VER);
            //PlayerPrefs.SetInt("DefaultUnitNum", DefaultUnitNum);
            PlayerPrefs.SetFloat("EventLimitTime", EventLimitTime);
            PlayerPrefs.SetFloat("SpotDispThreshold", SpotDispThreshold);
            PlayerPrefs.SetFloat("SpotTyakusinThreshold", SpotTyakusinThreshold);
            PlayerPrefs.SetFloat("SpotReTyakusinTime", SpotReTyakusinTime);
            Debug.Log("StopGPS:"+ StopAutoUpdate);
            PlayerPrefs.SetInt("StopGPS", StopAutoUpdate);
            PlayerPrefs.SetFloat("GPSY", GPSY);
            PlayerPrefs.SetFloat("GPSX", GPSX);
            PlayerPrefs.Save();
        }
    }
}
