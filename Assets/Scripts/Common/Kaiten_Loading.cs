﻿﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    public class Kaiten_Loading : MonoBehaviour
    {
        private float realTimeCount;

        void Start()
        {
            realTimeCount = Time.realtimeSinceStartup;
        }

        void Update()
        {
            //Debug.Log("rea:" + realTimeCount);
            Vector3 Angles = this.transform.rotation.eulerAngles;
            Angles.z = Angles.z + (Time.realtimeSinceStartup - realTimeCount) * 500f;
            //Angles.z = Angles.z + Time.deltaTime * 500f;
            this.transform.rotation = Quaternion.Euler(Angles);
            realTimeCount = Time.realtimeSinceStartup;
        }
    }
}