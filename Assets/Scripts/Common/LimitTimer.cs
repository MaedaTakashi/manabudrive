﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ManabuDriveApp.Common
{
    public class LimitTimer : MonoBehaviour
    {
        private Text txt;
        private int dispTime;
        private float timeLimit;

        public delegate void Callback(int value);
        Callback cb;

        void init()
        {
            txt = this.GetComponent<Text>();
        }

        public void TimerStart(float timeLimit, Callback callBack)
        {
            init();
            cb = callBack;
            this.timeLimit = timeLimit;
            timeDisp();
            this.gameObject.SetActive(true);
        }

        void Update()
        {
            if (cb == null) return;

            timeLimit -= Time.deltaTime;
            if (timeLimit > 0f)
            {
                //timeDisp();
            }
            else
            {
                timeLimit = 0f;
                //timeDisp();
                cb(1);
                cb = null;
            }

        }

        private void timeDisp()
        {
            int tmp = Mathf.CeilToInt(timeLimit);
            if (dispTime == tmp) return;
            dispTime = tmp;
            txt.text = dispTime.ToString();
        }

    }
}
