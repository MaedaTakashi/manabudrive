﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [System.Serializable]
    public class User
    {
        public int ID;
        public enum enGakunen { Non=0,Low=1,High=2}
        public enGakunen Gakunen;

        // 0:未発生　1：終了
        public int[] ClearFlgs = new int[20];

        // 0:未　1：完
        public int[] BookFlgs = new int[20];

        public DateTime StartDateTime;
        public DateTime EndDateTime;

        public bool[] RouteFlg = new bool[600];

        public User()
        {
            SetAllRouteFlg(false);
        }

        public void SetAllRouteFlg(bool value)
        {
            for (int i = 0; i < RouteFlg.Length; i++)
            {
                RouteFlg[i] = value;
            }
        }
    }
}
