﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [System.Serializable]
    public class PlayData
    {
        public List<User> Users;
        public int LastActiveUserID;
        public int LastActiveSpotIndex;
        public string LastScene;

        public float DemoTime;
        public bool IsDemoPlay;

        public PlayData()
        {
            Users = new List<User>();
        }

        internal void Clear()
        {
            Users.Clear();
            LastActiveUserID = -1;
            LastActiveSpotIndex = -1;
            LastScene = "Title";
            DemoTime = 0f;
            IsDemoPlay = false;
        }
    }
}
