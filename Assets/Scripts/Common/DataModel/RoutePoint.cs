﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [System.Serializable]
    public class RoutePoint
    {
        public static List<RoutePoint> Data = new List<RoutePoint>();
        public Vector2 GPSPos;
        public Vector2 EndGPSPos;
        public Vector2 ChkPointPos;

        public static void Create(string[] elements)
        {
            elements = MasterDataLoader.PadArray(elements,2);
            RoutePoint data = new RoutePoint(elements);
            Data.Add(data);
        }

        public RoutePoint(string[] elements)
        {
            GPSPos = new Vector2(float.Parse(elements[1]), float.Parse(elements[0]));
            EndGPSPos = new Vector2(float.Parse(elements[3]), float.Parse(elements[2]));
        }

    }
}
