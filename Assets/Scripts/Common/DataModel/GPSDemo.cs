﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [System.Serializable]
    public class GPSDemo
    {
        public static List<GPSDemo> Data = new List<GPSDemo>();
        public static double firstTimeStamp;
        public float timestamp;
        public float latitude;
        public float longitude;

        public static void Create(string[] elements)
        {
            elements = MasterDataLoader.PadArray(elements, 3);
            GPSDemo data = new GPSDemo();
            if(Data.Count == 0)
            {
                firstTimeStamp = double.Parse(elements[0]);
                data.timestamp = 0f;
            }
            else
            {
                data.timestamp = (float)(double.Parse(elements[0]) - firstTimeStamp);
            }

            //Debug.Log("data.timestamp:" + data.timestamp + " elements[0]:" + elements[0]);
            data.latitude = float.Parse(elements[1]);
            data.longitude = float.Parse(elements[2]);
            Data.Add(data);
        }
    }
}
