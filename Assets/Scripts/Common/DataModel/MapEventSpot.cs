﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManabuDriveApp.Common
{
    [System.Serializable]
    public class MapEventSpot
    {
        public static List<MapEventSpot> Data = new List<MapEventSpot>();
        public int ID;
        public Vector3 GPSPos;
        public String SpotName;
        public int SpotKindCode;
        public int SpotImageIndex;
        public String TeacherName;
        public String SpotText;
        public int BookIndex;
        public int CrearBookIndex;
        public int NIconColorIndex;
        public string TeacherKanaName;
        public int TyakusinImageIndex;

        public MapEventSpot(string[] elements)
        {
            ID = MasterDataLoader.ParseInt(elements[0], -1);
            SpotName = elements[1];
            GPSPos = new Vector3(float.Parse(elements[3]), float.Parse(elements[2]));
            SpotKindCode = MasterDataLoader.ParseInt(elements[4], -1);
            SpotImageIndex = MasterDataLoader.ParseInt(elements[5], -1);
            TeacherName = elements[6];
            SpotText = elements[7];
            BookIndex = MasterDataLoader.ParseInt(elements[8],-1);
            CrearBookIndex = MasterDataLoader.ParseInt(elements[9], -1);
            NIconColorIndex = MasterDataLoader.ParseInt(elements[10], -1);
            TeacherKanaName = elements[11];
            TyakusinImageIndex = MasterDataLoader.ParseInt(elements[12], -1);
        }


        public static void Create(string[] elements)
        {
            elements = MasterDataLoader.PadArray(elements,13);
            MapEventSpot data = new MapEventSpot(elements);
            Data.Add(data);
        }


    }
}
