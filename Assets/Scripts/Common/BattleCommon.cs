﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace ManabuDriveApp.Common
{
    public static class ConstColor {

        /// <summary>
        /// #FFFFFF形式の文字列から色を返す
        /// 不正な文字列の場合、白を返す
        /// </summary>
        /// <param name="hexColor"></param>
        /// <returns></returns>
        public static Color GetColorFromHTMLString(string hexColor)
        {
            Color color;
            if (ColorUtility.TryParseHtmlString(hexColor, out color))
            {
                return (color);
            }
            else
            {
                return (Color.white);
            }

        }


        ///// <summary>
        ///// uGUIのイベントが発生するかの確認
        ///// </summary>
        ///// <param name="_scrPos"></param>
        ///// <returns></returns>
        //public static bool IsUGUIHit(Vector3 _scrPos)
        //{
        //    PointerEventData pointer = new PointerEventData(EventSystem.current);
        //    pointer.position = _scrPos;
        //    List<RaycastResult> result = new List<RaycastResult>();
        //    EventSystem.current.RaycastAll(pointer, result);
        //    return (result.Count > 0);
        //}
    }
}
