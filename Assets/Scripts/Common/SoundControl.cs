﻿﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ManabuDriveApp.Common
{

    public class SoundControl : MonoBehaviour
    {

        private AudioSource BGMSource;
        private AudioSource[] SESources;
        List<AudioClip> clipList = new List<AudioClip>();

        #region Singleton
        private static SoundControl instance;
        //public static SoundControl Instance
        //{
        //    get { return instance; }
        //    //private set { instance = value; }
        //}

        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
                LoadSound();

                DontDestroyOnLoad(gameObject);
            }
            else
            {
                if (this != instance)
                {
                    Destroy(this.gameObject);
                    return;
                }
            }
        }
        #endregion Singleton


        private void LoadSound()
        {
            addSound("SE/SE01_TITLE", true);        //0
            addSound("SE/SE02_STARTBUTTON", true);  //1
            addSound("SE/SE04_FUKIDASHI", true);    //2
            addSound("SE/SE09_PYRAMID", true);      //3
            addSound("SE/SE10_METERUP", true);      //4
            addSound("SE/SE12_RAKUDA-FRAMEIN", true);   //5
            addSound("SE/SE13_RAKUDA-FRAMEOUT", true);  //6
            addSound("SE/SE14-01_MAP_TOUCH", true);     //7
            addSound("SE/SE14-02_MAP_TOUCH", true);     //8
            addSound("SE/SE13_MAP_TOUCH_RETURN", true); //9

            addSound("SE/SE07_AR_MATCHING", true);      //10
            addSound("SE/SE11-02_RAKUDA_TOUCH", true);  //11
            addSound("SE/SE11-03_RAKUDA_JUMP", true);   //12
            addSound("SE/SE05-04_PHONECALL", true);     //13
            addSound("SE/SE06_ANSWER_PHONECALL", true); //14
            addSound("SE/SE08_BOOK2", true);   //15
        }

        // Use this for initialization
        void Start()
        {
            BGMSource = transform.Find("BGM").GetComponent<AudioSource>();

            SESources = new AudioSource[4];
            for (int i = 0; i < 4; i++)
            {
                SESources[i] = transform.Find(
                    string.Format("SE{0:D2}", i)).GetComponent<AudioSource>();
            }
        }

        //一定時間おきにBluetoothスピーカに設定しに行き
        //失敗したら通常スピーカーに設定する
        float timeCount = 0f;
        void Update()
        {
            //timeCount += Time.deltaTime;
            //if (timeCount > 5f)
            //{
            //    SwitchSpeaker();
            //    timeCount = 0f;
            //}
        }

        [DllImport("__Internal")]
        private static extern bool SwitchSpeakerRoute(int mode);
        private void SwitchSpeaker()
        {
            //Debug.Log("SwitchSpeaker");
#if UNITY_IOS && !UNITY_EDITOR
            //iOS実機ではプラグインの使用
            bool rtn = SwitchSpeakerRoute(1);
            if(rtn){
            }else{
                rtn = SwitchSpeakerRoute(0);
            }
#else
            //iOS実機以外では素通り
#endif
        }


        //private List<int> lastCannel = new List<int>();
        //private int GetFreeChannel(int startIndex, int endIndex)
        //{
        //    for (int i = startIndex; i <= endIndex; i++)
        //    {
        //        if (!SESources[i].isPlaying)
        //        {
        //            return i;
        //        }
        //    }
        //    if (lastCannel.Count == 0) return 1;
        //    int last = lastCannel[0];
        //    lastCannel.RemoveAt(0);
        //    return last;
        //}

        //static public void PlaySoundFree(int soundIndex)
        //{
        //    if (instance == null) return;
        //    instance._PlaySoundFree(soundIndex);
        //}

        //private void _PlaySoundFree(int soundIndex)
        //{
        //    int channel = GetFreeChannel(2,3);
        //    //Debug.Log("PlaySoundFree " + soundIndex + " channel:" + channel);
        //    if (SESources[channel].isPlaying) SESources[channel].Stop();
        //    _PlaySound(musicMap[soundIndex], channel);
        //    lastCannel.Add(channel);
        //}


        static public void PlaySound(int soundIndex, int channel = 0, bool loop = false)
        {
            //Debug.Log("PlaySound:" + soundIndex + " channel:" + channel);
            if (instance == null) return;
            instance._PlaySound(soundIndex, channel,loop);
        }

        private void _PlaySound(int soundIndex, int channel,bool loop)
        {
            //Debug.Log("PlaySound:" + soundIndex + " channel:" + channel);
            if (SESources[channel].isPlaying) SESources[channel].Stop();
            _PlaySound(clipList[soundIndex], channel,loop);
        }

        static public void PlaySound(string soundName, int channel = 0)
        {
            if (instance == null) return;
            instance._PlaySound(soundName, channel);
        }

        private void _PlaySound(string soundName, int channel = 0)
        {
            if (SESources[channel].isPlaying) SESources[channel].Stop();
            AudioClip ac = Resources.Load<AudioClip>("Sounds/" + soundName);
            _PlaySound(ac, channel);
        }

        private void _PlaySound(AudioClip ac, int channel, bool loop = false)
        {
            SESources[channel].clip = ac;
            SESources[channel].loop = loop;
            SESources[channel].Play();
        }


        static public void PlaySoundOneShot(int soundIndex)
        {
            //Debug.Log("PlaySound:" + soundIndex + " channel:" + channel);
            if (instance == null) return;
            instance._PlaySoundOneShot(soundIndex);
        }

        private void _PlaySoundOneShot(int soundIndex)
        {
            SESources[0].PlayOneShot(clipList[soundIndex]);
        }


        static public void StopSound(int channel = 0)
        {
            if (instance == null) return;
            instance._StopSound(channel);
        }

        private void _StopSound(int channel)
        {
            SESources[channel].Stop();
        }



        //static public void PlayBGM(int soundIndex, bool loop = true)
        //{
        //    if (instance == null) return;
        //    instance._PlayBGM(soundIndex,loop);
        //}

        //private void _PlayBGM(int soundIndex, bool loop = true)
        //{
        //    if (BGMSource.isPlaying) BGMSource.Stop();
        //    BGMSource.clip = musicMap[soundIndex];
        //    BGMSource.loop = loop;
        //    BGMSource.Play();
        //}

        static public void PlayBGM(string BGMName, bool loop = true)
        {
            if (instance == null) return;
            instance._PlayBGM(BGMName, loop);
        }

        private void _PlayBGM(string BGMName, bool loop)
        {
            if (BGMSource.clip)
            {
                if (BGMSource.clip.name == BGMName)
                {
                    Debug.Log("同名BGM");
                    return;
                }
            }
            if (BGMSource.isPlaying) BGMSource.Stop();
            BGMSource.clip = Resources.Load<AudioClip>("Sounds/BGM/" + BGMName);
            BGMSource.loop = loop;
            BGMSource.Play();
            //Debug.Log("BGM:"+BGMSource.clip.name);
        }

        static public void StopBGM()
        {
            if (instance == null) return;
            instance.BGMSource.Stop();
        }

        private void addSound(string fileName,bool LoadNow)
        {
            AudioClip ac = Resources.Load<AudioClip>("Sounds/" + fileName);
            if (LoadNow) ac.LoadAudioData();
            clipList.Add(ac);
        }

        static public AudioSource GetBGMSource()
        {
            if (instance == null) return null;
            return instance.BGMSource;
        }

        static public AudioSource GetSESource(int index)
        {
            if (instance == null) return null;
            return instance.SESources[index];
        }

        static public void SetSEVol(int index,float volume)
        {
            if (instance == null) return;
            instance.SESources[index].volume = volume;
        }
    }
}