﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

namespace ManabuDriveApp.Common
{
    public class GameDataControl
    {

        [Serializable]
        public class StoreData
        {
            public string json;
            public string crc;
        }

        /// <summary>
        /// 指定されたオブジェクトを保存
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="fileName">File name.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void Save<T>(T data, string fileName)
        {
#if UNITY_IOS
		System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
#endif

            //Loadで呼び出し、GameControlのインスタンスに値を代入していない場合の対応
            //if (data is User)
            //{
            //    GameControl.control.user = data as User;
            //}

            string path = Path.Combine(Application.persistentDataPath, fileName);
            Debug.Log("Save: " + path);

            using (FileStream file = File.Open(path, FileMode.Create))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                StoreData storeData = new StoreData();
                storeData.json = JsonUtility.ToJson(data);
                storeData.crc = calcMd5(storeData.json + fileName);
                binaryFormatter.Serialize(file, storeData);
            }
        }

        /// <summary>
        /// 指定されたファイルの読み込み
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T Load<T>(string fileName)
        {
#if UNITY_IOS
		System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
#endif

            string path = Path.Combine(Application.persistentDataPath, fileName);
            Debug.Log("Load: " + path);

            if (!File.Exists(path))
            {
                Debug.LogWarning(fileName + " not found");
                return default(T);
            }

            using (FileStream file = File.Open(path, FileMode.Open))
            {
                try
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    StoreData storeData = (StoreData)binaryFormatter.Deserialize(file);
                    string crc = calcMd5(storeData.json + fileName);

                    if (!crc.Equals(storeData.crc))
                    {
                        Debug.LogWarning(fileName + " is broken!");
                        return default(T);
                    }

                    T data = JsonUtility.FromJson<T>(storeData.json);
                    return data;
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e);
                    return default(T);
                }
            }
        }

        /// <summary>
        /// 指定された文字列をMD5でハッシュ化
        /// </summary>
        /// <returns>The md5.</returns>
        /// <param name="srcStr">Source string.</param>
        private static string calcMd5(string str)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            // md5ハッシュ値を求める
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(str);
            byte[] hashBytes = md5.ComputeHash(bytes);

            // 求めたmd5値を文字列に変換する
            System.Text.StringBuilder stringBuilder;
            stringBuilder = new System.Text.StringBuilder();
            foreach (byte curByte in hashBytes)
            {
                stringBuilder.Append(curByte.ToString("x2"));
            }

            // 変換後の文字列を返す
            return stringBuilder.ToString();
        }
    }
}