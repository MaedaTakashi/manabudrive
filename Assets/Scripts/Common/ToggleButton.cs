﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace ManabuDriveApp.Common
{
    public class ToggleButton : Button
    {
        public Sprite OnSprite;
        public Sprite OffSprite;
        public bool isOn;

        protected override void Start()
        {
            base.Start();
            setSprite();
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            isOn = !isOn;
            setSprite();
            base.OnPointerClick(eventData);
        }

        private void setSprite()
        {
            if (isOn)
            {
                ((Image)targetGraphic).sprite = OnSprite;
            }
            else
            {
                ((Image)targetGraphic).sprite = OffSprite;
            }
        }
    }
}