﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ManabuDriveApp.Common
{
    public class Flick : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
    {
        [SerializeField]
        private UnityEngine.Events.UnityEvent m_events = new UnityEngine.Events.UnityEvent();

        [SerializeField]
        private Vector3 touchStartPos;
        [SerializeField]
        private Vector3 touchEndPos;
        private float timeCount;

        [SerializeField]
        float angle;

        [SerializeField]
        float mag;

        public void OnPointerDown(PointerEventData eventData)
        {
            timeCount = Time.realtimeSinceStartup;
            touchStartPos = eventData.position;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if(Time.realtimeSinceStartup < timeCount + 1f)
            {
                if (m_events != null)
                {
                    touchEndPos = eventData.position;
                    Vector2 tmpVec = touchEndPos - touchStartPos;
                    angle = Vector2.Angle(new Vector2(1f,0f), tmpVec);
                    if (angle < 30f)
                    {
                        mag = tmpVec.magnitude;
                        if (tmpVec.magnitude > 100f)
                        {
                            Debug.Log("test");
                            m_events.Invoke();
                        }
                    }
                }
            }
        }
    }
}
