﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

namespace ManabuDriveApp.Common
{
    public class GameManager : MonoBehaviour
    {
        #region Singleton
        private static GameManager instance;
        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (GameManager)FindObjectOfType(typeof(GameManager));
                    //if (instance == null)
                    //{
                    //    Debug.LogError(typeof(GameSetting) + "is nothing");
                    //}
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                //Debug.Log("GameManager Destroy");
                Destroy(this.gameObject);
                return;
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                GameStart();
            }
        }
        #endregion Singleton

        private Dictionary<int, User> dicUsers;

        public PlayData PlayData;


        [SerializeField]
        private User _ActiveUser;
        public static User ActiveUser
        {
            get
            {
                return instance._ActiveUser;
            }
        }

        //[SerializeField]
        private MapEventSpot _ActiveMapEventSpot;
        public static MapEventSpot ActiveMapEventSpot
        {
            set
            {
                instance._ActiveMapEventSpot = value;
            }
            get
            {
                return instance._ActiveMapEventSpot;
            }
        }


        //public float _DemoTime;
        public static float DemoTime
        {
            set {
                instance.PlayData.DemoTime = value;
            }
            get
            {
                return instance.PlayData.DemoTime;
            }
        }

        //public bool _IsDemoPlay;
        public static bool IsDemoPlay
        {
            set
            {
                instance.PlayData.IsDemoPlay = value;
            }
            get
            {
                return instance.PlayData.IsDemoPlay;
            }
        }

        private const string USER_DAT_FILE_NAME = "PlayData.dat";
        public GameSetting GameSetting;
        public static bool PlayDataIsLoaded;
        //[SerializeField]
        private EventSystem EventSys;


        /// <summary>
        /// ゲーム開始後ここを最初に通ること
        /// </summary>
        void GameStart()
        {
            EventSys = this.transform.Find("EventSystem").GetComponent<EventSystem>();

            GameSetting = new GameSetting();
            GameSetting.LoadSettingData();

            StartCoroutine(loadData());
            Debug.Log("GameStart");
        }


        IEnumerator loadData()
        {
            MasterDataLoader.Create();
            while (!MasterDataLoader.isLoaded)
            {
                //Debug.Log("csv読込待ち");
                yield return null;
            }


            dicUsers = new Dictionary<int, User>();
            PlayData = GameDataControl.Load<PlayData>(USER_DAT_FILE_NAME);
            if (PlayData == null)
            {
                PlayData = new PlayData();
            }
            else
            {
                foreach (User user in PlayData.Users)
                {
                    //Debug.Log(user);
                    dicUsers.Add(user.ID, user);
                    if(user.RouteFlg.Length != RoutePoint.Data.Count)
                    {
                        Array.Resize(ref user.RouteFlg, RoutePoint.Data.Count);
                    }
                }
            }

            if (PlayData.LastActiveSpotIndex > 0)
            {
                foreach(MapEventSpot spot in MapEventSpot.Data)
                {
                    if (PlayData.LastActiveSpotIndex == spot.ID)
                    {
                        _ActiveMapEventSpot = spot;
                        break;
                    }
                }
            }


            if (!dicUsers.TryGetValue(PlayData.LastActiveUserID, out _ActiveUser))
            {
                SetupNewUser();
                SaveData("Title");
            }
            //Debug.Log("laod  lastScene:" + instance.PlayData.LastScene);
            PlayDataIsLoaded = true;
        }


        /// <summary>
        /// 白紙のアクティブユーザー追加
        /// 10000までの番号でIDを付番しUsersに追加する
        /// </summary>
        public static void SetupNewUser()
        {
            instance._ActiveUser = new User();

            for(int newId = 1;newId<10000;newId++)
            {
                if (!instance.dicUsers.ContainsKey(newId))
                {
                    Debug.Log("NewUser ID:" + newId);
                    ActiveUser.ID = newId;
                    instance.dicUsers.Add(newId, ActiveUser);
                    instance.PlayData.Users.Add(ActiveUser);
                    ActiveUser.StartDateTime = DateTime.Now;
                    instance.PlayData.LastActiveUserID = newId;
                    return;
                }
            }
        }


        public static void SaveData(string lastScene = "")
        {
            if (ActiveMapEventSpot != null)
            {
                instance.PlayData.LastActiveSpotIndex = ActiveMapEventSpot.ID;
            }
            else
            {
                instance.PlayData.LastActiveSpotIndex = -1;
            }
            instance.PlayData.LastScene = lastScene;
            //Debug.Log("save  lastScene:" + lastScene);
            GameDataControl.Save(instance.PlayData, USER_DAT_FILE_NAME);
        }

        public static void ClearUsers()
        {
            instance._ActiveMapEventSpot = null;
            instance.PlayData.Clear();
        }


        //private MapEventSpot _ActiveMapEventSpot;
        public static bool EventSystemEnable
        {
            set
            {
                instance.EventSys.enabled = value;
            }
            get
            {
                return instance.EventSys.enabled;
            }
        }

        //public static void EventSystemEnable(bool value)
        //{
        //    instance.EventSys.enabled = value;
        //}
    }
}
