﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ManabuDriveApp.Battle
{
    /// <summary>
    /// 座標移動
    /// </summary>
    public class PosMove : MonoBehaviour
    {
        public enum enState { Non,Moving,End}
        public enState State = enState.Non;

        protected Transform thisTran;
        protected Vector2 movePos;

        protected Vector2 endPos;

        protected float moveTime;
        protected float timeCount;

        protected AnimationCurve moveCurve;

        protected void moveStartSubProc()
        {
            thisTran = this.transform;
            timeCount = 0;
            this.enabled = true;
            this.gameObject.SetActive(true);
            State = enState.Moving;
        }

        /// <summary>
        /// 開始
        /// </summary>
        /// <param name="movePos">1秒間に動く距離</param>
        public void MoveStart(Vector2 movePos)
        {
            moveStartSubProc();

            moveTime = 0;
            this.movePos = movePos;
            this.moveCurve = null;
        }

        public void MoveStart(Vector2 endPos, float moveTime)
        {
            moveStartSubProc();

            this.moveTime = moveTime;
            Vector2 startPos = thisTran.position;
            this.endPos = endPos;

            this.movePos = (endPos - startPos) / moveTime;
            this.moveCurve = null;
        }

        public void MoveStart(Vector2 startPos,Vector2 endPos,float moveTime)
        {
            moveStartSubProc();

            this.moveTime = moveTime;
            thisTran.position = startPos;
            this.endPos = endPos;

            this.movePos = (endPos - startPos)/moveTime;
            this.moveCurve = null;
        }

        public void MoveStart(Vector2 endPos, float moveTime, AnimationCurve moveCurve)
        {
            moveStartSubProc();

            this.moveTime = moveTime;
            Vector2 startPos = thisTran.position;
            this.endPos = endPos;

            this.movePos = endPos - startPos;
            this.moveCurve = moveCurve;
        }

        public void Stop()
        {
            State = enState.Non;
            this.enabled = false;
        }

        void Update()
        {
            if(State == enState.Moving)
            {
                if(moveTime == 0f)
                {
                    _moveProc();
                }
                else
                {
                    timeCount += Time.deltaTime;
                    if (timeCount < moveTime)
                    {
                        _moveProc();
                    }
                    else
                    {
                        thisTran.position = endPos;
                        State = enState.End;
                        this.enabled = false;
                    }
                }
            }
        }


        virtual protected void _moveProc()
        {
            Vector2 newPos = thisTran.position;
            if (moveCurve == null)
            {
                newPos += movePos * Time.deltaTime;
            }
            else
            {
                newPos = endPos - movePos * (1 - moveCurve.Evaluate(timeCount/moveTime));
            }
            //Debug.Log("movePos:" + movePos);
            thisTran.position = newPos;
        }

    }
}