﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// サイズ変化
    /// </summary>
    public class SizeMove : MonoBehaviour
    {
        public enum enState { Non, Moving , End }
        public enState State = enState.Non;

        public enum enMode { Nomal, Loop}
        public enMode Mode = enMode.Nomal;

        protected RectTransform thisRectTran;
        protected Vector2 moveSize;
        protected Vector2 endSize;

        protected float moveTime;
        protected float timeCount;

        protected AnimationCurve moveCurve;

        protected void moveStartSubProc()
        {
            thisRectTran = this.GetComponent<RectTransform>();
            timeCount = 0;
            this.enabled = true;
            this.gameObject.SetActive(true);
            State = enState.Moving;
        }


        public void MoveStart(Vector2 endSize, float moveTime, AnimationCurve moveCurve = null, enMode mode = enMode.Nomal)
        {
            moveStartSubProc();

            this.moveTime = moveTime;
            Vector2 startSize = thisRectTran.sizeDelta;
            this.endSize = endSize;

            this.moveSize = endSize - startSize;
            this.moveCurve = moveCurve;
            this.Mode = mode;
        }

        public void Stop()
        {
            State = enState.Non;
            this.enabled = false;
        }

        void Update()
        {
            if(State == enState.Moving)
            {
                if(moveTime == 0f)
                {
                    _moveProc();
                }
                else
                {
                    timeCount += Time.deltaTime;
                    if (timeCount < moveTime)
                    {
                        _moveProc();
                    }
                    else
                    {
                        thisRectTran.sizeDelta = endSize;
                        if(Mode == enMode.Loop)
                        {
                            timeCount = 0;
                            this.endSize -= this.moveSize;
                            this.moveSize *= -1f;
                        }
                        else
                        {
                            State = enState.End;
                            this.enabled = false;
                        }
                    }
                }
            }
        }


        virtual protected void _moveProc()
        {
            Vector2 newSize;
            if (moveCurve == null)
            {
                newSize = thisRectTran.sizeDelta;
                newSize += moveSize * Time.deltaTime / moveTime;
            }
            else
            {
                newSize = endSize - moveSize * (1 - moveCurve.Evaluate(timeCount/moveTime));
            }
            //Debug.Log("movePos:" + movePos);
            thisRectTran.sizeDelta = newSize;
        }




        ///// <summary>
        ///// 開始
        ///// </summary>
        ///// <param name="movePos">1秒間に動く距離</param>
        //public void MoveStart(Vector2 movePos)
        //{
        //    moveStartSubProc();

        //    moveTime = 0;
        //    this.moveSize = movePos;
        //    this.moveCurve = null;
        //}

        //public void MoveStart(Vector2 endSize, float moveTime)
        //{
        //    moveStartSubProc();

        //    this.moveTime = moveTime;
        //    Vector2 startPos = thisTran.position;
        //    this.endPos = endSize;

        //    this.moveSize = (endSize - startPos) / moveTime;
        //    this.moveCurve = null;
        //}

        //public void MoveStart(Vector2 startPos,Vector2 endPos,float moveTime)
        //{
        //    moveStartSubProc();

        //    this.moveTime = moveTime;
        //    thisTran.position = startPos;
        //    this.endPos = endPos;

        //    this.moveSize = (endPos - startPos)/moveTime;
        //    this.moveCurve = null;
        //}
    }
}