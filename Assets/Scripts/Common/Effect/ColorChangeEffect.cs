﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// 色変化
    /// </summary>
    public class ColorChangeEffect : ColorChangeEffectBase
    {
        public enum enState { Non, preChange, NowChanging, End };
        public enState State = enState.Non;

        public enum enMode { Non, MoveOn, TurnBack};
        public enMode MoveMode = enMode.Non;

        public Color StartColor;
        public Color EndColor;
        [SerializeField]
        private Color _moveColor;

        /// <summary>
        /// 0より大きい場合、ループごとにカウントし0になったタイミングで終了
        /// </summary>
        public int LoopCounter;

        void Start()
        {
            //if(MoveMode != enMode.Non)
            //{
            //    init();
            //    State = enState.NowChanging;
            //    MoveMode = enMode.MoveOn;
            //    resetTimeCount();
            //    _moveColor = EndColor - StartColor;
            //    setColor(StartColor);
            //}
        }

        void Update()
        {
            switch (State)
            {
                case enState.preChange:
                    timeCount += getDeltaTime();
                    if (PreTime <= timeCount)
                    {
                        resetTimeCount();
                        State = enState.NowChanging;
                    }
                    else if (ClickToSkip) {
                        if (Input.GetMouseButton(0))
                        {
                            resetTimeCount();
                            State = enState.NowChanging;
                        }
                    }
                    break;

                case enState.NowChanging:
                    timeCount += getDeltaTime();

                    if (timeCount < EndTime)
                    {
                        if(MoveMode== enMode.TurnBack)
                        {
                            setColor(1f - (timeCount / EndTime));
                        }
                        else if (MoveMode == enMode.MoveOn)
                        {
                            setColor(timeCount / EndTime);
                        }
                    }
                    else
                    {
                        procEnd();
                    }
                    break;

                case enState.End:
                    procEnd();
                    break;
            }
        }


        //public void StartFadeOut(float fadeTime)
        //{
        //    EndTime = fadeTime;
        //    StartFadeOut();
        //}

        ///// <summary>
        ///// フェードアウト開始時に呼ぶ関数
        ///// </summary>
        //public virtual void StartFadeOut()
        //{
        //    init();
        //    State = enState.NowFade;
        //    FadeInOrOut = enFadeMode.TurnBack;
        //    resetTimeCount();
        //    setColor(1f);
        //    this.enabled = true;
        //    this.gameObject.SetActive(true);
        //}

        ///// <summary>
        ///// フェードイン開始時に呼ぶ関数
        ///// </summary>
        ///// <param name="fadeTime"></param>
        //public void StartFadeIn(float fadeTime)
        //{
        //    EndTime = fadeTime;
        //    StartFadeIn();
        //}

        ///// <summary>
        ///// フェードイン開始時に呼ぶ関数
        ///// </summary>
        //public void StartFadeIn()
        //{
        //    init();
        //    State = enState.NowFade;
        //    FadeInOrOut = enFadeMode.MoveOn;
        //    resetTimeCount();
        //    setColor(0f);
        //    this.enabled = true;
        //    this.gameObject.SetActive(true);
        //}

        /// <summary>
        /// 少し間をおいて色変化開始
        /// </summary>
        public void PreChangeStart()
        {
            init();
            State = enState.preChange;
            MoveMode = enMode.MoveOn;
            resetTimeCount();
            _moveColor = EndColor - StartColor;
            setColor(StartColor);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// 色変化開始
        /// </summary>
        public void ChangeStart()
        {
            init();
            State = enState.NowChanging;
            MoveMode = enMode.MoveOn;
            resetTimeCount();
            _moveColor = EndColor - StartColor;
            setColor(StartColor);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }

        ///// <summary>
        ///// 色変化開始
        ///// </summary>
        //public void ChangeStartFull()
        //{
        //    init();
        //    State = enState.NowChanging;
        //    MoveMode = enMode.TurnBack;
        //    resetTimeCount();
        //    _moveColor = EndColor - StartColor;
        //    setColor(EndColor);
        //    this.enabled = true;
        //    this.gameObject.SetActive(true);
        //}
        //public void StartAlphaChangeMove(float fadeTime, float startAlpha, float endAlpha)
        //{
        //    init();
        //    State = enState.NowChanging;
        //    FadeInOrOut = enFadeMode.AlphaChangeMove;
        //    timeCount = 0;
        //    EndTime = fadeTime;
        //    _startAlpha = startAlpha;
        //    _moveAlpha = endAlpha - startAlpha;
        //    setColor(startAlpha);
        //    this.enabled = true;
        //    this.gameObject.SetActive(true);
        //}




        /// <summary>
        /// 色を設定する
        /// </summary>

        protected void setColor(Color newColor)
        {
            foreach (SpriteRenderer sr in srList)
            {
                newColor.a = sr.color.a;
                sr.color = newColor;
            }
            foreach (MaskableGraphic gra in graphics)
            {
                newColor.a = gra.color.a;
                gra.color = newColor;
            }
        }

        /// <summary>
        /// 色を設定する
        /// </summary>
        /// <param name="alpha"></param>
        protected void setColor(float value)
        {
            if(value == 0f)
            {
                setColor(StartColor);
            }
            else if(value == 1f)
            {
                setColor(EndColor);
            }
            else
            {
                foreach (SpriteRenderer sr in srList)
                {
                    sr.color = getColor(sr.color, value);
                }
                foreach (MaskableGraphic gra in graphics)
                {
                    gra.color = getColor(gra.color, value);
                }
            }
        }

        private Color getColor(Color oldColor,float value)
        {
            Color newColor = StartColor +(_moveColor * value);
            newColor.a = oldColor.a;
            return newColor;
        }


        /// <summary>
        /// 終了状態で呼ばれる処理
        /// フェードアウトの場合はデストロイまたは非アクティブ化する
        /// </summary>
        protected virtual void procEnd()
        {
            bool loopIsNotEnd = true;
            if (Loop)
            {
                if (LoopCounter > 0)
                {
                    LoopCounter--;
                    if (LoopCounter <= 0)
                    {
                        loopIsNotEnd = false;
                    }
                }
            }

            if (MoveMode == enMode.TurnBack)
            {
                //フェードアウトの場合
                if (Loop && loopIsNotEnd)
                {
                    //ループ設定ONならフェードインに切り替える
                    setColor(0f);
                    MoveMode = enMode.MoveOn;
                    resetTimeCount();
                }
                else if (EndToDestroy)
                {
                    //終了時デストロイ設定ONならDestroy
                    Destroy(this.gameObject);
                }
                else
                {
                    //いずれでもないなら非アクティブ化
                    //this.gameObject.SetActive(false);
                    State = enState.End;
                    this.enabled = false;
                }
            }
            else if (MoveMode == enMode.MoveOn)
            {
                //フェードインの場合

                //どの場合でも透明度は1に
                setColor(1f);

                if (Loop && loopIsNotEnd)
                {
                    //ループ設定ONならフェードアウトに切り替える
                    MoveMode = enMode.TurnBack;
                    resetTimeCount();
                }
                else
                {
                    //そうでないなら終了状態にしつつコンポーネントを無効状態にする
                    State = enState.End;
                    this.enabled = false;
                }
            }
        }

        public void ResetColor()
        {
            init();
            foreach (SpriteRenderer sr in srList)
            {
                sr.color = StartColor;
            }
            foreach (MaskableGraphic gra in graphics)
            {
                gra.color = StartColor;
            }
        }
    }
}