﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// 変化効果 ベースクラス
    /// </summary>
    public class ChangeEffectBase : MonoBehaviour
    {
        public bool Loop = false;

        protected float timeCount = 0;


        void Start()
        {
            init();
        }

        virtual protected void init()
        {

        }

        public bool UseRealTimeScale;
        float lastRealTime;
        protected void resetTimeCount()
        {
            timeCount = 0;
            lastRealTime = Time.realtimeSinceStartup;
        }

        virtual protected float getDeltaTime()
        {
            if (UseRealTimeScale)
            {
                float tmpCount = Time.realtimeSinceStartup - lastRealTime;
                lastRealTime = Time.realtimeSinceStartup;
                return tmpCount;
            }
            else
            {
                return Time.deltaTime;
            }
        }
    }
}