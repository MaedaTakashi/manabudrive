﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// 色変化 ベースクラス
    /// </summary>
    public class ColorChangeEffectBase : ChangeEffectBase
    {
        public float EndTime = 0.5f;
        public float PreTime = 1.0f;
        public bool EndToDestroy;
        public bool WithChild;
        public bool ClickToSkip;

        protected List<SpriteRenderer> srList;
        protected List<UnityEngine.UI.MaskableGraphic> graphics;

        public void ReInit()
        {
            if (srList != null)
            {
                srList.Clear();
            }
            srList = null;
            if (graphics != null)
            {
                graphics.Clear();
            }
            graphics = null;
            init();
        }

        override protected void init()
        {
            if (srList != null) return;
            srList = new List<SpriteRenderer>();

            SpriteRenderer sr = this.GetComponent<SpriteRenderer>();
            if (sr != null)
            {
                srList.Add(sr);
            }
            if (WithChild)
            {
                SpriteRenderer[] children = GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer child in children)
                {
                    srList.Add(child);
                }
            }

            graphics = new List<MaskableGraphic>();
            MaskableGraphic thisGra = this.GetComponent<MaskableGraphic>();
            if (thisGra)
            {
                graphics.Add(thisGra);
            }
            if (WithChild)
            {
                foreach (MaskableGraphic gra in GetComponentsInChildren<MaskableGraphic>())
                {
                    graphics.Add(gra);
                }
            }
        }
    }
}