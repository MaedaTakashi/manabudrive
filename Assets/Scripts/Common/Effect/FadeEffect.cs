﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ManabuDriveApp.Common
{
    /// <summary>
    /// スプライトのフェード
    /// </summary>
    public class FadeEffect : ColorChangeEffectBase
    {
        public enum enState { Non, preFade, NowFade, End };
        public enState State = enState.Non;

        public enum enFadeMode { Non, FadeIn, FadeOut ,AlphaChangeMove };
        public enFadeMode FadeInOrOut = enFadeMode.Non;

        protected float _startAlpha;
        protected float _moveAlpha;

        void Update()
        {
            switch (State)
            {
                case enState.preFade:
                    timeCount += getDeltaTime();
                    if (PreTime <= timeCount)
                    {
                        resetTimeCount();
                        State = enState.NowFade;
                    }
                    else if (ClickToSkip) {
                        if (Input.GetMouseButton(0))
                        {
                            resetTimeCount();
                            State = enState.NowFade;
                        }
                    }
                    break;

                case enState.NowFade:
                    timeCount += getDeltaTime();

                    if (timeCount < EndTime)
                    {
                        if(FadeInOrOut== enFadeMode.FadeOut)
                        {
                            setAlpha(1f - (timeCount / EndTime));
                        }
                        else if (FadeInOrOut == enFadeMode.FadeIn)
                        {
                            setAlpha(timeCount / EndTime);
                        }
                        else if (FadeInOrOut == enFadeMode.AlphaChangeMove)
                        {
                            setAlpha(_startAlpha + (_moveAlpha * (timeCount / EndTime)));
                        }
                    }
                    else
                    {
                        procFadeEnd();
                    }
                    break;

                case enState.End:
                    procFadeEnd();
                    break;
            }
        }


        public void StartFadeOut(float fadeTime)
        {
            EndTime = fadeTime;
            StartFadeOut();
        }

        /// <summary>
        /// フェードアウト開始時に呼ぶ関数
        /// </summary>
        public virtual void StartFadeOut()
        {
            init();
            State = enState.NowFade;
            FadeInOrOut = enFadeMode.FadeOut;
            resetTimeCount();
            setAlpha(1f);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// フェードイン開始時に呼ぶ関数
        /// </summary>
        /// <param name="fadeTime"></param>
        public void StartFadeIn(float fadeTime)
        {
            EndTime = fadeTime;
            StartFadeIn();
        }

        /// <summary>
        /// フェードイン開始時に呼ぶ関数
        /// </summary>
        public void StartFadeIn()
        {
            init();
            State = enState.NowFade;
            FadeInOrOut = enFadeMode.FadeIn;
            resetTimeCount();
            setAlpha(0f);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// 少し間をおいてフェードアウト開始
        /// </summary>
        public void PreFadeOutStart(float preFadeTime,float fadeTime)
        {
            this.PreTime = preFadeTime;
            this.EndTime = fadeTime;
            PreFadeOutStart();
        }

        /// <summary>
        /// 少し間をおいてフェードアウト開始
        /// </summary>
        public void PreFadeOutStart()
        {
            init();
            State = enState.preFade;
            FadeInOrOut = enFadeMode.FadeOut;
            resetTimeCount();
            setAlpha(1f);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// 少し間をおいてフェードイン開始
        /// </summary>
        public void PreFadeInStart()
        {
            init();
            State = enState.preFade;
            FadeInOrOut = enFadeMode.FadeIn;
            resetTimeCount();
            setAlpha(0f);
            this.enabled = true;
            this.gameObject.SetActive(true);
            //Debug.Log("fa " + this.gameObject.activeSelf);
        }

        public void StartAlphaChangeMove(float fadeTime, float startAlpha, float endAlpha)
        {
            init();
            State = enState.NowFade;
            FadeInOrOut = enFadeMode.AlphaChangeMove;
            resetTimeCount();
            EndTime = fadeTime;
            _startAlpha = startAlpha;
            _moveAlpha = endAlpha - startAlpha;
            setAlpha(startAlpha);
            this.enabled = true;
            this.gameObject.SetActive(true);
        }




        /// <summary>
        /// 透明度を設定する
        /// </summary>
        /// <param name="alpha"></param>
        protected void setAlpha(float alpha)
        {
            //Debug.Log("name:" + gameObject.name);
            foreach (SpriteRenderer sr in srList)
            {
                Color color = sr.color;
                color.a = alpha;
                sr.color = color;
            }
            //Debug.Log("gra count " + graphics.Count);
            foreach (MaskableGraphic gra in graphics)
            {
                if (gra != null)
                {
                    Color color = gra.color;
                    color.a = alpha;
                    gra.color = color;
                }
            }
        }



        /// <summary>
        /// 終了状態で呼ばれる処理
        /// フェードアウトの場合はデストロイまたは非アクティブ化する
        /// </summary>
        protected virtual void procFadeEnd()
        {
            if (FadeInOrOut == enFadeMode.FadeOut)
            {
                //フェードアウトの場合
                if (Loop)
                {
                    //ループ設定ONならフェードインに切り替える
                    setAlpha(0f);
                    loopProc(enFadeMode.FadeIn);
                }
                else if (EndToDestroy)
                {
                    //終了時デストロイ設定ONならDestroy
                    Destroy(this.gameObject);
                }
                else
                {
                    //いずれでもないなら非アクティブ化
                    this.gameObject.SetActive(false);
                    State = enState.End;
                    this.enabled = false;
                }
            }
            else if (FadeInOrOut == enFadeMode.FadeIn)
            {
                //フェードインの場合

                //どの場合でも透明度は1に
                setAlpha(1f);

                if (Loop)
                {
                    //ループ設定ONならフェードアウトに切り替える
                    loopProc(enFadeMode.FadeOut);
                }
                else
                {
                    //そうでないなら終了状態にしつつコンポーネントを無効状態にする
                    State = enState.End;
                    this.enabled = false;
                }
            }

            else if (FadeInOrOut == enFadeMode.AlphaChangeMove)
            //アルファ値変動の場合
            {
                float endAlpha = _startAlpha + _moveAlpha;
                setAlpha(endAlpha);

                if (Loop)
                {
                    //ループ設定ONなら初期値を現在の値に、変動量の正負を逆にする
                    _startAlpha = endAlpha;
                    _moveAlpha = - _moveAlpha;
                    loopProc(enFadeMode.AlphaChangeMove);
                }
                else
                {
                    if(endAlpha == 0f)
                    {
                        if (EndToDestroy)
                        {
                            //終了時デストロイ設定ONならDestroy
                            Destroy(this.gameObject);
                        }
                        else
                        {
                            //そうでないなら非アクティブ化
                            this.gameObject.SetActive(false);
                            State = enState.End;
                            this.enabled = false;
                        }
                    }
                    else
                    {
                        State = enState.End;
                        this.enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// ループする際に呼ばれる処理
        /// </summary>
        virtual protected void loopProc(enFadeMode fadeInOrOut)
        {
            FadeInOrOut = fadeInOrOut;
            resetTimeCount();
        }
    }
}