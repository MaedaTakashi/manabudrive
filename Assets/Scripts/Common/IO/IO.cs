﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

namespace ManabuDriveApp.Common
{
    public class IO
    {
        public static void TestSave()
        {
            logSave(Application.persistentDataPath + "/persistent.csv", "test " + Application.persistentDataPath);
            logSave(Application.temporaryCachePath + "/tmp.csv", "test " + Application.temporaryCachePath);
            logSave(Application.dataPath + "/FileName.csv", "test " + Application.dataPath);
        }

        public static void logSave(string path, string txt)
        {
            DebugManager.PutLog("save:" + path);

            //StreamWriter sw;
            //FileInfo fi;
            //fi = new FileInfo(path);
            //sw = fi.AppendText();
            //sw.WriteLine(txt);
            //sw.Flush();
            //sw.Close();
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                sw.Write(txt);
            }

            DebugManager.PutLog("save OK ");
        }
    }
}
