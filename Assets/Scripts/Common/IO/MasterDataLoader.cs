﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ManabuDriveApp.Common
{
    public class MasterDataLoader : MonoSingleton<MasterDataLoader> {
	    public static bool isLoaded = false;

	    public static void Create () {
		    MasterDataLoader.instance.Load ();
	    }

	    /// <summary>
	    /// Init this instance.
	    /// </summary>
	    void Load () {
		    isLoaded = false;
		    StartCoroutine (LoadCSV ());
	    }

	    /// <summary>
	    /// CSVを読み込み
	    /// </summary>
	    /// <returns>The CS.</returns>
	    IEnumerator LoadCSV () {
            MapEventSpot.Data.Clear();
            Action<string[]> action = MapEventSpot.Create;
            yield return StartCoroutine(ParseCSV("CSV/MapEventSpot", action));

            RoutePoint.Data.Clear();
            action = RoutePoint.Create;
            yield return StartCoroutine(ParseCSV("CSV/RoutePoints", action));

            isLoaded = true;
	    }

	    /// <summary>
	    /// CSVを解析
	    /// </summary>
	    /// <returns>The CS.</returns>
	    /// <param name="filePath">File path.</param>
	    /// <param name="callback">Callback.</param>
	    public static IEnumerator ParseCSV (string filePath, Action<string[]> callback) {
		    ResourceRequest request = Resources.LoadAsync<TextAsset> (filePath);
		    while (request.isDone == false) yield return null;
		    TextAsset textAsset = request.asset as TextAsset;
		    string text = textAsset.text.Trim ().Replace ("\r", "");
		    List<string> lines = text.Split ('\n').ToList ();

		    bool isHead = true;
		    string mergedLine = null;

		    foreach (string line in lines) {
                //mergedLine = line.Replace("@", "\n");
                mergedLine = line.Replace("\\n", "\n");

                if (!isHead) {
				    string[] elements = mergedLine.Split (',');
				    for (int i = 0; i < elements.Length; i++) {
					    if (String.IsNullOrEmpty (elements [i])) elements [i] = "";
					    elements [i] = elements [i].TrimEnd ('"').TrimStart ('"');
				        //elements[i] = elements[i].Replace("<com>",",");
				        //elements[i] = elements[i].Replace("<at>", "@");
				    }

				    callback (elements);
			    }

			    isHead = false;
		    }
			
		    Resources.UnloadUnusedAssets ();
	    }

	    /// <summary>
	    /// 配列の不足要素を0で埋める
	    /// </summary>
	    /// <returns>The array.</returns>
	    /// <param name="array">Array.</param>
	    /// <param name="length">Length.</param>
	    public static string[] PadArray (string[] elements, int length) {
		    if (elements.Length >= length) return elements;
		    string[] zeros = Enumerable.Repeat<string> ("0", length - elements.Length).ToArray ();
		    return elements.Concat (zeros).ToArray ();
	    }

        public static int ParseInt(string value,int defaultValue)
        {
            int tmp;
            if(int.TryParse(value,out tmp))
            {
                return (tmp);
            }
            else
            {
                return defaultValue;
            }
        }
    }
}

