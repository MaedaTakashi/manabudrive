﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class TimeCountDisp : MonoBehaviour
    {
        [SerializeField]
        private AnimationCurve playCurve;

        [SerializeField]
        private Text text;

        private int _startCount;
        private int _endCount;

        public enum enState{ Stop,Play};
        public enState State = enState.Stop;

        private float _time;
        private float _playTime;
        private float _moveCount;
        private string _strFormat;

        public void PlayStart(int startCount,int endCount,float playTime)
        {
            _time = 0f;
            _playTime = playTime;
            _moveCount = endCount - startCount;

            _startCount = startCount;
            _endCount = endCount;
            _strFormat = new string('0', _endCount.ToString().Length);

            State = enState.Play;

        }

        public void setCount(int count)
        {
            _strFormat = new string('0', count.ToString().Length);
            text.text = count.ToString(_strFormat);
        }


        void Update()
        {
            if(State== enState.Play)
            {
                int tmpNowCount;
                _time += Time.deltaTime;
                if (_time >= _playTime)
                {
                    tmpNowCount = _endCount;
                    State = enState.Stop;
                }
                else
                {
                    tmpNowCount = Mathf.FloorToInt(_startCount + _moveCount * playCurve.Evaluate(_time/_playTime));
                }

                text.text = tmpNowCount.ToString(_strFormat);

            }
        }

    }
}
