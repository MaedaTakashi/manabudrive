﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class KokinSyasinManager : MonoBehaviour
    {

        #region Singleton
        private static KokinSyasinManager instance;
        public static KokinSyasinManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (KokinSyasinManager)FindObjectOfType(typeof(KokinSyasinManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        [SerializeField]
        private TimeCountDisp TimeCounter;
        [SerializeField]
        private CameraImage CameraImage;

        [SerializeField]
        private VortexTimer VortexTimer;

        [SerializeField]
        private FadeEffect WebCamFade;

        [SerializeField]
        private FadeEffect OldImgFade;
        [SerializeField]
        private VortexTimer OldVortexTimer;


        [SerializeField]
        private Image OldImage;

        [SerializeField]
        private Image SilhouetteImage;

        [SerializeField]
        private TextMeshProUGUI txtCaption;

        [SerializeField]
        private TextMeshProUGUI txtButton;

        [SerializeField]
        private TextMeshProUGUI txtAfterMovie;


        [SerializeField]
        private FadeEffect fadeBefore;

        [SerializeField]
        private FadeEffect fadeAfter;


        //private Sprite sprSilhouette;
        //private Sprite sprOldTime;
        private int spotID;
        void Start()
        {

            if(GameManager.ActiveMapEventSpot == null)
            {
                spotID = 4;
            }
            else
            {
                spotID = GameManager.ActiveMapEventSpot.ID;
            }

            StartCoroutine(coStart());
            //Debug.Log("spotID:" + spotID);

        }

        private IEnumerator coStart()
        {
            yield return null;

            switch (spotID)
            {
                case 3:
                    //sprSilhouette = Resources.Load<Sprite>("KokinSyasin/Silhouette03");
                    //SilhouetteImage.sprite = sprSilhouette;
                    //sprOldTime = Resources.Load<Sprite>("KokinSyasin/OldTimeImage03");
                    //OldTimeImage.sprite = sprOldTime;
                    //sprSilhouette = Resources.Load<Sprite>("KokinSyasin/Silhouette03");
                    SilhouetteImage.sprite = Resources.Load<Sprite>("KokinSyasin/Silhouette03");
                    yield return null;
                    SilhouetteImage.GetComponent<FadeEffect>().StartFadeIn();
                    yield return null;
                    //sprOldTime = Resources.Load<Sprite>("KokinSyasin/OldTimeImage03");
                    OldImage.sprite = Resources.Load<Sprite>("KokinSyasin/OldTimeImage03");
                    yield return null;
                    txtCaption.text = "枠線にぴったり合わせよう！";
                    txtButton.text = "ぴったり！";
                    txtAfterMovie.text = "明治44年にタイムスリップ！";
                    break;

                case 4:
                    //SilhouetteImage.gameObject.SetActive(false);
                    //sprOldTime = Resources.Load<Sprite>("KokinSyasin/OldTimeImage04");
                    //OldTimeImage.sprite = sprOldTime;
                    OldImage.sprite = Resources.Load<Sprite>("KokinSyasin/OldTimeImage04");
                    yield return null;
                    txtCaption.text = "海の方向に向けてみよう！";
                    txtButton.text = "向けた！";
                    txtAfterMovie.text = "1853年にタイムスリップ！";
                    break;
            }

            yield return null;
            CameraImage.enabled = true;
            yield return null;

            fadeBefore.StartFadeIn();
        }
        
        //void OnDestroy()
        //{
        //    Debug.Log("OnDestroy:" + sprSilhouette);
        //    Destroy(sprSilhouette);
        //    Destroy(sprOldTime);
        //}

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    //SoundControl.PlaySound(0);
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    if (!nowWarping)
                    {
                        StartCoroutine(warpImageEffect());
                    }
                    break;
            }
        }

        bool nowWarping = false;
        private IEnumerator warpImageEffect()
        {
            nowWarping = true;
            WebCamFade.gameObject.SetActive(true);
            yield return null;

            foreach (Graphic gra in WebCamFade.GetComponentsInChildren<Graphic>())
            {
               gra.color = Color.white;
            }
            OldImgFade.gameObject.SetActive(false);

            if (SilhouetteImage.gameObject.activeSelf)
            {
                SilhouetteImage.GetComponent<FadeEffect>().StartFadeOut();
            }

            int afterCount;
            if (spotID == 4)
            {
                afterCount = 1853;
            }
            else
            {
                afterCount = 1911;
            }

            Camera MainCam = VortexTimer.GetComponent<Camera>();
            MainCam.clearFlags = CameraClearFlags.Depth;

            TimeCounter.gameObject.SetActive(true);
            FadeEffect timeFade = TimeCounter.GetComponent<FadeEffect>();
            timeFade.StartFadeIn();
            TimeCounter.setCount(2018);
            CameraImage.WebCamStop();

            fadeBefore.StartFadeOut();

            VortexTimer.PlayStart(0f, 1440f, 2.5f);
            OldVortexTimer.PlayStart(-5760f, 0f, 5f);

            //0.5
            yield return new WaitForSeconds(0.5f);
            TimeCounter.PlayStart(2018, afterCount, 4.5f);

            //1.5
            yield return new WaitForSeconds(1.0f);
            WebCamFade.StartFadeOut(1.0f);
            OldImgFade.StartFadeIn(1.0f);

            //2.5
            yield return new WaitForSeconds(1.0f);
            VortexTimer.PlayStart(1440f, 0f, 2.5f);

            //5.0
            yield return new WaitForSeconds(2.5f);
            nowWarping = false;

            timeFade.PreFadeOutStart();
            fadeAfter.StartFadeIn();

            effectEnd();
        }

        private void effectEnd()
        {
            //暫定
            OldImgFade.GetComponent<ClickToNext>().ClickToGotoEventScene = true;
            return;

            //switch (spotID)
            //{
            //    case 3:
            //        break;

            //    case 4:
            //        StartCoroutine(waitAndGotoScene(2f, "Puzzle"));
            //        break;
            //}
        }

        private IEnumerator waitAndGotoScene(float waitTime,string sceneName)
        {
            yield return new WaitForSeconds(waitTime);
            ScheneSwitcher.LoadScene(sceneName);
        }
    }
}
