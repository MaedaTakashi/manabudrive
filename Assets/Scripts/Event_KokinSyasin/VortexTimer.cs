﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityStandardAssets.ImageEffects;

namespace ManabuDriveApp.KokinSyasin
{
    public class VortexTimer : MonoBehaviour
    {
        [SerializeField]
        private AnimationCurve playCurve;

        public enum enState{ Stop,Play};
        public enState State = enState.Stop;

        private float _time;
        private float _playTime;

        private float _startValue;
        private float _endValue;
        private float _moveValue;

        private UnityStandardAssets.ImageEffects.Vortex vortex;

        public void PlayStart(float startValue,float endValue,float playTime)
        {
            vortex = this.GetComponent<Vortex>();
            vortex.enabled = true;
            _time = 0f;
            _playTime = playTime;
            _moveValue = endValue - startValue;

            _startValue = startValue;
            _endValue = endValue;

            State = enState.Play;
            this.enabled = true;
        }

        void Update()
        {
            if(State== enState.Play)
            {
                _time += Time.deltaTime;
                if (_time >= _playTime)
                {
                    vortex.angle = _endValue;
                    State = enState.Stop;
                    this.enabled = false;
                    //vortex.enabled = false;
                }
                else
                {
                    vortex.angle = _startValue + _moveValue * playCurve.Evaluate(_time/_playTime);
                }
            }
        }

    }
}
