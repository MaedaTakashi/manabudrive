﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class ClickToNext : MonoBehaviour, IPointerClickHandler
    {
        public bool ClickToGotoEventScene = false;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (ClickToGotoEventScene)
            {
                KokinSyasinManager.Instance.ClickButton(0);
            }
        }
    }
}
