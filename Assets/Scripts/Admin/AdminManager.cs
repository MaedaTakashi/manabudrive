﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Admin
{
    public class AdminManager : MonoBehaviour
    {

        #region Singleton
        private static AdminManager instance;
        public static AdminManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (AdminManager)FindObjectOfType(typeof(AdminManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    DataClear();
                    break;

                case 1:
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;
            }
        }


        private void DataClear()
        {
            GameManager.ClearUsers();
            GameManager.SetupNewUser();
            GameManager.SaveData("Title");
            ScheneSwitcher.LoadScene("Title", 1f);
        }
    }
}
