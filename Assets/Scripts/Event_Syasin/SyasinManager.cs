﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class SyasinManager : MonoBehaviour
    {

        #region Singleton
        private static SyasinManager instance;
        public static SyasinManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (SyasinManager)FindObjectOfType(typeof(SyasinManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        [SerializeField]
        private CameraImage WebCam;

        [SerializeField]
        private TextMeshProUGUI txtCaption;


        [SerializeField]
        private TextMeshProUGUI txtAfterMovie;


        [SerializeField]
        private FadeEffect fadeBefore;

        [SerializeField]
        private FadeEffect fadeAfter;

        [SerializeField]
        private GameObject Sight;


        //private Sprite sprSilhouette;
        //private Sprite sprOldTime;
        private int spotID;
        void Start()
        {

            if(GameManager.ActiveMapEventSpot == null)
            {
                spotID = 3;
            }
            else
            {
                spotID = GameManager.ActiveMapEventSpot.ID;
            }

            StartCoroutine(coStart());
            //Debug.Log("spotID:" + spotID);

        }

        private IEnumerator coStart()
        {
            yield return null;

            switch (spotID)
            {
                case 3:
                    yield return null;
                    txtCaption.text = "枠線にぴったり合わせよう！";
                    break;

            }

            yield return null;
            WebCam.enabled = true;
            yield return null;

            fadeBefore.StartFadeIn();
        }
        
        //void OnDestroy()
        //{
        //    Debug.Log("OnDestroy:" + sprSilhouette);
        //    Destroy(sprSilhouette);
        //    Destroy(sprOldTime);
        //}

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    //SoundControl.PlaySound(0);
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    if (!nowWarping)
                    {
                        StartCoroutine(warpImageEffect());
                    }
                    break;
            }
        }

        bool nowWarping = false;
        private IEnumerator warpImageEffect()
        {
            nowWarping = true;
            //WebCamFade.gameObject.SetActive(true);
            yield return null;

            WebCam.WebCamStop();
            WebCam.enabled = false;
            Sight.SetActive(false);

            yield return null;
            WebCam.GetComponent<ColorChangeEffect>().ChangeStart();
            fadeBefore.StartFadeOut();

            //VortexTimer.PlayStart(0f, 1440f, 2.5f);
            //OldVortexTimer.PlayStart(-5760f, 0f, 5f);

            ////0.5
            //yield return new WaitForSeconds(0.5f);
            //TimeCounter.PlayStart(2018, afterCount, 4.5f);

            ////1.5
            //yield return new WaitForSeconds(1.0f);
            //WebCamFade.StartFadeOut(1.0f);
            //OldImgFade.gameObject.SetActive(true);

            ////2.5
            //yield return new WaitForSeconds(1.0f);
            //VortexTimer.PlayStart(1440f, 0f, 2.5f);

            ////5.0
            yield return new WaitForSeconds(1.0f);
            nowWarping = false;

            //timeFade.PreFadeOutStart();
            fadeAfter.StartFadeIn();

            effectEnd();
        }

        private void effectEnd()
        {
            switch (spotID)
            {
                case 3:
                    break;

                case 4:
                    StartCoroutine(waitAndGotoScene(2f, "Puzzle"));
                    break;
            }
        }

        private IEnumerator waitAndGotoScene(float waitTime,string sceneName)
        {
            yield return new WaitForSeconds(waitTime);
            ScheneSwitcher.LoadScene(sceneName);
        }
    }
}
