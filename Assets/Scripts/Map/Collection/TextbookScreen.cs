﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class TextbookScreen : MonoBehaviour
    {
        public Image TextBookImage;
        public CollectionAddPanel CollectionAddPanel;

        public enum enBackTo { Collection,Map}
        private enBackTo backTo;

        RectTransform thisRectTran;
        Vector2 panelPos;
        Vector2 panelOutPos = new Vector2(1536f, 0f);

        void init()
        {
            if (thisRectTran) return;
            thisRectTran = this.GetComponent<RectTransform>();
            panelPos = thisRectTran.anchoredPosition;
        }

        public void Show(int textbookIndex,bool isNew,enBackTo backTo,bool withFade)
        {
            init();

            this.backTo = backTo;
            if (isNew)
            {
                CollectionAddPanel.Show();
            }
            this.gameObject.SetActive(true);

            if (withFade)
            {
                thisRectTran.anchoredPosition = panelOutPos;
                StartCoroutine(coShow());
                GameManager.EventSystemEnable = false;
            }
            else
            {
                thisRectTran.anchoredPosition = panelPos;
                MapManager.Instance.CollectionScreen.Close(false);
                MapManager.Instance.MapScreen.SetActive(false);
                MapManager.Instance.Warikomi.gameObject.SetActive(false);
            }

        }

        IEnumerator coShow()
        {
            LeanTween.move(thisRectTran, panelPos, 0.5f).setEaseInOutCubic();

            yield return new WaitForSeconds(0.5f);

            MapManager.Instance.CollectionScreen.Close(false);
            GameManager.EventSystemEnable = true;
        }


        public void Back()
        {
            StartCoroutine(coBack());
            GameManager.EventSystemEnable = false;

        }

        IEnumerator coBack()
        {
            LeanTween.move(thisRectTran, panelOutPos, 0.5f).setEaseInOutCubic();


            if (backTo == enBackTo.Map)
            {
                MapManager.Instance.ShowMap();
                MapManager.Instance.CollectionScreen.Close(false);
            }
            else
            {
                MapManager.Instance.ShowCollection(false);
            }
            yield return new WaitForSeconds(0.5f);
            GameManager.EventSystemEnable = true;
            this.gameObject.SetActive(false);
        }


        void OnDisable()
        {
            Debug.Log("OnDisable");
            if(TextBookImage != null)
            {
                TextBookImage.sprite = null;
            }

            if (CollectionAddPanel != null)
            {
                if (CollectionAddPanel.gameObject.activeSelf)
                {
                    CollectionAddPanel.Close();
                }
            }

            if (MapManager.Instance != null)
            {
                MapManager.Instance.UnloadTextbook();
            }
        }
    }
}
