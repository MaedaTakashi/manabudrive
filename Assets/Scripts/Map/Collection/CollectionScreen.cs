﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class CollectionScreen : MonoBehaviour
    {
        public GameObject OrgItem;
        public Transform ItemsTran;
        public TextMeshProUGUI txtNum;
        public RectTransform ContentRectTran;
        public Sprite[] ImageSprites;
        public Sprite NoimageSprite;

        private bool isInit = false;
        private Vector2 defaultContentSize;

        private void init()
        {
            if (isInit) return;
            defaultContentSize = ContentRectTran.sizeDelta;
            isInit = true;
        }

        public void Show(bool withFade)
        {
            const float ROW_HEIGHT = 512f;
            const float COL_WIDTH = 456f;

            init();

            clearItem();

            OrgItem.SetActive(false);
            Vector2 orgPos = OrgItem.GetComponent<RectTransform>().anchoredPosition;

            int lastRow = 0;
            int numOpen = 0;
            for (int i = 0; i < GameManager.ActiveUser.BookFlgs.Length; i++)
            {
                GameObject go = GameObject.Instantiate(OrgItem, ItemsTran);
                int row = i / 3;
                int col = i % 3;

                lastRow = row;
                Vector2 tmpPos = new Vector2(orgPos.x + col * COL_WIDTH, orgPos.y + row * -ROW_HEIGHT);
                go.GetComponent<RectTransform>().anchoredPosition = tmpPos;
                CollectionItem item = go.GetComponent<CollectionItem>();
                if(GameManager.ActiveUser.BookFlgs[i] == 1)
                {
                    item.TextbookIndex = i;
                    if(i< ImageSprites.Length)
                    {
                        item.Image.sprite = ImageSprites[i];
                    }
                    else
                    {
                        item.Image.sprite = NoimageSprite;
                    }
                    numOpen++;
                }
                else
                {
                    item.TextbookIndex = -1;
                    item.Image.sprite = NoimageSprite;
                    item.Button.interactable = false;
                }
                go.SetActive(true);
            }
            txtNum.text = numOpen.ToString() + "/" + GameManager.ActiveUser.BookFlgs.Length + "人";
            ContentRectTran.sizeDelta = new Vector2(defaultContentSize.x, defaultContentSize.y + lastRow * ROW_HEIGHT);

            CanvasGroup cg = this.GetComponent<CanvasGroup>();
            this.gameObject.SetActive(true);

            if (withFade)
            {
                cg.alpha = 0.1f;
                LeanTween.alphaCanvas(this.GetComponent<CanvasGroup>(), 1f, 0.5f);
                StartCoroutine(coShow());
                GameManager.EventSystemEnable = false;
            }
            else
            {
                cg.alpha = 1.0f;
            }
        }

        IEnumerator coShow()
        {
            yield return new WaitForSeconds(0.5f);
            GameManager.EventSystemEnable = true;
        }

        private void clearItem()
        {
            foreach (Transform itemTran in ItemsTran)
            {
                if(itemTran.gameObject != OrgItem)
                {
                    Destroy(itemTran.gameObject);
                }
            }
        }

        public void Close(bool withFade)
        {
            clearItem();

            if (withFade)
            {
                if (this.gameObject.activeSelf)
                {
                    CanvasGroup cg = this.GetComponent<CanvasGroup>();
                    LeanTween.alphaCanvas(this.GetComponent<CanvasGroup>(), 0.1f, 0.5f);
                    StartCoroutine(coClose());
                    GameManager.EventSystemEnable = false;
                }
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }

        IEnumerator coClose()
        {
            yield return new WaitForSeconds(0.5f);
            GameManager.EventSystemEnable = true;
            this.gameObject.SetActive(false);
        }
    }
}
