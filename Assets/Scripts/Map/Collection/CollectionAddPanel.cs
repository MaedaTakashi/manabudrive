﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class CollectionAddPanel : MonoBehaviour,IPointerClickHandler
    {
        //public ParticleSystem PaparParticle;
        [SerializeField]
        private Sprite[] TeacherSprite;

        [SerializeField]
        private Image TeacherImage;

        [SerializeField]
        private TMPro.TextMeshProUGUI msg;

        public void Show()
        {
            MapEventSpot spot = GameManager.ActiveMapEventSpot;
            if((spot.BookIndex >= 0)&& (spot.BookIndex < TeacherSprite.Length))
            {
                if(TeacherSprite[spot.BookIndex] != null)
                {
                    TeacherImage.enabled = true;
                    TeacherImage.sprite = TeacherSprite[spot.BookIndex];
                }
                else
                {
                    TeacherImage.enabled = false;
                }
            }
            else
            {
                TeacherImage.enabled = false;
            }
            msg.text = spot.TeacherName + "先生が\nコレクションに加わったよ！";

            this.gameObject.SetActive(true);
        }

        public void Close()
        {
            //if (PaparParticle != null)
            //{
            //    PaparParticle.Stop();
            //}
            GameManager.SaveData();
            this.gameObject.SetActive(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Close();
        }
    }
}
