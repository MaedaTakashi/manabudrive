﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class CollectionItem : MonoBehaviour
    {
        public int TextbookIndex;
        public Image Image;
        public Button Button;

        public void ShowTextbook()
        {
            SoundControl.PlaySound(8);
            MapManager.Instance.ShowTextbook(TextbookIndex,false, TextbookScreen.enBackTo.Collection,true);
        }
    }
}
