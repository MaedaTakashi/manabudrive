﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class MiniMap : MonoBehaviour
    {

        RawImage mapGuiTexture;
        private float intervalTime = 0.0f;
        private int width = 250;
        private int height = 250;
        private double longitude;
        private double latitude;
        private int zoom = 16;

        private Text posText;

        private InputField input1;
        private InputField input2;
        private InputField input3;
        private bool nowGetPos;
        private double lastTimeStamp;

        void Start()
        {
            lastTimeStamp = 0;
            mapGuiTexture = this.GetComponent<RawImage>();
            mapGuiTexture.color = Color.white;

            input1 = this.transform.parent.Find("InputField1").GetComponent<InputField>();
            input2 = this.transform.parent.Find("InputField2").GetComponent<InputField>();
            input3 = this.transform.parent.Find("InputField3").GetComponent<InputField>();

            input1.text = width.ToString();
            input2.text = height.ToString();
            input3.text = zoom.ToString();

            Vector2 size = this.GetComponent<RectTransform>().sizeDelta;
            posText = this.transform.Find("Text").GetComponent<Text>();

            nowGetPos = true;
            //width = (int)size.x;
            //height = (int)size.y;
            GetPos();
        }
        void Update()
        {
            if (nowGetPos) return;
            //毎フレーム読んでると処理が重くなるので、3秒毎に更新
            intervalTime += Time.deltaTime;
            if (intervalTime >= 3.0f)
            {
                GetPos();
                intervalTime = 0.0f;
            }
        }
        void GetPos()
        {
            //GPSで取得した緯度経度を変数に代入
            StartCoroutine(GetGPS());
        }
        void GetMap()
        {
            //マップを取得
            StartCoroutine(GetStreetViewImage(latitude, longitude, zoom));
        }
        private IEnumerator GetGPS()
        {
            DebugManager.PutLog("GPS取得");
            nowGetPos = true;
            DebugManager.PutLog(" isEnabledByUser:" + Input.location.isEnabledByUser);
            if (!Input.location.isEnabledByUser)
            {
                nowGetPos = false;
                yield break;
            }
            Input.location.Start();
            int waitCount = 20;
            while ((Input.location.status == LocationServiceStatus.Initializing) && (waitCount > 0))
            {
                DebugManager.PutLog(" 初期化待ち:" + waitCount.ToString() + " status:" + Input.location.status);
                yield return new WaitForSeconds(1);
                waitCount--;
            }
            if (waitCount < 1)
            {
                DebugManager.PutLog("  timeOut");
                nowGetPos = false;
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                DebugManager.PutLog(" Unable to determine device location");
                nowGetPos = false;
                yield break;
            }
            else
            {
                //posText.text = ToStrLocation();
                //print("Location: " +
                //      Input.location.lastData.latitude + " " +
                //      Input.location.lastData.longitude + " " +
                //      Input.location.lastData.altitude + " " +
                //      Input.location.lastData.horizontalAccuracy + " " +
                //      Input.location.lastData.timestamp);
            }

            waitCount = 20;
            while (Input.location.lastData.timestamp <= lastTimeStamp && waitCount > 0)
            {
                DebugManager.PutLog(" タイムスタンプ更新待ち:" + waitCount.ToString());
                yield return new WaitForSeconds(1);
                waitCount--;
            }
            if (waitCount < 1)
            {
                DebugManager.PutLog("  timeOut");
                nowGetPos = false;
                yield break;
            }

            lastTimeStamp = Input.location.lastData.timestamp;
            Input.location.Stop();
            DebugManager.PutLog(" GPS取得終了");

            longitude = Input.location.lastData.longitude;
            latitude = Input.location.lastData.latitude;
            posText.text = ToStrLocation();
            GetMap();
        }

        private IEnumerator GetStreetViewImage(double latitude, double longitude, double zoom)
        {
            string url = "http://maps.googleapis.com/maps/api/staticmap?center="
                + latitude + "," + longitude + "&zoom=" + zoom + "&size=" + width + "x"
                + height + "&markers=size:mid%7Ccolor:red%7C" + latitude + "," + longitude;
            DebugManager.PutLog("www取得:" + url);
            WWW www = new WWW(url);
            yield return www;
            //マップの画像をTextureとして貼り付ける
            mapGuiTexture.texture = www.texture;
            ////ミニマップに透明度を与える
            //mapGuiTexture.color = new Color(mapGuiTexture.color.r, mapGuiTexture.color.g, mapGuiTexture.color.b, 0.4f);
            nowGetPos = false;
        }

        private string ToStrLocation()
        {
            string tmpStr = "緯度　latitude: " + Input.location.lastData.latitude + "\n"
                + "経度 longitude: " + Input.location.lastData.longitude + "\n"
                + "高度　altitude: " + Input.location.lastData.altitude + "\n"
                + "水平方向精度　horizontalAccuracy: " + Input.location.lastData.horizontalAccuracy + "\n"
                + "垂直方向精度　verticalAccuracy: " + Input.location.lastData.verticalAccuracy + "\n"
                + "timestamp: " + GetTimeStr(Input.location.lastData.timestamp)
                ;
            return tmpStr;
        }

        private string GetTimeStr(double timestamp)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp).ToLocalTime();
            return (datetime.ToString("yyyy-MM-dd HH:mm:ss"));
        }


        public void ChangeInput1()
        {
            int tmp;
            if(int.TryParse(input1.text,out tmp))
            {
                width = tmp;
            }
        }

        public void ChangeInput2()
        {
            int tmp;
            if (int.TryParse(input2.text, out tmp))
            {
                height = tmp;
            }
        }

        public void ChangeInput3()
        {
            int tmp;
            if (int.TryParse(input3.text, out tmp))
            {
                zoom = tmp;
            }
        }
    }
}
