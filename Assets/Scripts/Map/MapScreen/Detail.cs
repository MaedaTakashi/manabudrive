﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class Detail : MonoBehaviour,IPointerClickHandler
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI SpotName;
        [SerializeField]
        private TMPro.TextMeshProUGUI SpotText;
        [SerializeField]
        private Image SpotImage;
        [SerializeField]
        private Sprite[] SpotSprite;
        [SerializeField]
        private Sprite DmySprite;
        [SerializeField]
        private Button btn_GotoTextbook;

        private MapEventSpot spot;

        RectTransform panelRectTran;
        Vector2 panelPos;
        Vector2 panelOutPos = new Vector2(0f,-784f);

        void init()
        {
            if (panelRectTran) return;
            panelRectTran = this.transform.Find("DetailPanel").GetComponent<RectTransform>();
            panelPos = panelRectTran.anchoredPosition;
        }

        public void Show(MapItem item)
        {
            init();

            spot = item.SpotData;
            SpotName.text = spot.TeacherName + "先生";
            SpotText.text = spot.SpotText;

            if ((spot.BookIndex >= 0) && (spot.BookIndex < SpotSprite.Length))
            {
                if (SpotSprite[spot.BookIndex] != null)
                {
                    //TeacherImage.enabled = true;
                    SpotImage.sprite = SpotSprite[spot.BookIndex];
                }
                else
                {
                    SpotImage.sprite = DmySprite;
                }
            }
            else
            {
                SpotImage.sprite = DmySprite;
            }
            //if(item.State == MapItem.enState.end)
            //{
            //    EndMark.SetActive(true);
            //}
            //else
            //{
            //    EndMark.SetActive(false);
            //}

            if (item.SpotData.BookIndex < 0)
            {
                btn_GotoTextbook.interactable = false;
            }
            else if (item.SpotData.BookIndex >= GameManager.ActiveUser.BookFlgs.Length)
            {
                btn_GotoTextbook.interactable = false;
            }
            else if (GameManager.ActiveUser.BookFlgs[item.SpotData.BookIndex] == 1)
            {
                btn_GotoTextbook.interactable = true;
            }
            else
            {
                btn_GotoTextbook.interactable = false;
            }

            panelRectTran.anchoredPosition = panelOutPos;

			panelRectTran.GetComponent<Image>().color = new Color(1,1,1,0);
			SpotName.color = Color.clear;
			SpotText.color = Color.clear;

            this.gameObject.SetActive(true);
            StartCoroutine(coShow());
            GameManager.EventSystemEnable = false;
        }

        IEnumerator coShow()
        {
			LeanTween.move(panelRectTran, panelPos, 1.0f).setEaseOutQuart();
			LeanTween.alpha(panelRectTran,1.0f,1.0f).setEaseOutQuart();

			SpotName.color = Color.black;
			SpotText.color = Color.black;

            yield return new WaitForSeconds(1.0f);

            GameManager.EventSystemEnable = true;
        }

        public void Close()
        {
            StartCoroutine(coClose());
            GameManager.EventSystemEnable = false;
        }

        IEnumerator coClose()
        {
			LeanTween.move(panelRectTran, panelOutPos, 0.7f).setEaseInQuart();
			LeanTween.alpha(panelRectTran,0.0f,0.7f).setEaseOutQuart();

			SpotName.color = Color.clear;
			SpotText.color = Color.clear;
            yield return new WaitForSeconds(0.8f);

            GameManager.EventSystemEnable = true;
            this.gameObject.SetActive(false);
        }

        public void GotoTextbook()
        {
            MapManager.Instance.ShowTextbook(spot.BookIndex,false, TextbookScreen.enBackTo.Map,true);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Close();
        }
    }
}
