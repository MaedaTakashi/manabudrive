﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class MapViewLoader : MonoBehaviour
    {
        private MapViewControl MapViewControl;

        public Sprite[] IconSprite_n;
        public Sprite[] IconSprite_r;
        public Sprite[] IconSprite_w;
        public Sprite IconSprite_Start;


        public void Load(MapViewControl mapViewControl)
        {
            this.MapViewControl = mapViewControl;
            StartCoroutine(coLoad());
        }

        IEnumerator coLoad()
        {
            yield return StartCoroutine(LoadAsyncTextureCoroutine("MapScreen/map_overall"));

            while (!MasterDataLoader.isLoaded)
            {
                yield return null;
            }

            yield return StartCoroutine(coCreateRoutePoints());
            yield return StartCoroutine(coCreateItem());
        }

        /// <summary>
        /// リソースからテクスチャを非同期でロードするコルーチン
        /// </summary>
        IEnumerator LoadAsyncTextureCoroutine(string filePath)
        {
            // リソースの非同期読込開始
            ResourceRequest resReq = Resources.LoadAsync<Sprite>(filePath);
            // 終わるまで待つ
            while (resReq.isDone == false)
            {
                //Debug.Log("Loading progress:" + resReq.progress.ToString());
                yield return 0;
            }
            // テクスチャ表示
            Image img = MapViewControl.MapRectTran.GetComponent<Image>();
            img.color = Color.white;
            img.sprite = resReq.asset as Sprite;

            Destroy(MapViewControl.transform.Find("Text").gameObject);
        }

        /// <summary>
        /// マップアイテム作成
        /// </summary>
        /// <returns></returns>
        IEnumerator coCreateItem()
        {
            Transform itemsTran = MapViewControl.MapRectTran.transform.Find("Items");
            foreach (MapEventSpot spotData in MapEventSpot.Data)
            {
                yield return null;

                MapItem.enKind kind = (MapItem.enKind)spotData.SpotKindCode;
                //Nonは無視
                if (kind == MapItem.enKind.Non) continue;

                GameObject go = Instantiate(MapViewControl.MapItemOrg, itemsTran);

                Vector2 defaultPos = MapViewControl.CalcPosDisp(spotData.GPSPos);
                MapItem item = go.GetComponent<MapItem>();
                int id = spotData.ID;

                int imageIndex = spotData.SpotImageIndex;
                if (kind == MapItem.enKind.Start)
                {
                    Sprite[] icons = { null, null, IconSprite_Start };
                    item.Init(kind, icons);
                }
                else if (kind == MapItem.enKind.Voice)
                {
                    Sprite[] icons = { null, null, null};
                    item.Init(kind, icons);
                }
                else
                {
                    //int nIndex = id % (IconSprite_n.Length);
                    Sprite[] icons = { IconSprite_n[spotData.NIconColorIndex]
                            , IconSprite_r[imageIndex], IconSprite_w[imageIndex] };
                    item.Init(kind,icons);
                }
                item.ID = id;
                go.name = "Item" + id.ToString("000");
                item.defaultPos = defaultPos;
                item.GPSPos = spotData.GPSPos;
                item.SpotName = spotData.SpotName;
                item.Kind = kind;
                item.SpotData = spotData;

                go.GetComponent<RectTransform>().anchoredPosition = defaultPos;
                MapViewControl.MapItemsAdd(item);
            }
            MapViewControl.IsReadyMapItems = true;
            MapViewControl.SeachItem();
        }


        IEnumerator coCreateRoutePoints()
        {
            List<Vector2> points = new List<Vector2>();
            for (int i = 0; i < RoutePoint.Data.Count; i++)
            {
                Vector2 defaultPos = MapViewControl.CalcPosDisp(RoutePoint.Data[i].GPSPos);
                RoutePoint.Data[i].ChkPointPos = defaultPos;
                points.Add(defaultPos);
                defaultPos = MapViewControl.CalcPosDisp(RoutePoint.Data[i].EndGPSPos);
                points.Add(defaultPos);
            }
            yield return null;
            MapViewControl.Route.Init(points);
        }
    }
}
