﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    [RequireComponent(typeof(CanvasRenderer))]
    [RequireComponent(typeof(RectTransform))]
    public class RouteMesh : Graphic
    {
        public float width;
        private Vector2 _defaultSize;
        private List<Vector2> _points;
        private RectTransform parentRectTran;

        public void Init(List<Vector2> points)
        {
            parentRectTran = this.transform.parent.GetComponent<RectTransform>();
            _defaultSize = this.parentRectTran.sizeDelta;
            _points = points;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            Color tmpColor;

            if (_points == null) return;
            //            for (int i = 0; i < _points.Count - 1; i++)
            //逆順で描画
            for (int i = _points.Count - 2; i >= 0; i--)
            {

                if (GameManager.ActiveUser.RouteFlg[i] == false)
                {
                    tmpColor = Color.gray;
                }
                else if (GameManager.ActiveUser.RouteFlg[i+1] == false)
                {
                    tmpColor = Color.gray;
                }
                //else if (GameManager.ActiveUser.RouteFlg[i - 1] == false)
                //{
                //    tmpColor = Color.gray;
                //}
                else
                {
                    tmpColor = color;
                }

                UIVertex[] v = new UIVertex[4];
                for (int k = 0; k < 4; k++)
                {
                    v[k] = UIVertex.simpleVert;
                    v[k].color = tmpColor;
                }

                //if (i == 0)
                //{
                //    Vector2 vec = _points[i + 1] - _points[i];
                //    Vector2 vec2 = new Vector2(vec.y, vec.x * -1f).normalized * width;
                //    v[0].position = convZoomedPos(_points[i] + vec2);
                //    v[1].position = convZoomedPos(_points[i] - vec2);
                //}
                //else
                //{
                //    Vector2 vec2 = getWidthVec(_points[i-1], _points[i], _points[i+1]);
                //    v[0].position = convZoomedPos(_points[i] + vec2);
                //    v[1].position = convZoomedPos(_points[i] - vec2);
                //}


                //if (i == _points.Count - 2)
                //{
                //    Vector2 vec = _points[i + 1] - _points[i];
                //    Vector2 vec2 = new Vector2(vec.y, vec.x * -1f).normalized * width;
                //    v[2].position = convZoomedPos(_points[i + 1] - vec2);
                //    v[3].position = convZoomedPos(_points[i + 1] + vec2);
                //}
                //else
                //{
                //    Vector2 vec2 = getWidthVec(_points[i], _points[i+1], _points[i+2]);
                //    v[2].position = convZoomedPos(_points[i+1] - vec2);
                //    v[3].position = convZoomedPos(_points[i+1] + vec2);
                //}

                //
                Vector2 vec = _points[i + 1] - _points[i];
                Vector2 vec2 = new Vector2(vec.y, vec.x * -1f).normalized * width;
                v[0].position = convZoomedPos(_points[i] + vec2);
                v[1].position = convZoomedPos(_points[i] - vec2);
                v[2].position = convZoomedPos(_points[i+1] - vec2);
                v[3].position = convZoomedPos(_points[i+1] + vec2);


                vh.AddUIVertexQuad(new UIVertex[] {v[0], v[1], v[2], v[3]});
            }
        }

        private Vector2 convZoomedPos(Vector2 pos)
        {
            return new Vector2(
                pos.x * this.parentRectTran.sizeDelta.x / _defaultSize.x
               , pos.y * this.parentRectTran.sizeDelta.y / _defaultSize.y
               );
        }

        private Vector2 getWidthVec(Vector2 pos1, Vector2 pos2, Vector2 pos3)
        {

            Vector2 vec1 = pos1 - pos2;
            Vector2 vec2 = pos3 - pos2;

            float ang = Vector2.Angle(vec1, vec2) / 2f;
            //Debug.Log(ang);
            float radian = ang * Mathf.PI / 180f;
            Vector2 tmpVec = new Vector2(Mathf.Cos(radian) * width, Mathf.Sin(radian) * width);

            return(tmpVec);
        }

        public void Redraw()
        {
            UpdateGeometry();
        }
    }
}