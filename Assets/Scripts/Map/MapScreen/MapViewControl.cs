﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class MapViewControl : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
    {
        public RectTransform MapRectTran;
        public GameObject MapScreen;

        //GPS座標：表示位置の変換の基準を得るための点
        public RectTransform BasisPos1;
        public RectTransform BasisPos2;
        private Vector2 basePos1GPS;
        private Vector2 basePos1DispPos;
        private Vector2 distanceRate;

        public RectTransform CarPosRectTran;
        public RouteLine Route;

        //標準ズーム倍率での車表示アンカード位置
        private Vector2 carDefaultPos;
        public Vector2 CarGPSPos;

        //マップアイテムインスタンス元
        public GameObject MapItemOrg;

        //ズーム倍率
        private float zoom;
        public Vector2 DefaultSize;

        private List<MapItem> mapItems = new List<MapItem>();
        public bool IsReadyMapItems = false;

        //ドラッグでマップを動かす操作を行っている最中か
        public bool IsPointerDown = false;
        private float pointerWait;

        private int sizeTweenID = -1;
        private int moveTweenID = -1;

        //マップ表示範囲サイズ
        private Vector2 thisSize;

        //ゲーム設定インスタンス
        private GameSetting gs;

        /// <summary>
        /// 初期処理
        /// </summary>
        public void Init()
        {
            gs = GameManager.Instance.GameSetting;

            //初期化
            thisSize = this.GetComponent<RectTransform>().sizeDelta /2f;
            zoom = 1.0f;
            DefaultSize = MapRectTran.sizeDelta;


            //基準座標取得
            basePos1DispPos = BasisPos1.anchoredPosition;
            //basePos1GPS = new Vector2(141.306296f, 43.079951f);
            //Vector2 basePos2GPS = new Vector2(141.303271f, 43.076176f);
            basePos1GPS = BasisPos1.GetComponent<BasisPos>().GPSPos;
            Vector2 basePos2GPS = BasisPos2.GetComponent<BasisPos>().GPSPos;

            //変換割合算出
            distanceRate
                = calcDistanceRate(basePos1GPS, basePos2GPS, basePos1DispPos, BasisPos2.anchoredPosition);

        }

        void OnEnable()
        {
            pointerWait = 0f;
            IsPointerDown = false;
        }

        //void Start()
        //{
        //    //マップアイテム作成
        //    StartCoroutine(CreateItem());
        //}

        internal void MapItemsAdd(MapItem item)
        {
            mapItems.Add(item);
        }

        ///// <summary>
        ///// マップアイテム作成
        ///// </summary>
        ///// <returns></returns>
        //IEnumerator CreateItem()
        //{
        //    while (!MasterDataLoader.isLoaded)
        //    {
        //        yield return null;
        //    }

        //    Transform itemsTran = MapRectTran.transform.Find("Items");
        //    foreach (MapEventSpot spotData in MapEventSpot.Data)
        //    {
        //        yield return null;

        //        MapItem.enKind kind = (MapItem.enKind)spotData.SpotKindCode;
        //        //Nonは無視
        //        if (kind == MapItem.enKind.Non) continue;

        //        GameObject go = Instantiate(MapItemOrg, itemsTran);

        //        Vector2 defaultPos = CalcPosDisp(spotData.GPSPos);
        //        MapItem item = go.GetComponent<MapItem>();
        //        item.Init();
        //        item.ID = spotData.ID;
        //        go.name = "Item" + item.ID.ToString("000");
        //        item.defaultPos = defaultPos;
        //        item.GPSPos = spotData.GPSPos;
        //        item.SpotName = spotData.SpotName;
        //        item.Kind = (MapItem.enKind)spotData.SpotKindCode;
        //        item.SpotData = spotData;

        //        go.GetComponent<RectTransform>().anchoredPosition = defaultPos;
        //        mapItems.Add(item);
        //    }
        //    isReadyMapItems = true;
        //    SeachItem();
        //}


        /// <summary>
        /// 基準座標2点からGPS座標→表示座標への変換割合を算出する
        /// </summary>
        /// <param name="pos1GPS"></param>
        /// <param name="pos2GPS"></param>
        /// <param name="pos1Disp"></param>
        /// <param name="pos2Disp"></param>
        /// <returns></returns>
        private Vector2 calcDistanceRate(Vector2 pos1GPS, Vector2 pos2GPS, Vector2 pos1Disp, Vector2 pos2Disp)
        {
            float tmpX = calcRateSub(pos1GPS.x, pos2GPS.x, pos1Disp.x, pos2Disp.x);
            float tmpY = calcRateSub(pos1GPS.y, pos2GPS.y, pos1Disp.y, pos2Disp.y);
            return (new Vector2(tmpX, tmpY));
        }

        float calcRateSub(float start,float end, float startDisp, float endDisp)
        {
            float diff = start - end;
            float diffDisp = startDisp - endDisp;
            return  diffDisp / diff;
        }

        /// <summary>
        /// GPS座標から表示座標を取得する
        /// </summary>
        /// <param name="posGPS">取得対象のGPS座標</param>
        /// <returns></returns>
        public Vector2 CalcPosDisp(Vector2 posGPS)
        {
            float tmpX = calcPosDispSub(posGPS.x, basePos1GPS.x, basePos1DispPos.x, distanceRate.x);
            float tmpY = calcPosDispSub(posGPS.y, basePos1GPS.y, basePos1DispPos.y, distanceRate.y);
            return new Vector2(tmpX, tmpY);
        }

        float calcPosDispSub(float value, float startGPS, float start, float rate)
        {
            float tmp = (startGPS - value) * rate;
            return start - tmp;
        }


        /// <summary>
        /// 車の座標をセットする
        /// </summary>
        /// <param name="la"></param>
        /// <param name="lo"></param>
        public void SetCarPos(float la,float lo)
        {
            //chkRoutePoint();
            CarGPSPos = new Vector2(lo, la);
            carDefaultPos = CalcPosDisp(CarGPSPos);
            if (GameManager.Instance.GameSetting.StopAutoUpdate != 1)
            {
                Route.RouteHitChk.Chk(carDefaultPos);
            }

            if (LeanTween.isTweening(sizeTweenID)) return;

            //Debug.Log("SetCarPos la:" + la + " lo:" + lo);
            if(!CarPosRectTran.gameObject.activeSelf) CarPosRectTran.gameObject.SetActive(true);
            //Debug.Log("carDefaultPos:" + carDefaultPos + " gps:" + carGPSPos.x + " " + carGPSPos.y);
            CarPosRectTran.anchoredPosition = carDefaultPos * zoom;

            //if (MapScreen.activeSelf)
            //{
                //StartCoroutine(SeachItem());
                SeachItem();
                if (!IsPointerDown)
                {
                    if (MapScreen.activeSelf)
                    {
                        StartCoroutine(ViewScrollFix());
                    }
                }
            //}
        }

        private void chkRoutePoint()
        {
            for(int i=0;i<RoutePoint.Data.Count;i++)
            {
                if (GameManager.ActiveUser.RouteFlg[i] == false)
                {
                    float d = (CarGPSPos - RoutePoint.Data[i].GPSPos).magnitude;
                    //Debug.Log("d:" + d + " i:" + i);
                    if (d < 0.0003f)
                    {
                        GameManager.ActiveUser.RouteFlg[i] = true;
                        Route.Redraw();
                        //GameManager.SaveData();
                    }
                }
            }
        }


        /// <summary>
        /// マップアイテムと車との距離を調べて表示の切り替えやイベントの発生
        /// </summary>
        /// <returns></returns>
        public void SeachItem()
        {
            //Debug.Log("--SeachItem--");
            if (!IsReadyMapItems)
            {
                //yield break;
                return;
            }

            if (MapManager.Instance.Warikomi.gameObject.activeSelf) return;
            if (!GameManager.EventSystemEnable) return;

            foreach (MapItem item in mapItems)
            {
                item.CalcMag(CarGPSPos);
            }

            mapItems.Sort(MapItem.CompareByMag);

            float waitCount = 0.01f;

            //bool hit = false;
            foreach (MapItem item in mapItems)
            {
                //Debug.Log("item.ID:" + item.ID);
                if(item.State == MapItem.enState.End)
                {
                    continue;
                }
                else if (GameManager.ActiveUser.ClearFlgs[item.ID] == 1)
                {
                    item.DoEnd(waitCount);
                }
                else
                {
                    float tmp = item.Magnitude;
                    if (tmp < gs.SpotTyakusinThreshold)
                    {
                        if (MapManager.WarikomiWaitTime <= 0f)
                        {
                            if (item.Kind == MapItem.enKind.Voice)
                            {
                                MapManager.Instance.DoVoiceEvent(item);
                            }
                            else
                            {
                                MapManager.Instance.WarikomiShow(item);
                                return;
                            }
                        }
                    }

                    if(item.Kind!= MapItem.enKind.Voice)
                    {
                        if (tmp < gs.SpotDispThreshold)
                        {
                            item.Near(waitCount);
                        }
                        else
                        {
                            item.Show(waitCount);
                        }
                    }

                }
                waitCount += 0.08f;
            }
        }

        /// <summary>
        /// 車が表示範囲外にある場合、マップ位置を補正
        /// </summary>
        /// <returns></returns>
        IEnumerator ViewScrollFix()
        {
            yield return null;
            if (LeanTween.isTweening(sizeTweenID)) yield break;

            Vector2 carVec = CarPosRectTran.anchoredPosition;
            Vector2 mapVec = MapRectTran.anchoredPosition;
            bool chk = false;
            float tmp;
            if (ViewScrollFixSub(carVec.x,mapVec.x, thisSize.x, out tmp))
            {
                chk = true;
                mapVec.x = tmp;
            }
            if (ViewScrollFixSub(carVec.y, mapVec.y, thisSize.y, out tmp))
            {
                chk = true;
                mapVec.y = tmp;
            }
            yield return null;
            if (LeanTween.isTweening(sizeTweenID)) yield break;

            if (chk)
            {
                this.GetComponent<ScrollRect>().StopMovement();
                //MapRectTran.anchoredPosition = mapVec;
                moveTweenID = LeanTween.move(MapRectTran, mapVec, 1f).setEaseOutCubic().id;
            }
        }

        /// <summary>
        /// マップ位置補正処理サブ処理
        /// </summary>
        /// <param name="car">車座標</param>
        /// <param name="map">マップ本体座標</param>
        /// <param name="view">表示範囲</param>
        /// <param name="fix">補正されたマップ本体座標</param>
        /// <returns></returns>
        private bool ViewScrollFixSub(float car, float map,float view,out float fix)
        {
            const float yoyuu = 200f;

            if (car - yoyuu < -view - map)
            {
                fix = -(car - yoyuu) - view;
                return true;
            }
            else if (car + yoyuu > view - map)
            {
                fix = -(car + yoyuu) + view;
                return true;
            }
            fix = 0f;
            return false;
        }

 
        public void ZoomUp()
        {
            if(zoom < 5.0f)
            {
                LeanTween.cancel(moveTweenID);
                zoomChange(zoom + 0.3f);
                LeanTween.move(MapRectTran, -carDefaultPos * zoom, 0.5f);
            }
        }

        public void ZoomDown()
        {
            if (zoom > 0.5f)
            {
                LeanTween.cancel(moveTweenID);
                float oldZoom = zoom;
                zoomChange(zoom - 0.3f);
                LeanTween.move(MapRectTran, MapRectTran.anchoredPosition / oldZoom * zoom, 0.5f);
            }
        }

        /// <summary>
        /// ズーム変更
        /// </summary>
        /// <param name="newZoom">倍率</param>
        private void zoomChange(float newZoom)
        {
            this.GetComponent<ScrollRect>().StopMovement();

            const float tweenTime = 0.5f;
            sizeTweenID = LeanTween.size(MapRectTran, DefaultSize * newZoom, tweenTime).id;
            LeanTween.scale(Route.gameObject, Vector2.one * newZoom, tweenTime);

            foreach (MapItem item in mapItems)
            {
                LeanTween.move(item.GetComponent<RectTransform>()
                    , item.defaultPos * newZoom, tweenTime);
            }
            LeanTween.move(CarPosRectTran , carDefaultPos * newZoom, tweenTime);

            //Route.Redraw(newZoom);
            //Route.zoom = newZoom;
            //Route.Redraw();
            zoom = newZoom;
        }

        /// <summary>
        /// 押下時処理
        /// 　ドラッグでマップを動かす操作をされたとみなす
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            IsPointerDown = true;
            LeanTween.cancel(moveTweenID);
        }

        /// <summary>
        /// 押下終了時処理
        /// 　ドラッグでマップを動かす操作で指が離れたとみなす
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            //指が離れてから2秒間は操作中扱いとする
            pointerWait = 2.0f;
        }

        void Update()
        {
            if(pointerWait > 0f)
            {
                pointerWait -= Time.deltaTime;
                if (pointerWait <= 0f)
                {
                    IsPointerDown = false;
                }
            }
        }

        void OnDestroy()
        {
            MapRectTran.GetComponent<Image>().sprite = null;
        }
    }
}
