﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class MapItem : MonoBehaviour,IPointerClickHandler
    {
        public Detail ItemDetail;


        public int ID;

        public string SpotName;
        public Vector2 defaultPos;
        public Vector2 GPSPos;

        public enum enState { Show, Near, Hide, End };
        public enState State = enState.Hide;

        public enum enKind { Non = -1, Start = 0, AR = 1 ,Kokon = 2,Talk=3, Guide=4 ,Voice=5};
        public enKind Kind;

        private GameObject activeGO;
        private GameObject nGO;
        private GameObject wGO;
        //private GameObject farGO;

        public MapEventSpot SpotData;

        public float Magnitude;
        //void Start()
        //{
        //    init();
        //}
        private int tweenID = -1;

        public void Init(enKind kind, Sprite[] iconSprite)
        {
            //if (activeGO) return;
            nGO = this.transform.Find("NonActive").gameObject;
            nGO.GetComponent<Image>().sprite = iconSprite[0];
            nGO.SetActive(false);
            activeGO = this.transform.Find("Active").gameObject;
            activeGO.GetComponent<Image>().sprite = iconSprite[1];
            activeGO.SetActive(false);
            wGO = this.transform.Find("End").gameObject;
            wGO.GetComponent<Image>().sprite = iconSprite[2];
            wGO.SetActive(false);
            //farGO = this.transform.Find("Far").gameObject;
            //farGO.SetActive(false);
            this.gameObject.SetActive(true);

            this.Kind = kind;
            if (kind == enKind.Start)
            {
                State = enState.End;
                wGO.GetComponent<Image>().SetNativeSize();
                wGO.transform.localScale = Vector2.one;
                RectTransform rectTran = wGO.GetComponent<RectTransform>();
                rectTran.pivot = new Vector2(0.5f, 0.5f);
                rectTran.anchoredPosition = Vector2.zero;
                wGO.SetActive(true);
                //wGO.transform.localScale = Vector2.one * 0.5f;
            }
            else
            {
                State = enState.Hide;
            }
        }

        public void CalcMag(Vector2 carGPSPos)
        {
            Magnitude = (carGPSPos - GPSPos).magnitude;
        }

        private float waitCount = 0f;
        void Update()
        {
            if (waitCount > 0f)
            {
                waitCount -= Time.deltaTime;
                if(waitCount <= 0f)
                {
                    switch (State)
                    {
                        case enState.Near:
                            activeGO.SetActive(true);
                            nGO.SetActive(false);
                            wGO.SetActive(false);
                            activeGO.transform.localScale = Vector2.one * 0.2f;
                            LeanTween.cancel(tweenID);
                            tweenID = LeanTween.scale(activeGO, Vector2.one, 0.5f).setEaseOutBack().id;
                            break;

                        case enState.Show:
                            //Debug.Log("gameo" + gameObject.name);
                            activeGO.SetActive(false);
                            wGO.SetActive(false);
                            nGO.SetActive(true);
                            nGO.transform.localScale = Vector2.one * 0.2f;
                            LeanTween.cancel(tweenID);
                            tweenID = LeanTween.scale(nGO, Vector2.one, 0.5f).setEaseOutBack().id;
                            break;

                        case enState.End:
                            //Debug.Log("gameo" + gameObject.name);
                            activeGO.SetActive(false);
                            nGO.SetActive(false);
                            wGO.SetActive(true);
                            wGO.transform.localScale = Vector2.one * 0.2f;
                            LeanTween.cancel(tweenID);
                            tweenID = LeanTween.scale(wGO, Vector2.one, 0.5f).setEaseOutBack().id;
                            break;
                            
                    }
                }
            }
        }

        public void Near(float waitCount)
        {
            if (State == enState.Near) return;
            SoundControl.PlaySound(2);
            State = enState.Near;
            if (SpotData.SpotImageIndex < 0) return;
            this.waitCount = waitCount;
            //Debug.Log("Near " + gameObject.name);
        }

        public void Show(float waitCount)
        {
            if (State == enState.Show) return;
            State = enState.Show;
            if (SpotData.SpotImageIndex < 0) return;
            this.waitCount = waitCount;
            //Debug.Log("Show" + gameObject.name);
        }

        public void DoEnd(float waitCount)
        {
            if (State == enState.End) return;
            State = enState.End;
            if (SpotData.SpotImageIndex < 0) return;
            this.waitCount = waitCount;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (SpotData.BookIndex < 0) return;
            ItemDetail.Show(this);
        }

        public void ShowDetail()
        {
            if (SpotData.BookIndex < 0) return;
            ItemDetail.Show(this);
        }


        public static int CompareByMag(MapItem a, MapItem b)
        {
            if (a.Magnitude > b.Magnitude)
            {
                return 1;
            }

            if (a.Magnitude < b.Magnitude)
            {
                return -1;
            }

            return 0;
        }
    }
}
