﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using Vectrosity;

namespace ManabuDriveApp.Map
{
    //[RequireComponent(typeof(Vectrosity.VectorObject2D))]

    /// <summary>
    /// ルート進行表示
    /// ※線分は後の線が上に描かれるため、座標リストには進行ルート逆順で格納する（csvの時点で逆順データとすること）
    /// 
    /// </summary>
    public class RouteLine : MonoBehaviour
    {
        //private Vector2[] _points;
        VectorObject2D _Vectrosity;

        private VectorLine line;

        [SerializeField]
        private Color OKColor;

        [SerializeField]
        private Color NonColor;

        public RouteHitChk RouteHitChk;

        public void Init(List<Vector2> pointList)
        {
            //List<Vector2> pointList = new List<Vector2>(points);
            //_points = points;

            _Vectrosity = this.GetComponent<VectorObject2D>();
            line = _Vectrosity.vectorLine;
            line.points2.Clear();
            line.points2= pointList;

            line.Draw();
            setLineColor();
            _Vectrosity.enabled = true;

            RouteHitChk.Init(this,pointList);
        }

        public void setLineColor()
        {
            if (line == null) return;

//            line.SetColor(new Color(1f, 1f, 1f, 1f));
            line.SetColor(this.NonColor);
            //Debug.Log("Length:" + line.lineVertices.Length);
            for (int i = 0; i < GameManager.ActiveUser.RouteFlg.Length - 1; i++)
            {
                if (GameManager.ActiveUser.RouteFlg[i] == false)
                {
                }
                else if (GameManager.ActiveUser.RouteFlg[i + 1] == false)
                {
                }
                else
                {
                    line.SetColor(OKColor, i);
                }
            }
        }

        public void setLineColor(int index)
        {
            bool needDraw = false;
            if (line == null) return;
            if(index != 0)
            {
                if (GameManager.ActiveUser.RouteFlg[index])
                {
                    if (GameManager.ActiveUser.RouteFlg[index-1])
                    {
                        line.SetColor(OKColor, index-1);
//                        line.lineColors[index-1] = OKColor;
                        needDraw = true;
                        Debug.Log(" 1 index:" + index);
                    }
                }
            }

            if (index != GameManager.ActiveUser.RouteFlg.Length-1)
            {
                if (GameManager.ActiveUser.RouteFlg[index])
                {
                    if (GameManager.ActiveUser.RouteFlg[index + 1])
                    {
//                        line.lineColors[index] = OKColor;
                        line.SetColor(OKColor, index);
                        //line.colo
                        //needDraw = true;
                        Debug.Log(" 2 index:" + index);
                    }
                }
            }

            if (needDraw)
            {
//                line.smoothColor = true;
//                line.smoothColor = false;
//                line.Draw();
            }
        }

        public void Redraw()
        {

        }
    }
}