﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    /// <summary>
    /// 進行ルート通過チェック用空間
    /// 非uGUI空間に、GPS座標から変換した表示用座標でコライダ付きルート座標を展開する
    /// 通過済みの座標は作らない。および、通過済みとなった際にDestroyする。
    /// </summary>
    public class RouteHitChk : MonoBehaviour
    {
        private RouteLine route;

        [SerializeField]
        private GameObject pointOrg;

        private List<Vector2> points;

        private bool isFirst = true;
        private Vector2 lastChkPos;

        public void Init(RouteLine route, List<Vector2> points)
        {
            isFirst = true;
            this.route = route;
            this.points = points;
            Reset();
        }

        public void Reset()
        {
            Debug.Log("RouteHitChk Reset");
            Transform tranPoints = this.transform.Find("Points");
            if (tranPoints)
            {
                Destroy(tranPoints.gameObject);
            }

            GameObject goNewPoints = new GameObject("Points");
            tranPoints = goNewPoints.transform;
            tranPoints.SetParent(this.transform);

            bool[] RouteFlg = GameManager.ActiveUser.RouteFlg;
            for (int i=0;i< RoutePoint.Data.Count;i++)
            {
                if (!RouteFlg[i])
                {
                    GameObject go = GameObject.Instantiate(pointOrg, RoutePoint.Data[i].ChkPointPos, Quaternion.identity, tranPoints);
                    go.GetComponent<RouteHitChkPoint>().RouteIndex = i;
                }
            }
        }

        public void Chk(Vector2 newPos)
        {
            if (isFirst)
            {
                isFirst = false;
                lastChkPos = newPos;
                return;
            }
            else if(newPos == lastChkPos)
            {
                return;
            }

            //RaycastHit2D[] hits = Physics2D.LinecastAll(lastChkPos, newPos,9);
            int layerMask = 1 << 9;
            RaycastHit2D[] hits = Physics2D.LinecastAll(lastChkPos, newPos, layerMask);

            foreach (RaycastHit2D hit in hits)
            {
                GameObject go = hit.collider.gameObject;
                int idx = go.GetComponent<RouteHitChkPoint>().RouteIndex;
                GameManager.ActiveUser.RouteFlg[idx] = true;
                route.setLineColor(idx);
                Destroy(go);
            }
//            Debug.Log("hitc:" + hits.Length + " lastChkPos:" +lastChkPos + " n:"+ newPos);
            lastChkPos = newPos;
        }
    }
}
