﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class Clouds : MonoBehaviour
    {
        [SerializeField]
        private GameObject cloudOrg;

        private const int NUM = 10;

        void Start()
        {
            RectTransform ContentRectTran = this.transform.parent.GetComponent<RectTransform>();
            Vector2 contentSize = ContentRectTran.sizeDelta;
            float ySpan = contentSize.y / NUM;
            float yTop = (contentSize.y * ContentRectTran.pivot.y);

            float rangeMinX = -contentSize.x * ContentRectTran.pivot.x;
            float rangeMaxX = contentSize.x * (1f-ContentRectTran.pivot.x);

            for (int i=0;i< NUM; i++)
            {
                float tmpYStart = yTop - (i * ySpan);
                GameObject go = GameObject.Instantiate(cloudOrg, this.transform);
                Vector2 pos = new Vector2(UnityEngine.Random.Range(rangeMinX, rangeMaxX)
                    , tmpYStart - UnityEngine.Random.Range(0f, ySpan));
                go.GetComponent<CloudObject>().Init(tmpYStart, ySpan,pos);
            }

            cloudOrg.SetActive(false);
        }


    }
}
