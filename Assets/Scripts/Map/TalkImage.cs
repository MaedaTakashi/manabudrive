﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class TalkImage : MonoBehaviour, IPointerClickHandler
    {
        public bool ClickToGotoEventScene = false;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (ClickToGotoEventScene)
            {
                MapManager.Instance.GotoEventScene();
            }
        }
    }
}
