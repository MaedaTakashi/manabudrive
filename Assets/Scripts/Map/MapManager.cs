﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    /// <summary>
    /// マップ画面制御
    /// </summary>
    public class MapManager : MonoBehaviour
    {

        #region Singleton
        private static MapManager instance;
        public static MapManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (MapManager)FindObjectOfType(typeof(MapManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        public GameObject MapScreen;
        public Warikomi Warikomi;
        public CollectionScreen CollectionScreen;
        public TextbookScreen TextbookScreen;
        public GameObject DebugPanel;
        
        public Text GPSDispText;
        public Toggle DemoToggle;
        public DemoPlayer DemoPlayer;
        public MapViewControl MapViewControl;

        private GPSControl GPSControl;

        //着信拒否後の待ち時間カウント
        private float _WarikomiWaitTime = 0f;
        public static float WarikomiWaitTime
        {
            set
            {
                instance._WarikomiWaitTime = value;
            }
            get
            {
                return instance._WarikomiWaitTime;
            }
        }

        private bool nowLoading = false;

        void Start()
        {
            MapViewControl.Init();
            this.GetComponent<MapViewLoader>().Load(MapViewControl);

            GPSControl = this.GetComponent<GPSControl>();
            if (GameManager.IsDemoPlay)
            {
                DemoToggle.isOn = true;
            }

            _WarikomiWaitTime = GameManager.Instance.GameSetting.SpotReTyakusinTime;

            //アクティブなイベント地点情報を保持している場合はその画面が終了して戻ってきた状況
            if (GameManager.ActiveMapEventSpot == null)
            {
                ShowMap();
            }
            else
            {
                //クリアで追加される教科書がある場合は教科書画面を開く
                int tmp = GameManager.ActiveMapEventSpot.CrearBookIndex;
                if (tmp < 0)
                {
                    ShowMap();
                }
                else
                {
                    GameManager.ActiveUser.BookFlgs[tmp] = 1;
                    ShowTextbook(tmp, true, TextbookScreen.enBackTo.Collection,false);
                    //GameManager.ActiveMapEventSpot = null;
                }
            }

            if (GameManager.Instance.GameSetting.StopAutoUpdate == 1)
            {
                float x = GameManager.Instance.GameSetting.GPSX;
                float y = GameManager.Instance.GameSetting.GPSY;
                if ((y>35.4f)&&(y<35.5f)&& (x > 139.6f) && (y < 139.7f))
                {
                    Debug_UpdateGPS(y,x);
                }
            }

            SoundControl.PlayBGM("NISSAN_KIDSDRIVE_M01");
        }

        void Update()
        {
            if (_WarikomiWaitTime > 0f)
            {
                _WarikomiWaitTime -= Time.deltaTime;
            }
        }

        public void ClickButton(int value)
        {
            if (nowLoading) return;
            switch (value)
            {
                case 1:
                    GameManager.ActiveMapEventSpot = MapEventSpot.Data[2];
                    ScheneSwitcher.LoadScene("KokinSyasin", 0.5f);
                    break;

                case 2:
                    GameManager.ActiveMapEventSpot = MapEventSpot.Data[3];
                    ScheneSwitcher.LoadScene("Chat", 0.5f);
                    break;

                case 9:
                    ScheneSwitcher.LoadScene("SpeakerTest", 0.5f);
                    break;

                case 10:
                    ScheneSwitcher.LoadScene("ARKurabe");
                    break;

                case 11:
                    //着信拒否
                    MapManager.WarikomiWaitTime = GameManager.Instance.GameSetting.SpotReTyakusinTime;
                    ShowMap();
                    MapViewControl.SeachItem();
                    break;

                case 12:
                    ScheneSwitcher.LoadScene("Syasin",0.5f);
                    break;

                case 20:
                    GotoEventScene();
                    break;

                case 21:
                    SoundControl.PlaySound(7);
                    ShowCollection(true);
                    break;

                case 22:
                    SoundControl.PlaySound(9);
                    ShowMap();
                    break;

            }
        }

        /// <summary>
        /// 着信画面表示
        /// </summary>
        /// <param name="item"></param>
        public void WarikomiShow(MapItem item)
        {
            if (nowLoading) return;
            GameManager.EventSystemEnable = false;
            StartCoroutine(coWarikomiShow(item));
        }

        private IEnumerator coWarikomiShow(MapItem item)
        {
            GameManager.ActiveMapEventSpot = item.SpotData;
            Warikomi.Show(item.SpotData);

            yield return new WaitForSeconds(0.5f);
            MapScreen.SetActive(false);
            TextbookScreen.gameObject.SetActive(false);
            CollectionScreen.Close(false);
        }

        /// <summary>
        /// マップ上ボイスイベント
        /// </summary>
        /// <param name="item"></param>
        public void DoVoiceEvent(MapItem item)
        {
            if (nowLoading) return;
            GameManager.EventSystemEnable = false;
            StartCoroutine(coVoiceEvent(item));
        }

        /// <summary>
        /// マップ上ボイスイベントコルーチン
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private IEnumerator coVoiceEvent(MapItem item)
        {
            GameManager.ActiveMapEventSpot = item.SpotData;
            if (!MapScreen.activeSelf)
            {
                MapScreen.SetActive(true);
            }
            if (CollectionScreen.gameObject.activeSelf)
            {
                CollectionScreen.Close(false);
            }
            if (TextbookScreen.gameObject.activeSelf)
            {
                TextbookScreen.gameObject.SetActive(false);
            }
            yield return null;


            string path;
            switch (item.SpotData.ID)
            {
                case 15:
                    path = "Voice/st026";
                    break;

                default:
                    yield break;
            }

            SoundControl.PlaySound(path,3);
            yield return new WaitForSeconds(0.5f);

            AudioSource audio = SoundControl.GetSESource(3);
            while (true)
            {
                yield return new WaitForFixedUpdate();
                if (!audio.isPlaying)
                {
                    break;
                }
            }
            Debug.Log("マップ上ボイスイベントコルーチン終了");
            //遷移無しで保存
            GotoEventScene();
            GameManager.EventSystemEnable = true;
        }
        /// <summary>
        /// 教科書画面表示
        /// </summary>
        /// <param name="bookIndex"></param>
        /// <param name="isNew"></param>
        /// <param name="backTo"></param>
        public void ShowTextbook(int bookIndex,bool isNew,TextbookScreen.enBackTo backTo, bool withFade)
        {
            if (nowLoading) return;
            StartCoroutine(coShowTextbook(bookIndex,isNew,backTo,withFade));
        }

        /// <summary>
        /// 教科書画面表示コルーチン
        /// 　教科書画像のロード処理から順に行うためコルーチン化
        /// </summary>
        /// <param name="bookIndex"></param>
        /// <param name="isNew"></param>
        /// <param name="backTo"></param>
        /// <returns></returns>
        private IEnumerator coShowTextbook(int bookIndex, bool isNew, TextbookScreen.enBackTo backTo,bool withFade)
        {
            nowLoading = true;
            yield return null;

            string tmpPath = "Textbook/Textbook" + bookIndex.ToString("00");
            Debug.Log(tmpPath);
            Sprite tmpSprite = Resources.Load<Sprite>(tmpPath);
            Debug.Log(tmpSprite);
            if (tmpSprite != null)
            {
                TextbookScreen.TextBookImage.sprite = tmpSprite;
                TextbookScreen.TextBookImage.color = Color.white;
            }
            else
            {
                TextbookScreen.TextBookImage.sprite = null;
            }

            yield return null;

            //MapScreen.SetActive(false);
            //Warikomi.gameObject.SetActive(false);
            TextbookScreen.Show(bookIndex, isNew, backTo,withFade);
            nowLoading = false;
        }

        public void UnloadTextbook()
        {
            StartCoroutine(coUnloadTextbook());
        }

        /// <summary>
        /// 教科書画像のアンロード
        /// </summary>
        /// <returns></returns>
        public IEnumerator coUnloadTextbook()
        {
            yield return null;
            Resources.UnloadUnusedAssets();
        }

        /// <summary>
        /// 先生コレクション画面表示
        /// </summary>
        public void ShowCollection(bool withFade)
        {
            if (nowLoading) return;

            Warikomi.gameObject.SetActive(false);
            //TextbookScreen.gameObject.SetActive(false);
            CollectionScreen.Show(withFade);
            if (withFade)
            {
                StartCoroutine(coShowCollection());
            }
            else
            {
                MapScreen.SetActive(false);
            }
        }
        IEnumerator coShowCollection()
        {
            yield return new WaitForSeconds(0.5f);
            MapScreen.SetActive(false);
        }

        /// <summary>
        /// マップ画面表示
        /// </summary>
        public void ShowMap()
        {
            if (nowLoading) return;
            MapScreen.SetActive(true);
            //Warikomi.gameObject.SetActive(false);
            //TextbookScreen.gameObject.SetActive(false);
            CollectionScreen.Close(true);

            GameManager.EventSystemEnable = true;
        }

        /// <summary>
        /// 現在地の緯度経度座標を更新する
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <param name="isDemoData"></param>
        /// <param name="la"></param>
        /// <param name="lo"></param>
        public void UpdateGPS(double timeStamp,bool isDemoData, float la ,float lo)
        {
            if (GameManager.Instance.GameSetting.StopAutoUpdate == 1) return;

            MapViewControl.SetCarPos(la, lo);

            if (isDemoData)
            {
                GPSDispText.text = GetTimeStrDemo(timeStamp) + "    " + la + "," + lo;
            }
            else
            {
                GPSDispText.text = GetTimeStr(timeStamp) + " " + la + "," + lo;
            }
            //Debug.Log("UpdateGPS la:" + la + " lo:" + lo);
        }

        /// <summary>
        /// 座標文字表示
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <param name="la"></param>
        /// <param name="lo"></param>
        public void UpdateGPSText(double timeStamp, float la, float lo)
        {
            GPSDispText.text = GetTimeStrDemo(timeStamp) + "    " + la + "," + lo;
        }
        

        private string GetTimeStr(double timestamp)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp).ToLocalTime();
//            return (datetime.ToString("yyyy-MM-dd HH:mm:ss"));
            return (datetime.ToString("HH:mm:ss"));
        }

        private string GetTimeStrDemo(double timestamp)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp);
            //return (datetime.ToString("yyyy-MM-dd HH:mm:ss"));
            return (datetime.ToString("HH:mm:ss"));
        }


        /// <summary>
        /// イベント地点に応じたシーンへ遷移する
        /// 　現状はどれも通話シーン
        /// </summary>
        public void GotoEventScene()
        {
            MapEventSpot spotData = GameManager.ActiveMapEventSpot;
            //今のところ遷移時点でクリアフラグを立てる
            GameManager.ActiveUser.ClearFlgs[spotData.ID] = 1;
            GameManager.DemoTime = DemoPlayer.GetComponent<Slider>().value;


            switch (spotData.SpotKindCode)
            {
                case 1:
                    GameManager.SaveData("ARKurabe");
                    ScheneSwitcher.LoadScene("ARKurabe", 0.5f);
                    break;

                case 2:
                    GameManager.SaveData("KokinSyasin");
                    ScheneSwitcher.LoadScene("KokinSyasin", 0.5f);
                    break;

                case 3:
                    GameManager.SaveData("KokinSyasin");
                    ScheneSwitcher.LoadScene("KokinSyasin");
                    break;

                case 4:
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 5:
                    GameManager.SaveData("Map");
                    break;
            }
        }


        /// <summary>
        /// デバッグ
        /// デモデータ再生トグル変更時処理
        /// </summary>
        public void DemoToggleChange()
        {
            DemoPlayer.enabled = DemoToggle.isOn;
            GPSControl.enabled = !DemoToggle.isOn;
            if (!DemoToggle.isOn)
            {
                GameManager.IsDemoPlay = false;
            }
        }

        int debugDispState = 0;
        public void DebugDisp()
        {
            switch (debugDispState)
            {
                case 0:
                    debugDispState = 1;
                    DebugPanel.SetActive(false);
                    break;
                case 1:
                    debugDispState = 0;
                    DebugPanel.SetActive(true);
                    break;
            }
        }

        public void Debug_UpdateGPS(float la, float lo)
        {
            GameManager.ActiveUser.SetAllRouteFlg(false);
            MapViewControl.Route.setLineColor();
            MapViewControl.Route.RouteHitChk.Reset();

            _WarikomiWaitTime = 3f;
            MapViewControl.SetCarPos(la, lo);
            StartCoroutine(coDebug_UpdateGPS(la, lo));
        }
        IEnumerator coDebug_UpdateGPS(float la, float lo)
        {
            Debug.Log("re");
            yield return new WaitForSeconds(5f);
            MapViewControl.SetCarPos(la, lo);
        }

        public void RouteSet()
        {
            bool value = !GameManager.ActiveUser.RouteFlg[0];
            GameManager.ActiveUser.SetAllRouteFlg(value);
            //            MapViewControl.Route.Redraw();
            MapViewControl.Route.setLineColor();
            MapViewControl.Route.RouteHitChk.Reset();
        }
    }
}
