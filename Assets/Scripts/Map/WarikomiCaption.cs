﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class WarikomiCaption : MonoBehaviour
    {
        private TMPro.TextMeshProUGUI Caption;
        float timeCount;
        int State;

        void Start()
        {
            timeCount = 0f;
            State = 0;
            Caption = this.GetComponent<TextMeshProUGUI>();
        }

        void Update()
        {
            timeCount += Time.deltaTime;
            if(timeCount > 1.0f)
            {
                timeCount -= 1f;
                switch (State)
                {
                    case 0:
                        Caption.text = "プルプルプル.";
                        State = 1;
                        break;
                    case 1:
                        Caption.text = "プルプルプル..";
                        State = 2;
                        break;
                    case 2:
                        Caption.text = "プルプルプル...";
                        State = 0;
                        break;
                }
            }
        }

    }
}
