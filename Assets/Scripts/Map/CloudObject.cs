﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class CloudObject : MonoBehaviour
    {
        private const int NUM = 8;
        private const float SPD = 40f;

        private Vector2 defaultPos;
        private Vector2 defaultSize;

        private RectTransform thisRectTran;
        private Image thisImage;

        [SerializeField]
        private MapViewControl MapViewControl;
        private RectTransform ContentRectTran;

        private float baseY;
        private float ySpan;

        private float dispX;

        public void Init(float yStart,float ySpan,Vector2 startDefaultPos)
        {
            baseY = yStart;
            this.ySpan = ySpan;
            defaultPos = startDefaultPos;
        }

        void Start()
        {
            thisRectTran = this.GetComponent<RectTransform>();
            defaultSize = thisRectTran.sizeDelta;
            ContentRectTran = this.transform.parent.parent.GetComponent<RectTransform>();
            thisImage = this.GetComponent<Image>();

        }

        void Update()
        {
            defaultPos.x += Time.deltaTime * SPD;

            if(defaultPos.x > 
                (MapViewControl.DefaultSize.x * (1f-ContentRectTran.pivot.x)) + (defaultSize.x))
            {
                defaultPos.x -= MapViewControl.DefaultSize.x + defaultSize.x;
                defaultPos.y = baseY - UnityEngine.Random.Range(0, ySpan);
            }

            thisRectTran.anchoredPosition = defaultPos * (ContentRectTran.sizeDelta.x/ MapViewControl.DefaultSize.x);

            dispX = thisRectTran.anchoredPosition.x + ContentRectTran.anchoredPosition.x;
            if((dispX> -1536f/2f- defaultSize.x)&& (dispX < 1536f / 2f + defaultSize.x))
            {
                thisImage.enabled = true;
            }
            else
            {
                thisImage.enabled = false;
            }
        }


    }
}
