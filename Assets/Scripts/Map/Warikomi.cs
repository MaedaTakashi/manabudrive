﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class Warikomi : MonoBehaviour
    {
        [SerializeField]
        private TMPro.TextMeshProUGUI SpotName;
        [SerializeField]
        private TMPro.TextMeshProUGUI SpotNameTalkPanel;
        [SerializeField]
        private TMPro.TextMeshProUGUI Caption;

        [SerializeField]
        private Image SpotImage;

        [SerializeField]
        private Image KaoImage;

        [SerializeField]
        private Image SpotImage2;

        //[SerializeField]
        //private Animator CircleImageAnime;

        [SerializeField]
        private SizeMove CircleImageBG;
        [SerializeField]
        private SizeMove CircleImageMask;

        [SerializeField]
        private FadeEffect PanelFade;

        [SerializeField]
        private AnimationCurve StartCurve = new AnimationCurve();
        [SerializeField]
        private AnimationCurve LoopCurve = new AnimationCurve();

        [SerializeField]
        private TalkPanel TalkPanel;

        private bool isImageLoaded;

        void Start()
        {
            //動作確認用
            //StartCoroutine(coDebug());
        }


        IEnumerator coDebug()
        {
            while (!MasterDataLoader.isLoaded)
            {
                yield return null;
            }
            Show(MapEventSpot.Data[1]);
        }


        public void Show(MapEventSpot SpotData)
        {
            GameManager.EventSystemEnable = false;

            isImageLoaded = false;

            SpotName.text = SpotData.TeacherName + "先生";
//            SpotNameTalkPanel.text = SpotName.text;

            this.gameObject.SetActive(true);

            StartCoroutine(coStart(SpotData.TyakusinImageIndex));

            FadeEffect fade = this.GetComponent<FadeEffect>();
            fade.StartFadeIn(0.5f);

            StartCoroutine(coStartWait());
        }


        IEnumerator coStart(int imageIndex)
        {
            yield return StartCoroutine(LoadAsyncTextureCoroutine("Map/Warikomi/t" + imageIndex.ToString("00"),SpotImage));
            yield return StartCoroutine(LoadAsyncTextureCoroutine("Map/Warikomi/f" + imageIndex.ToString("00"),SpotImage2));
//            SpotImage2.sprite = SpotImage.sprite;
            isImageLoaded = true;
        }

        IEnumerator coStartWait()
        {
            yield return new WaitForSeconds(0.5f);

            while (!isImageLoaded)
            {
                yield return null;
            }

            SoundControl.PlaySound(13,0,true);

            const float START_TIME = 0.7f;

            //CircleImageAnime.Play("Start", 0, 0f);
            CircleImageBG.GetComponent<RectTransform>().sizeDelta = Vector2.one;
            CircleImageBG.GetComponent<Image>().enabled = true;
            CircleImageBG.MoveStart(Vector2.one* 532.48f, START_TIME, StartCurve);

            CircleImageMask.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
            CircleImageMask.GetComponent<Image>().enabled = true;
            CircleImageMask.MoveStart(Vector2.one * 509.76f, START_TIME, StartCurve);
            SpotImage.enabled = true;
            PanelFade.StartFadeIn(START_TIME);

            yield return new WaitForSeconds(START_TIME);

            CircleImageBG.MoveStart(Vector2.one * 452f, 0.75f, LoopCurve, SizeMove.enMode.Loop);
            CircleImageMask.MoveStart(Vector2.one * 429.28f, 0.75f, LoopCurve, SizeMove.enMode.Loop);

            GameManager.EventSystemEnable = true;
        }

        /// <summary>
        /// リソースからテクスチャを非同期でロードするコルーチン
        /// </summary>
        IEnumerator LoadAsyncTextureCoroutine(string filePath, Image img)
        {
            Debug.Log("LoadAsyncTextureCoroutine start:" + filePath);
            // リソースの非同期読込開始
            ResourceRequest resReq = Resources.LoadAsync<Sprite>(filePath);
            // 終わるまで待つ

            //Debug.Log("LoadAsyncTextureCoroutine resReq start");
            while (resReq.isDone == false)
            {
                //Debug.Log("Loading progress:" + resReq.progress.ToString());
                yield return 0;
            }
            //Debug.Log("LoadAsyncTextureCoroutine resReq end");
            img.color = Color.white;
            img.sprite = resReq.asset as Sprite;
            //Debug.Log("LoadAsyncTextureCoroutine end");
        }



        public void TelOn()
        {
            TalkPanel.Show();
            StartCoroutine(coTelOn());
        }

        IEnumerator coTelOn()
        {
            const float START_TIME = 0.5f;

            SoundControl.PlaySound(14);

            CircleImageBG.MoveStart(Vector2.one * 2600f, START_TIME);
            CircleImageMask.MoveStart(Vector2.one * 2577.28f, START_TIME);

            SpotImage.GetComponent<SizeMove>().MoveStart(new Vector2(1536f, 1536f), 0.3f, StartCurve);

            SpotImage2.GetComponent<FadeEffect>().StartFadeIn(0.3f);
            KaoImage.GetComponent<FadeEffect>().StartFadeIn(0.3f);

            SpotImage2.GetComponent<RectTransform>().sizeDelta = new Vector2(786f,786f);
            KaoImage.GetComponent<RectTransform>().sizeDelta = new Vector2(768f, 786f);

            SpotImage2.GetComponent<SizeMove>().MoveStart(new Vector2(1536f, 1536f), 0.3f,StartCurve);
            KaoImage.GetComponent<SizeMove>().MoveStart(new Vector2(1536f, 1536f), 0.3f, StartCurve);
            yield return null;
        }

        public void TelOff()
        {
            GameManager.EventSystemEnable = false;
            SoundControl.StopSound(0);
            StartCoroutine(coTelOff());
        }

        IEnumerator coTelOff()
        {
            const float START_TIME = 0.6f;

            CircleImageBG.MoveStart(Vector2.one, START_TIME, StartCurve);
            CircleImageMask.MoveStart(Vector2.one, START_TIME, StartCurve);
            PanelFade.StartFadeOut(START_TIME);

            yield return new WaitForSeconds(START_TIME);

            this.GetComponent<FadeEffect>().StartFadeOut(0.5f);
//            yield return new WaitForSeconds(0.5f);

//            this.gameObject.SetActive(false);
            MapManager.Instance.ClickButton(11);
        }

        void OnDisable()
        {
            Debug.Log("OnDisable warikomi");
            if (SpotImage)
            {
                SpotImage.sprite = null;
            }
            if (SpotImage2)
            {
                SpotImage2.sprite = null;
            }
        }
    }
}
