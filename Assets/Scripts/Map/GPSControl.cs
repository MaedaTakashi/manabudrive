﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class GPSControl : MonoBehaviour
    {
        private float intervalTime = 0.0f;

        private bool nowGetPos;
        private double lastTimeStamp;

        private MapManager mng;

        void Start()
        {
            mng = this.GetComponent<MapManager>();

            lastTimeStamp = 0;
        }

        void Update()
        {
            if (nowGetPos) return;

            //3秒毎に更新
            intervalTime += Time.deltaTime;
            if (intervalTime >= 3.0f)
            {
                GetPos();
                intervalTime = 0.0f;
            }
        }

        void GetPos()
        {
            StartCoroutine(GetGPS());
        }


        private IEnumerator GetGPS()
        {
            DebugManager.PutLog("GPS取得");
            nowGetPos = true;

            if (!Input.location.isEnabledByUser)
            {
                DebugManager.PutLog(" isEnabledByUser:" + Input.location.isEnabledByUser);
                nowGetPos = false;
                yield break;
            }
            Input.location.Start();
            int waitCount = 20;
            while ((Input.location.status == LocationServiceStatus.Initializing) && (waitCount > 0))
            {
                DebugManager.PutLog(" 初期化待ち:" + waitCount.ToString() + " status:" + Input.location.status);
                yield return new WaitForSeconds(1);
                waitCount--;
            }
            if (waitCount < 1)
            {
                DebugManager.PutLog("  timeOut");
                nowGetPos = false;
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                DebugManager.PutLog(" Unable to determine device location");
                nowGetPos = false;
                yield break;
            }
            else
            {
                //posText.text = ToStrLocation();
                //print("Location: " +
                //      Input.location.lastData.latitude + " " +
                //      Input.location.lastData.longitude + " " +
                //      Input.location.lastData.altitude + " " +
                //      Input.location.lastData.horizontalAccuracy + " " +
                //      Input.location.lastData.timestamp);
            }

            waitCount = 20;
            while (Input.location.lastData.timestamp <= lastTimeStamp && waitCount > 0)
            {
                DebugManager.PutLog(" タイムスタンプ更新待ち:" + waitCount.ToString());
                yield return new WaitForSeconds(1);
                waitCount--;
            }
            if (waitCount < 1)
            {
                DebugManager.PutLog("  timeOut");
                nowGetPos = false;
                yield break;
            }

            lastTimeStamp = Input.location.lastData.timestamp;
            Input.location.Stop();
            DebugManager.PutLog(" GPS取得終了");

            mng.UpdateGPS(Input.location.lastData.timestamp,false, Input.location.lastData.latitude, Input.location.lastData.longitude);
            nowGetPos = false;
        }

        private string ToStrLocation()
        {
            string tmpStr = "緯度　latitude: " + Input.location.lastData.latitude + "\n"
                + "経度 longitude: " + Input.location.lastData.longitude + "\n"
                + "高度　altitude: " + Input.location.lastData.altitude + "\n"
                + "水平方向精度　horizontalAccuracy: " + Input.location.lastData.horizontalAccuracy + "\n"
                + "垂直方向精度　verticalAccuracy: " + Input.location.lastData.verticalAccuracy + "\n"
                + "timestamp: " + GetTimeStr(Input.location.lastData.timestamp)
                ;
            return tmpStr;
        }

        private string GetTimeStr(double timestamp)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp).ToLocalTime();
            return (datetime.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
