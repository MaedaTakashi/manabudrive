﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class DemoPlayer : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
    {
        private Slider DemoSlider;
        [SerializeField]
        private bool stopUpdate;

        //public List<GPSDemo> demodatas;
        void Start()
        {
            DemoSlider = this.GetComponent<Slider>();
            stopUpdate = true;
            StartCoroutine(LoadGPSDemoCSV());
        }

        DemoDataControl demoDataCtrl;

        [SerializeField]
        private Toggle HighSppedToggle;

        IEnumerator LoadGPSDemoCSV()
        {
            GPSDemo.Data.Clear();

            Action<string[]> action = GPSDemo.Create;
            yield return StartCoroutine(MasterDataLoader.ParseCSV("CSV/GPSDemo", action));

            Debug.Log("GPSデモデータ 読み込み終了");

            if (GPSDemo.Data.Count > 0)
            {
                //double firstTimeStamp = GPSDemo.Data[0].timestamp;
                //Debug.Log("firstTimeStamp:" + firstTimeStamp);
                //double lastTimeStamp = GPSDemo.Data[GPSDemo.Data.Count - 1].timestamp;
                //foreach (GPSDemo demodata in GPSDemo.Data)
                //{
                //    Debug.Log("1 demodata.timestamp:" + demodata.timestamp);
                //    demodata.timestamp -= firstTimeStamp;
                //    Debug.Log("2 demodata.timestamp:" + demodata.timestamp);
                //}
                DemoSlider.maxValue = GPSDemo.Data[GPSDemo.Data.Count - 1].timestamp;
                DemoSlider.minValue = 0;
            }
            demoDataCtrl = new DemoDataControl();
            //demodatas = GPSDemo.Data;
            Debug.Log("GPSデモデータ 調整終了");

            if (GameManager.IsDemoPlay)
            {
                Debug.Log("デモ再開");

                DemoSlider.value = GameManager.DemoTime;
                timeCount = DemoSlider.value;
                demoDataCtrl.ResetDemoDataIndex(DemoSlider.value);
            }
            GameManager.IsDemoPlay = true;
            UpdateDemo();
            stopUpdate = false;
        }

        public void ChangeSlider()
        {
            if (!GameManager.IsDemoPlay) return;

            if (isPointerDown)
            {
                timeCount = DemoSlider.value;
                demoDataCtrl.ResetDemoDataIndex(timeCount);

                MapManager.Instance.UpdateGPSText(demoDataCtrl.NowData.timestamp
                    , demoDataCtrl.NowData.latitude, demoDataCtrl.NowData.longitude);
            }
        }

        bool isPointerDown = false;
        public void OnPointerDown(PointerEventData eventData)
        {
            stopUpdate = true;
            isPointerDown = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            timeCount = DemoSlider.value;
            demoDataCtrl.ResetDemoDataIndex(timeCount);
            UpdateDemo();
            stopUpdate = false;
            isPointerDown = false;
        }


        float timeCount;
        void Update()
        {
            if (stopUpdate) return;

            if (HighSppedToggle.isOn)
            {
                timeCount += Time.deltaTime * 10.0f;
            }
            else
            {
                timeCount += Time.deltaTime;
            }
            DemoSlider.value = timeCount;
            if (demoDataCtrl.UpdateDemoDataIndex(timeCount))
            {
                UpdateDemo();
            }

        }

        private void UpdateDemo()
        {
            MapManager.Instance.UpdateGPS(demoDataCtrl.NowData.timestamp, true
                , demoDataCtrl.NowData.latitude, demoDataCtrl.NowData.longitude);
        }

        /// <summary>
        /// デモデータ取得用クラス
        /// </summary>
        class DemoDataControl
        {
            private int nowIdx;
            public GPSDemo NowData;

            public DemoDataControl()
            {
                nowIdx = 0;
                NowData = GPSDemo.Data[0];
            }

            /// <summary>
            /// デモデータ更新
            /// タイムスタンプが次行を越えていたら更新する
            /// </summary>
            /// <param name="timeStamp"></param>
            /// <returns></returns>
            public bool UpdateDemoDataIndex(float timeStamp)
            {
                if (nowIdx >= GPSDemo.Data.Count - 1)
                {
                    //Debug.Log("最終行まで来てる");
                    //変化なし
                    return false;
                }

                if (timeStamp < GPSDemo.Data[nowIdx + 1].timestamp)
                {
                    //Debug.Log("次行のタイムスタンプより小さい nowIdx:" + nowIdx + " timeStamp:" + timeStamp);
                    //変化なし
                    return false;
                }

                for (int i = nowIdx + 2; i < GPSDemo.Data.Count; i++)
                {
                    if (timeStamp < GPSDemo.Data[i].timestamp)
                    {
                        nowIdx = i - 1;
                        NowData = GPSDemo.Data[nowIdx];
                        return true;
                    }
                }

                Debug.Log("最終行を越えた");
                //最終行をセット
                nowIdx = GPSDemo.Data.Count - 1;
                NowData = GPSDemo.Data[nowIdx];

                return true;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="timeStamp"></param>
            /// <returns></returns>
            public void ResetDemoDataIndex(float timeStamp)
            {
                nowIdx = 0;
                NowData = GPSDemo.Data[0];
                UpdateDemoDataIndex(timeStamp);
            }
        }

        void OnDestroy()
        {
            GPSDemo.Data.Clear();
        }
    }
}
