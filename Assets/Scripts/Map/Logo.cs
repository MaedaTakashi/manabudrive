﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class Logo : MonoBehaviour, IPointerClickHandler
    {
        private int count = 0;
        public void OnPointerClick(PointerEventData eventData)
        {
            if(count < 4)
            {
                count++;
            }
            else
            {
                ScheneSwitcher.LoadScene("Admin");
            }
        }
    }
}
