﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Map
{
    public class TalkPanel : MonoBehaviour
    {
        float timeCount;
        int State;

        [SerializeField]
        private RectTransform lowerPanel;
        [SerializeField]
        private TextMeshProUGUI txtTime;
        [SerializeField]
        private TextMeshProUGUI txtName;
        [SerializeField]
        private ToggleButton tglMute;
        [SerializeField]
        private GameObject WebCamera;

        private RawImage rawImage;
        private VideoPlayer videoPlayer;

        const int SE_INDEX = 3;

        public bool IsLoaded;

        void init()
        {
            if (rawImage) return;
            rawImage = this.transform.Find("RawImage").GetComponent<RawImage>();
            videoPlayer = rawImage.GetComponent<VideoPlayer>();
            videoPlayer.prepareCompleted += vpPrepareCompleted;
            videoPlayer.loopPointReached += vpPlayEnd;

        }

        public void Show()
        {
            init();
            IsLoaded = false;
            GameManager.EventSystemEnable = false;
            this.gameObject.SetActive(true);
            StartCoroutine(coStart());
        }


        IEnumerator coStart()
        {

            while (!MasterDataLoader.isLoaded)
            {
                yield return null;
            }

            if (GameManager.ActiveMapEventSpot == null)
            {
                GameManager.ActiveMapEventSpot = MapEventSpot.Data[3];
            }

            int spotID = GameManager.ActiveMapEventSpot.ID;
            Debug.Log("Talk Start,SpotID:" + spotID);

            MapEventSpot spot = GameManager.ActiveMapEventSpot;

            txtName.text = spot.TeacherName + "<size=64>先生</size>";
            txtName.enabled = true;

            switch (spotID)
            {
                case 2:
                    StartCoroutine(coLoadAsyncVideoClip("Video/cityguide_landmark", rawImage));
                    break;

                case 7:
                    StartCoroutine(coLoadAsyncVideoClip("Video/cityguide_baybridge", rawImage));
                    break;

                case 9:
                    StartCoroutine(coLoadAsyncVideoClip("Video/cityguide_baybridge", rawImage));
                    break;

                default:
                    string tmpPath = "Chat/Chat" + spot.ID.ToString("00");
                    StartCoroutine(coLoadAsyncTexture(tmpPath, rawImage));
                    rawImage.GetComponent<TalkImage>().ClickToGotoEventScene = true;
                    break;
            }

            Debug.Log("lowerPanel up start");
            lowerPanel.gameObject.SetActive(true);
            lowerPanel.anchoredPosition = new Vector2(0f, -1536f);
            LeanTween.move(lowerPanel, new Vector2(0f, -1024), 0.3f).setEaseOutQuad();

            yield return new WaitForSeconds(0.5f);

            GameManager.EventSystemEnable = true;

            if (spot.SpotKindCode == 3)
            {
                WebCamera.SetActive(true);
            }

            while (!IsLoaded)
            {
                yield return null;
            }
            rawImage.enabled = true;
            if (videoPlayer)
            {
                videoPlayer.Play();
            }

            //foreach (Transform tran in this.transform.parent)
            //{
            //    if (tran != this.transform)
            //    {
            //        Destroy(tran.gameObject);
            //    }
            //}
        }

        public void ClickMuteButton()
        {
            if (tglMute.isOn)
            {
                SoundControl.SetSEVol(SE_INDEX, 0f);
            }
            else
            {
                SoundControl.SetSEVol(SE_INDEX, 1f);
            }
        }

        void vpPrepareCompleted(VideoPlayer vp)
        {
            Debug.Log("vpPrepareCompleted:");
            IsLoaded = true;
        }

        void vpPlayEnd(VideoPlayer vp)
        {
            Debug.Log("vpPlayEnd:");
            //Destroy(this.gameObject);
            MapManager.Instance.GotoEventScene();
            //nexrButton.GetComponent<FadeEffect>().StartFadeIn();
            //GotoNext();
        }


        /// <summary>
        /// リソースからテクスチャを非同期でロードするコルーチン
        /// </summary>
        IEnumerator coLoadAsyncTexture(string filePath, RawImage rawImg)
        {
            Debug.Log("LoadAsyncTextureCoroutine start:" + filePath);
            // リソースの非同期読込開始
            ResourceRequest resReq = Resources.LoadAsync<Sprite>(filePath);

            // 終わるまで待つ
            while (resReq.isDone == false)
            {
                yield return 0;
            }

            rawImage.color = Color.white;
            rawImage.texture = (resReq.asset as Sprite).texture;
            IsLoaded = true;
        }


        /// <summary>
        /// リソースから動画を非同期でロードするコルーチン
        /// </summary>
        IEnumerator coLoadAsyncVideoClip(string filePath, RawImage rawImg)
        {
            Debug.Log("LoadAsyncVideoClipCoroutine start:" + filePath);
            // リソースの非同期読込開始
            ResourceRequest resReq = Resources.LoadAsync<VideoClip>(filePath);

            // 終わるまで待つ
            while (resReq.isDone == false)
            {
                yield return 0;
            }

            RenderTexture rt = new RenderTexture(1536, 1536, 0);
            rawImage.texture = rt;
            videoPlayer.targetTexture = rt;
            rawImage.color = Color.white;

            videoPlayer.clip = resReq.asset as VideoClip;

            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            videoPlayer.SetTargetAudioSource(0, SoundControl.GetSESource(SE_INDEX));
            SoundControl.SetSEVol(SE_INDEX, 1f);
            videoPlayer.Prepare();
            Debug.Log("videoPlayer.Prepare");
        }


        private int lastSec;
        void Update()
        {
            if (videoPlayer)
            {
                if (videoPlayer.isPlaying)
                {
                    int sec = (int)videoPlayer.time;
                    if(sec != lastSec)
                    {
                        lastSec = sec;
                        int m = sec / 60;
                        int s = sec % 60;
                        txtTime.text = m.ToString("00") + ":" + s.ToString("00");
                    }
                }
            }
        }

        void OnDestroy()
        {
            if (rawImage)
            {
                rawImage.texture = null;
            }
            //ChatImage = null;
        }
    }
}
