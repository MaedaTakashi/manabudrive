﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.KokinSyasin
{
    public class IjinTalkManager : MonoBehaviour
    {

        #region Singleton
        private static IjinTalkManager instance;
        public static IjinTalkManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (IjinTalkManager)FindObjectOfType(typeof(IjinTalkManager));
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        [SerializeField]
        private TimeCountDisp TimeCounter;
        [SerializeField]
        private CameraImage CameraImage;

        [SerializeField]
        private VortexTimer VortexTimer;

        [SerializeField]
        private FadeEffect WebCamFade;

        [SerializeField]
        private FadeEffect OldImgFade;
        [SerializeField]
        private VortexTimer OldVortexTimer;


        void Start()
        {
        }

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    //SoundControl.PlaySound(0);
                    //GameSetting.Instance.StageMode = 1;
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    if (!nowWarping)
                    {
                        StartCoroutine(warpImageEffect());
                    }
                    break;
            }
        }

        bool nowWarping = false;
        private IEnumerator warpImageEffect()
        {
            nowWarping = true;

            WebCamFade.gameObject.SetActive(true);
            foreach(Graphic gra in WebCamFade.GetComponentsInChildren<Graphic>())
            {
               gra.color = Color.white;
            }
            OldImgFade.gameObject.SetActive(false);

            TimeCounter.gameObject.SetActive(true);
            TimeCounter.GetComponent<Text>().text = 2017.ToString();
            CameraImage.WebCamStop();

            VortexTimer.PlayStart(0f, 1440f, 2.5f);
            OldVortexTimer.PlayStart(-5760f, 0f, 5f);

            //0.5
            yield return new WaitForSeconds(0.5f);
            TimeCounter.PlayStart(2017, 1911, 4.5f);

            //1.5
            yield return new WaitForSeconds(1.0f);
            WebCamFade.StartFadeOut(1.0f);
            OldImgFade.gameObject.SetActive(true);

            //2.5
            yield return new WaitForSeconds(1.0f);
            VortexTimer.PlayStart(1440f, 0f, 2.5f);

            //5.0
            yield return new WaitForSeconds(2.5f);
            nowWarping = false;
        }
    }
}
