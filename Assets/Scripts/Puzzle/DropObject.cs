﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Puzzle
{
    public class DropObject : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public int ItemID;

        [SerializeField]
        private Animator HakkenAnim;

        [SerializeField]
        private Animator ItemAnim;

        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerDrag == null) return;
            //Image droppedImage = pointerEventData.pointerDrag.GetComponent<Image>();
            //iconImage.sprite = droppedImage.sprite;
            //iconImage.color = Vector4.one * 0.6f;
        }

        public void OnPointerExit(PointerEventData pointerEventData)
        {
            if (pointerEventData.pointerDrag == null) return;
            //iconImage.sprite = nowSprite;
            //if (nowSprite == null)
            //    iconImage.color = Vector4.zero;
            //else
            //    iconImage.color = Vector4.one;
        }

        public void OnDrop(PointerEventData pointerEventData)
        {
            //Image droppedImage = pointerEventData.pointerDrag.GetComponent<Image>();
            //iconImage.sprite = droppedImage.sprite;
            //nowSprite = droppedImage.sprite;
            //iconImage.color = Vector4.one;
            DragObject drgItem = pointerEventData.pointerDrag.GetComponent<DragObject>();
            if (drgItem.ItemID == ItemID)
            {
                PuzzleManager.Instance.IsDoing = true;
                HakkenAnim.enabled = true;
                ItemAnim.enabled = true;

                drgItem.Success();
                StartCoroutine(coSuccess());
            }

            //DraggingItem droppedItem = pointerEventData.pointerDrag.GetComponent<DraggingItem>();
            //if (droppedItem.DragObj.ItemID == ItemID)
            //{
            //    HakkenAnim.enabled = true;
            //    ItemAnim.enabled = true;

            //    droppedItem.DragObj.Success();
            //}
        }

        private IEnumerator coSuccess()
        {
            yield return null;

            yield return new WaitForSeconds(1.0f);
            PuzzleManager.Instance.ShowDetail(ItemID);
            HakkenAnim.SetInteger("State", 2);
            PuzzleManager.Instance.IsDoing = false;
        }
    }
}
