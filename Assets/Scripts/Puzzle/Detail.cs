﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Puzzle
{
    public class Detail : MonoBehaviour
    {
        [SerializeField]
        private Image Image;

        [SerializeField]
        private TextMeshProUGUI textName;

        [SerializeField]
        private TextMeshProUGUI textText;

        public void Show(int itemID)
        {
            this.gameObject.SetActive(true);
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }
    }
}
