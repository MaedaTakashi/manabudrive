﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Puzzle
{
    public class Talk : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] Objs;

        void Start()
        {
            StartCoroutine(coTalk());
        }


        private IEnumerator coTalk()
        {
            yield return null;

            yield return new WaitForSeconds(1.0f);
            Objs[0].SetActive(true);

            yield return new WaitForSeconds(1.0f);
            Objs[1].SetActive(true);

            yield return new WaitForSeconds(1.0f);
            Objs[2].SetActive(true);

            yield return new WaitForSeconds(1.0f);
            Objs[3].SetActive(true);

        }

        public void ClickCloseButton()
        {
            this.gameObject.SetActive(false);
            PuzzleManager.Instance.StartSeach();
        }

    }
}
