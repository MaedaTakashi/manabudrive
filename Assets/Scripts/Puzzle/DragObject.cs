﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Puzzle
{
    [RequireComponent(typeof(Image))]
    public class DragObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField]
        private Sprite OffSprite;

        [SerializeField]
        private Sprite OnSprite;

        //[SerializeField]
        private Image Image;

        private Transform canvasTran;
        private GameObject draggingObject;
        private Vector2 draggingPosDiff;

        public int ItemID;
        public enum enState { Non, Success };
        public enState State = enState.Non;

        void Awake()
        {
            canvasTran = getCanvas(this.transform);
            Image = this.GetComponent<Image>();
        }

        private Transform getCanvas(Transform tran)
        {
            Transform tmpTran = tran.parent;
            Canvas tmpCanvas;
            for (int i=0; i < 100; i++)
            {
                if (tmpTran == null)
                {
                    return null;
                }
                tmpCanvas = tmpTran.GetComponent<Canvas>();
                if (tmpCanvas != null)
                {
                    return tmpTran;
                }
                tmpTran = tmpTran.parent;
            }
            return null;
        }

        public void OnBeginDrag(PointerEventData pointerEventData)
        {
            if (PuzzleManager.Instance.IsDoing) return;
            if (State == enState.Success) return;

            draggingPosDiff = (Vector2)this.transform.position - pointerEventData.position;

            CreateDragObject();
            draggingObject.transform.position = pointerEventData.position + draggingPosDiff;
        }

        public void OnDrag(PointerEventData pointerEventData)
        {
            if (PuzzleManager.Instance.IsDoing) return;
            if (draggingObject == null) return;
            draggingObject.transform.position = pointerEventData.position + draggingPosDiff;
        }

        public void OnEndDrag(PointerEventData pointerEventData)
        {
            //if (PuzzleManager.Instance.IsDoing) return;
            gameObject.GetComponent<Image>().color = Vector4.one;
            Destroy(draggingObject);
        }

        // ドラッグオブジェクト作成
        private void CreateDragObject()
        {
            draggingObject = new GameObject("Dragging Object");
            draggingObject.transform.SetParent(canvasTran);
            draggingObject.transform.SetAsLastSibling();
            draggingObject.transform.localScale = Vector3.one;

            // レイキャストがブロックされないように
            CanvasGroup canvasGroup = draggingObject.AddComponent<CanvasGroup>();
            canvasGroup.blocksRaycasts = false;

            Image draggingImage = draggingObject.AddComponent<Image>();
            Image sourceImage = GetComponent<Image>();

            draggingImage.sprite = sourceImage.sprite;
            draggingImage.rectTransform.sizeDelta = sourceImage.rectTransform.sizeDelta;
            draggingImage.color = sourceImage.color;
            draggingImage.material = sourceImage.material;

            gameObject.GetComponent<Image>().color = Vector4.one * 0.6f;

            DraggingItem draggingItem = draggingObject.AddComponent<DraggingItem>();
            draggingItem.DragObj = this;
            //Debug.Log(draggingItem.DragObj.ItemID);
        }

        public void Success()
        {
            Image.sprite = OnSprite;
            State = enState.Success;
        }
    }
}
