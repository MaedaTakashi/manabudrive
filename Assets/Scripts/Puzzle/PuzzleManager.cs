﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Puzzle
{
    public class PuzzleManager : MonoBehaviour
    {

        #region Singleton
        private static PuzzleManager instance;
        public static PuzzleManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (PuzzleManager)FindObjectOfType(typeof(PuzzleManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        //private int state = 0;

        //[SerializeField]
        //private Image Image;

        //[SerializeField]
        //private Sprite[] Sprites;

        [SerializeField]
        private GameObject TalkPanel;

        [SerializeField]
        private FadeEffect FirstPanelFade;

        [SerializeField]
        private FadeEffect SeachPanelFade;

        [SerializeField]
        private Detail DetailPanel;


        public bool IsDoing = false;

        void Start()
        {
            TalkPanel.SetActive(true);
        }

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                //    Image.sprite = Sprites[0];
                //    state++;
                //    break;

                //case 1:
                //    Image.sprite = Sprites[1];
                //    state++;
                //    break;

                //case 2:
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;
            }
        }

        public void StartSeach()
        {
            FirstPanelFade.StartFadeOut();
            SeachPanelFade.PreFadeInStart();
        }

        public void ShowDetail(int ItemID)
        {
            DetailPanel.Show(ItemID);
        }

    }
}
