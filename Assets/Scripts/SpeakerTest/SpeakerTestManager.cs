﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using System.Runtime.InteropServices;

namespace ManabuDriveApp.SpeakerTest
{
    public class SpeakerTestManager : MonoBehaviour
    {

        #region Singleton
        private static SpeakerTestManager instance;
        public static SpeakerTestManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (SpeakerTestManager)FindObjectOfType(typeof(SpeakerTestManager));
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        //[SerializeField]
        //private Text dispText;

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    //SoundControl.PlaySound(0);
                    //GameSetting.Instance.StageMode = 1;
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    SwitchSpeaker();
                    break;

                case 2:
                    SoundControl.PlaySound(0);
                    break;

            }
        }

        [DllImport("__Internal")]
        private static extern bool SwitchSpeakerRoute(int mode);
        int nowSpeakerIndex = 0;

        private void SwitchSpeaker()
        {
            if(nowSpeakerIndex == 0)
            {
                nowSpeakerIndex = 1;
            }
            else
            {
                nowSpeakerIndex = 0;
            }

#if UNITY_IOS && !UNITY_EDITOR
            //iOS実機ではプラグインの使用
            DebugManager.PutLog("スピーカーテスト 切り替え実行");
            bool rtn = SwitchSpeakerRoute(nowSpeakerIndex);
            if(rtn){
                DebugManager.PutLog("スピーカーテスト 成功が返った mode:" + nowSpeakerIndex + "(0: build-in speaker / 1: Bluetooth or A2DP)");
            }else{
                DebugManager.PutLog("スピーカーテスト 失敗が返った mode:" + nowSpeakerIndex + "(0: build-in speaker / 1: Bluetooth or A2DP)");
            }
#else
            //iOS実機以外では素通り
            DebugManager.PutLog("スピーカーテスト 対象外OS");
#endif
        }
    }
}
