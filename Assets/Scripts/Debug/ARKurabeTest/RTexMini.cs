﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ManabuDriveApp.Common
{
    public class RTexMini : MonoBehaviour {

        [SerializeField]
        private RenderTexture[] renderTex;
        [SerializeField]
        private RawImage[] rawImage;
        [SerializeField]
        private VideoPlayer[] videoPlayer;


        int state = 0;

        public void ClickButton()
        {
            if (state == 0)
            {
                rawImage[0].texture = renderTex[2];
                rawImage[1].texture = renderTex[3];
                videoPlayer[0].targetTexture = renderTex[2];
                videoPlayer[1].targetTexture = renderTex[3];
                state = 1;
            }
            else
            {
                rawImage[0].texture = renderTex[0];
                rawImage[1].texture = renderTex[1];
                videoPlayer[0].targetTexture = renderTex[0];
                videoPlayer[1].targetTexture = renderTex[1];
                state = 0;
            }
            DebugManager.PutLog("レンダテクスチャ変更:" + rawImage[0].texture);
        }
    }
}