﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ManabuDriveApp.Common
{
    public class DebugManager : MonoBehaviour
    {

        #region Singleton
        private static DebugManager instance;
        public static DebugManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (DebugManager)FindObjectOfType(typeof(DebugManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        public GameObject LogTextGO;
        public Text LogText;

        public GameObject DebugMenu;
        public InputField txtEventLimitTime;
        public InputField txtSpotDispThreshold;
        public InputField txtSpotTyakusinThreshold;
        public InputField txtSpotReTyakusinTime;

        public Toggle tglStopGPS;
        public InputField txtGPSX;
        public InputField txtGPSY;
        public GameObject[] mapgos;

        void Start()
        {
        }

        public static void PutLog(string newLogStr)
        {
            Debug.Log(newLogStr);
            //Debug.Log(instance.name);
            instance._PutLog(newLogStr);
        }
        //private List<string> logStrs;
        private void _PutLog(string newLogStr)
        {
            if (LogText == null) return;

            //if (logStrs == null)
            //{
            //    logStrs = new List<string>();
            //}

            //if (logStrs.Count > 9)
            //{
            //    logStrs.RemoveAt(0);
            //}

            //logStrs.Add(newLogStr);
            //string tmpStr = logStrs[0];
            //for(int i=1;i<logStrs.Count;i++)
            //{
            //    tmpStr += "\n" + logStrs[i];
            //}
            LogText.text += "\n" + newLogStr;
        }

        public void ClearDebugText()
        {
            LogText.text = "";
        }

        public void ClickDebugSwich()
        {
            bool isDisp = !LogTextGO.activeSelf;
            LogTextGO.SetActive(isDisp);
            DebugMenu.SetActive(isDisp);

            GameSetting gs = GameManager.Instance.GameSetting;
            if (isDisp)
            {
                txtEventLimitTime.text = gs.EventLimitTime.ToString();
                txtSpotDispThreshold.text = gs.SpotDispThreshold.ToString();
                txtSpotTyakusinThreshold.text = gs.SpotTyakusinThreshold.ToString();
                txtSpotReTyakusinTime.text = gs.SpotReTyakusinTime.ToString();

                if (ManabuDriveApp.Map.MapManager.Instance != null)
                {
                    tglStopGPS.isOn = (gs.StopAutoUpdate == 1);
                    Vector2 vec = ManabuDriveApp.Map.MapManager.Instance.MapViewControl.CarGPSPos;
                    txtGPSY.text = vec.y.ToString();
                    txtGPSX.text = vec.x.ToString();

                    foreach(GameObject go in mapgos)
                    {
                        go.SetActive(true);
                    }
                }
                else
                {
                    foreach (GameObject go in mapgos)
                    {
                        go.SetActive(false);
                    }
                }

            }
            else
            {
                SetPrefFloat(ref gs.EventLimitTime, txtEventLimitTime);
                SetPrefFloat(ref gs.SpotDispThreshold, txtSpotDispThreshold);
                SetPrefFloat(ref gs.SpotTyakusinThreshold, txtSpotTyakusinThreshold);
                SetPrefFloat(ref gs.SpotReTyakusinTime, txtSpotReTyakusinTime);


                gs.StopAutoUpdate = (tglStopGPS.isOn) ? 1 : 0;
                SetPrefFloat(ref gs.GPSY, txtGPSY);
                SetPrefFloat(ref gs.GPSX, txtGPSX);

                gs.SaveSettingData();
            }
        }

        private void SetPrefFloat(ref float pref,InputField txt)
        {
            float tmp;
            if(float.TryParse(txt.text,out tmp))
            {
                pref = tmp;
            }
        }

        public void ChangeDebugToggle()
        {
            if (ARKurabe.ARKurabeManager.Instance)
            {
                ARKurabe.ARKurabeManager.Instance.DebugDisp();
            }
            else if (Map.MapManager.Instance)
            {
                Map.MapManager.Instance.DebugDisp();
            }
        }

        private int reso = 0;
        private Vector2Int defaultScreenSize;
        public void ChangeResolution()
        {
            float rate;

            switch (reso)
            {
                case 0:
                    defaultScreenSize = new Vector2Int(Screen.currentResolution.width, Screen.currentResolution.height);
                    rate = 0.75f;
                    reso = 1;
                    break;
                case 1:
                    rate = 0.5f;
                    reso = 2;
                    break;
                default:
                    rate = 1f;
                    reso = 0;
                    break;
            }

            DebugManager.PutLog("変更前の解像度：" + Screen.currentResolution.width + "x" + Screen.currentResolution.height);
            Screen.SetResolution((int)((float)defaultScreenSize.x * rate)
                , (int)((float)defaultScreenSize.y * rate), true);
        }


        public void ClickJump(int index)
        {
            if (ManabuDriveApp.Map.MapManager.Instance != null)
            {
                switch (index)
                {
                    case 1:
                        //ランドマーク
                        setJumpData(35.45326f, 139.6378f);
                        break;
                    case 2:
                        //駐車場
                        setJumpData(35.45262f, 139.6399f);
                        break;
                    case 3:
                        //ベイブリッジ
                        setJumpData(35.44937f,139.6709f);
                        break;
                }

                GameSetting gs = GameManager.Instance.GameSetting;
                tglStopGPS.isOn = true;
                SetPrefFloat(ref gs.GPSY, txtGPSY);
                SetPrefFloat(ref gs.GPSX, txtGPSX);
                Map.MapManager.Instance.Debug_UpdateGPS(gs.GPSY, gs.GPSX);

                ClickDebugSwich();
            }
        }

        private void setJumpData(float y,float x)
        {
            txtGPSX.text = x.ToString();
            txtGPSY.text = y.ToString();
        }

    }
}
