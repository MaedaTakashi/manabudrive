﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.FirstScene
{
    public class FirstScene : MonoBehaviour
    {
        private bool isLoaded = false;
        void Start()
        {
            StartCoroutine(GotoLastScene());
        }

        public void ClickButton(int index)
        {
            if (!isLoaded) return;

            switch (index)
            {
                case 0:
                    gotoScene();
                    break;
                case 1:
                    ScheneSwitcher.LoadScene("ARKurabe_TestVer", 0.5f);
                    break;
                case 2:
                    DataClear();
                    break;
            }
        }

        private void gotoScene()
        {
            string lastScene = GameManager.Instance.PlayData.LastScene;
            Debug.Log("lastScene:" + lastScene);
            switch (lastScene)
            {
                case "Chat":
                    ScheneSwitcher.LoadScene("Chat", 0.5f);
                    break;

                case "ARKurabe":
                    ScheneSwitcher.LoadScene("ARKurabe", 0.5f);
                    break;

                case "KokinSyasin":
                    ScheneSwitcher.LoadScene("KokinSyasin", 0.5f);
                    break;

                case "Title":
                    ScheneSwitcher.LoadScene("Title");
                    break;

                case "Opening":
                    ScheneSwitcher.LoadScene("Opening", 0.5f);
                    break;

                default:
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;
            }
        }

        IEnumerator GotoLastScene()
        {
            while (!GameManager.PlayDataIsLoaded)
            {
                Debug.Log("ユーザデータ構築待ち");
                yield return null;
            }
            isLoaded = true;

        }

        private void DataClear()
        {
            GameManager.ClearUsers();
            GameManager.SetupNewUser();
            GameManager.SaveData("Title");
            ScheneSwitcher.LoadScene("Title", 1f);
        }

    }
}
