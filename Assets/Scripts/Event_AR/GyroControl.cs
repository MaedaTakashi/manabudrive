﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{

    /// <summary>
    /// ジャイロの制御
    /// 　ジャイロの検出、角度の変換、
    /// 　および角度をAR空間上のカメラのゲームオブジェクトまたはパララックス制御用変数へ渡す
    /// </summary>
    public class GyroControl : MonoBehaviour
    {
        //初期角度
        Quaternion startgyro;
        public Text InfoText;
        public bool ExistGyro;

        //単純パララックスモード用変数
        //UI部品側から参照する
        public float GyroRotationZ;
        public Vector2 GyroXY;

        /// <summary>
        /// AR空間を使用するか
        /// true:AR空間モード　false:単純パララックスモード
        /// </summary>
        public bool UseARWorld;

        void OnEnable()
        {
            //DebugManager.PutLog("ジャイロコントロール有効化");
            GyroInit();
        }

        void OnDisable()
        {
            if (InfoText)
            {
                InfoText.text = "ジャイロ検出停止";
            }

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                GyroRotationZ = 0f;
                GyroXY = Vector2.zero;
            }

            this.transform.localRotation = Quaternion.identity;
            //Input.gyro.enabled = false;
        }

        public void GyroInit()
        {
			Debug.Log("Gyro Init====");
            Input.gyro.enabled = true;
            if (Input.gyro.enabled)
            {
                DebugManager.PutLog("ジャイロ角初期化処理");
                Quaternion tmpGyro = Input.gyro.attitude;
                startgyro = Quaternion.Euler(90, 0, 0) * (new Quaternion(-tmpGyro.x, -tmpGyro.y, tmpGyro.z, tmpGyro.w));
                ExistGyro = true;
				DebugManager.PutLog("-ジャイロ角初期化処理 終了");
				DebugManager.PutLog("start="+startgyro.eulerAngles.x.ToString()+","+startgyro.eulerAngles.y.ToString()+","+startgyro.eulerAngles.z.ToString());

            }
            else
            {
                Debug.Log("ジャイロデバイスなし");
                InfoText.text = "ジャイロデバイスが見つかりません。";
                ExistGyro = false;
            }
			this.transform.localRotation = Quaternion.identity;
            //Debug.Log("this.transform.localRotation:" + this.transform.localRotation + " Quaternion.identity:" + Quaternion.identity);
        }

		bool isStart;
		int startCounter;
		int startCtrMax = 20;
        void Update()
        {
            if (!ExistGyro) return;

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                InfoText.text = "ジャイロ初期角度:" + startgyro.eulerAngles + "\n"
                    //+ "初期角度からの角度差:" + diff + "\n"
                    //+ "影響割合:" + rate + "\n\n"
                    + "横回転角度:" + GyroRotationZ + "\n"
                    + "傾き(-1～1):" + GyroXY;
                return;
            }

            Input.gyro.enabled = true;
            if (Input.gyro.enabled)
            {
                Quaternion gyro = Input.gyro.attitude;
				gyro = Quaternion.Euler(90, 0, 0) * (new Quaternion(-gyro.x, -gyro.y, gyro.z, gyro.w));
				if(!isStart){
					startCounter++;
					startgyro = gyro;
					if(startCounter > startCtrMax){
						isStart = true;
					}
				}

                Vector3 diff = gyro.eulerAngles - startgyro.eulerAngles;
                Vector3 fixdDiff = new Vector3(angleFix(diff.x), angleFix(diff.y), diff.z);
//                GyroDiffFromStart = new Vector3(angleFix(diff.x), angleFix(diff.y), angleFix(diff.z));

				DebugManager.Instance.ClearDebugText();
				DebugManager.PutLog("start="+startgyro.eulerAngles.x.ToString()+","+startgyro.eulerAngles.y.ToString()+","+startgyro.eulerAngles.z.ToString());
				DebugManager.PutLog("now="+gyro.eulerAngles.x.ToString()+","+gyro.eulerAngles.y.ToString()+","+gyro.eulerAngles.z.ToString());

                //ジャイロ角度反映率
                float rate = ARKurabeManager.Instance.RotRate;


                if (UseARWorld)
                {
                    //AR空間モードの場合
                    //アタッチしてるゲームオブジェクトの角度を変える
                    Vector3 vec = new Vector3(fixdDiff.x * rate, fixdDiff.y * rate, fixdDiff.z);
                    this.transform.localRotation = Quaternion.Euler(vec);
					//this.transform.localRotation = Quaternion.identity;
                    InfoText.text = "ジャイロ初期角度:" + startgyro.eulerAngles + "\n"
                        + "初期角度からの角度差:" + diff + "\n"
                        + "影響割合:" + rate;

                }
                else
                {
                    //単純パララックスモードの場合
                    //UI側が参照できるよう角度を出す
                    GyroRotationZ = -fixdDiff.z;
                    GyroXY = new Vector2(fixdDiff.y / 90f, fixdDiff.x / 90f) * rate;

                    InfoText.text = "ジャイロ初期角度:" + startgyro.eulerAngles + "\n"
                        + "初期角度からの角度差:" + diff + "\n"
                        + "影響割合:" + rate + "\n\n"
                        + "横回転角度:" + GyroRotationZ + "\n"
                        + "傾き(-1～1):" + GyroXY;
                }

            }
        }

        /// <summary>
        /// 角度を-180～180の範囲に直す
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        private float angleFix(float angle)
        {
            //角度を-180～180の範囲に直す
            while (angle > 180f)
            {
                angle -= 360f;
            }
            while (angle < -180f)
            {
                angle += 360f;
            }

            //90度で折り返す
            while (angle > 90f)
            {
                angle = 180f - angle;
            }
            while (angle < -90f)
            {
                angle = -180f - angle;
            }
            return angle;
        }

        void OnDestroy()
        {
            Input.gyro.enabled = false;
        }
    }
}
