﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべUIオブジェクト
    /// </summary>
    public class ARKurabeImageUIObj : MonoBehaviour
    {
        public GyroControl GyroControl;
        private RectTransform rectTran;
        private Vector2 defaultPos;
        //private Quaternion defaultRot;
        public Vector2 MovableValue = new Vector2(1f,1f);

        void Start()
        {
            rectTran = this.GetComponent<RectTransform>();
            defaultPos = rectTran.anchoredPosition;
            //defaultRot = this.transform.localRotation;
        }

        void OnEnable()
        {
            if (rectTran)
            {
                rectTran.anchoredPosition = defaultPos;
            }
            //this.transform.localRotation = defaultRot;
        }

        void Update()
        {
            if (GyroControl.UseARWorld) return;

            if (GyroControl.ExistGyro)
            {
                rectTran.anchoredPosition = defaultPos + new Vector2(MovableValue.x * GyroControl.GyroXY.x,MovableValue.y * GyroControl.GyroXY.y);
            }
        }

        void OnDisable()
        {
            if (this.gameObject)
            {
                rectTran.anchoredPosition = defaultPos;
            }
        }
    }
}
