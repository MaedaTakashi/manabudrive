﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityEngine.Video;

namespace ManabuDriveApp.ARKurabe
{
    public class KurabeButton : MonoBehaviour
    {
        //public Image ImgBG;
        public ARKurabeImageUI ARKurabeImageUI;
        //public ARMap ARMap;
        public VideoClip VideoClip;
        public VideoClip VideoClipLight;

        virtual public void ClickButton()
        {
            if (ARKurabeManager.Instance.ShowVideoClip(this) == false) return;

            //ボタン拡大アニメ
            this.GetComponent<Button>().interactable = false;
            Animator anim = this.GetComponent<Animator>();
            anim.enabled = true;
            anim.Play("On",0,0f);
            this.transform.Find("TextImage").GetComponent<Image>().color = ConstColor.GetColorFromHTMLString("#666666FF"); ;
        }

        internal void ChangeNonActive()
        {
            Animator anim = this.GetComponent<Animator>();
            anim.enabled = true;
            anim.Play("Off", 0, 0f);
            this.GetComponent<Button>().interactable = true;
            this.transform.Find("TextImage").GetComponent<Image>().color = ConstColor.GetColorFromHTMLString("#333333FF"); ;
        }
    }
}
