﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityEngine.Playables;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべUI
    /// </summary>
    public class ARKurabeImageUI : MonoBehaviour
    {
        private GyroControl GyroControl;

        public ARMap ARMap;
        public PlayableDirector PlayableDirector;

        private void init()
        {
            if (GyroControl != null) return;
            GyroControl = ARKurabeManager.Instance.GyroControl;
        }

        public void Disp(bool value)
        {
            init();

            TakeOverPos[] tmp = this.transform.GetComponentsInChildren<TakeOverPos>();
            foreach(TakeOverPos child in tmp)
            {
                child.GetAndSetPos();
            }

            this.transform.rotation = Quaternion.identity;
            this.gameObject.SetActive(value);
            if (ARKurabeManager.Instance.GyroControl.UseARWorld)
            {
                ARMap.Disp(value);
            }
        }

        void Update()
        {
            init();
            if (GyroControl.UseARWorld) return;

            if (GyroControl.ExistGyro)
            {
                this.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, GyroControl.GyroRotationZ));
            }
        }

        void OnDisable()
        {
            if (this.gameObject)
            {
                this.transform.rotation = Quaternion.identity;
            }
        }
    }
}
