﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべっこ ラクダ前面アニメ制御
    /// </summary>
    public class ImageUI_Camel : MonoBehaviour
    {
        [SerializeField]
        private Animator animLeftEye;
        [SerializeField]
        private Animator animRightEye;

        /// <summary>
        /// ARくらべっこ 疑似AR状態のラクダへの参照
        /// </summary>
        [SerializeField]
        private ImageUI_Rakuda ImageUI_Rakuda;

        /// <summary>
        /// 瞬きアニメを行う
        /// </summary>
        public void AnimeEye()
        {
            animLeftEye.Play(0);
            animRightEye.Play(0);
        }

        /// <summary>
        /// アニメ終了時に呼ばれる
        /// </summary>
        public void AnimeEnd()
        {
            ImageUI_Rakuda.EndBigAnime();
            this.gameObject.SetActive(false);
        }

        public void DoSound(int SoundIndex)
        {
            SoundControl.PlaySound(SoundIndex);
        }
    }
}
