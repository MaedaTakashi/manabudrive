﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityEngine.Video;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべっこ　主処理
    /// </summary>
    public class ARKurabeManager : MonoBehaviour
    {

        #region Singleton
        private static ARKurabeManager instance;
        public static ARKurabeManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (ARKurabeManager)FindObjectOfType(typeof(ARKurabeManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        public float RotRate;
        public GameObject ARWorld;
        //public ARMap ARMap;
        public InputField InputRotRate;

        public GameObject Panel_Awase;
        public GameObject Silhouette;
        public GameObject Panel_Kurabe;

        public KurabeButton FirstKurabe;
        public LimitTimer LimitTimer;


        public VideoPlayer[] videoPlayer;
        public RawImage[] videoRawImage;
        private bool[] isVideoReady = new bool[2];
        private bool[] isVideoEnd = new bool[2];

        public CameraImage webCamControl;
        public Toggle UseARToggle;
        public Toggle VisibleARModel;
        public GyroControl GyroControl;

        public GameObject DebugItemPanel;
        public GameObject DebugItemPanelChild1;
        public GameObject DebugItemPanelChild2;

        protected bool timerIsOn = false;

        void Start()
        {
            RotRate = 1f;
            Panel_Kurabe.SetActive(false);
            Panel_Awase.SetActive(true);

            for(int i=0;i< videoPlayer.Length; i++)
            {
                videoPlayer[i].prepareCompleted += vpPrepareCompleted;
                videoPlayer[i].loopPointReached += vpPlayEnd;
            }

            //ARMap.Init();
            DebugDisp();
        }

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    //SoundControl.PlaySound(0);
                    GameManager.SaveData("Map");
                    ScheneSwitcher.LoadScene("Map", 0.5f);
                    break;

                case 1:
                    webCamControl.PlayStopToggle();
                    break;

            }
        }

        private int getIndexVideoPlayer(VideoPlayer vp)
        {
            for(int i = 0; i < videoPlayer.Length; i++)
            {
                if(vp == videoPlayer[i])
                {
                    return i;
                }
            }
            return -1;
        }

        void vpPrepareCompleted(VideoPlayer vp)
        {
            int index = getIndexVideoPlayer(vp);
            if (index< 0) return;
            isVideoReady[index] = true;

            for (int i = 0; i < isVideoReady.Length; i++){
                if(isVideoReady[i] == false)
                {
                    return;
                }
            }

            for (int i = 0; i < videoPlayer.Length; i++)
            {
                if (videoPlayer[i].enabled)
                {
                    videoPlayer[i].Play();
                    StartCoroutine(coDelayDispImg(videoRawImage[i]));
                }
            }
        }


        private IEnumerator coDelayDispImg(RawImage rawImage)
        {
            yield return new WaitForSeconds(0.1f);
            //yield return null;

            rawImage.enabled = true;
        }

        void vpPlayEnd(VideoPlayer vp)
        {
            int index = getIndexVideoPlayer(vp);
            if (index < 0) return;
            isVideoEnd[index] = true;

            for (int i = 0; i < isVideoEnd.Length; i++)
            {
                if (isVideoEnd[i] == false)
                {
                    return;
                }
            }


            if (lastKurabeButton)
            {
                ARKurabeUIDisp(lastKurabeButton, true);
            }

            for (int i = 0; i < videoPlayer.Length; i++)
            {
                if (videoPlayer[i].enabled)
                {
                    videoPlayer[i].enabled = false;
                    videoRawImage[i].enabled = false;
                }
            }


            if (!timerIsOn)
            {
                LimitTimer.TimerStart(GameManager.Instance.GameSetting.EventLimitTime,timerEnd);
                timerIsOn = true;
            }
            return;
        }


        void timerEnd(int rtn)
        {
            Debug.Log("タイマー終了");
            timerIsOn = false;
        }


        public void InputRate()
        {
            float tmp;
            if (float.TryParse(InputRotRate.text,out tmp))
            {
                if (tmp > 0f)
                {
                    RotRate = tmp / 100f;
                    Debug.Log("RateChange " + RotRate);
                    return;
                }
            }

            InputRotRate.text = Mathf.FloorToInt(RotRate * 100f).ToString();
        }

        /// <summary>
        /// デバッグ用　画像座標決定方式の設定トグル変更
        /// </summary>
        public void ChangeToggle()
        {
            GyroControl.UseARWorld = UseARToggle.isOn;
        }

        /// <summary>
        /// デバッグ用　画像座標決定方式の設定トグル変更
        /// </summary>
        public void ChangeToggle2()
        {
            bool value = VisibleARModel.isOn;
            MeshRenderer[] mrs = ARWorld.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mrs)
            {
                mr.enabled = value;
            }
        }

        virtual public void DebugDisp()
        {
            DebugItemPanel.SetActive(!DebugItemPanel.activeSelf);
        }


        virtual public void ClickPittari()
        {
            Silhouette.SetActive(false);
            Panel_Awase.GetComponent<FadeEffect>().StartFadeOut(1f);
            Panel_Awase.transform.Find("Button").GetComponent<Button>().enabled = false;

            //Panel_Kurabe.transform.Find("Button").GetComponent<Button>().enabled = false;

            FirstKurabe.ClickButton();

            FadeEffect panel_KurabeFade = Panel_Kurabe.GetComponent<FadeEffect>();
            panel_KurabeFade.PreTime = 1f;
            panel_KurabeFade.EndTime = 1f;
            panel_KurabeFade.PreFadeInStart();
            //ARWorld.SetActive(true);
        }

        //IEnumerator CoLoadScene(string scene, float fadeTime)
        //{
        //    yield return new WaitForSeconds(1.0f);
        //}

        protected KurabeButton lastKurabeButton;
        internal bool ShowVideoClip(KurabeButton kurabeBtn)
        {
            if (videoPlayer[0].isPlaying)
            {
                DebugManager.PutLog("動画プレイ中によりキャンセル");
                return false;
            }

            ARWorld.SetActive(false);

            for (int i = 0; i < videoPlayer.Length; i++)
            {
                isVideoReady[i] = false;
                isVideoEnd[i] = false;
                videoRawImage[i].enabled = false;
            }

            videoPlayer[0].enabled = true;
            videoPlayer[0].clip = kurabeBtn.VideoClip;
            videoPlayer[0].Prepare();

            if (kurabeBtn.VideoClipLight != null)
            {
                videoPlayer[1].enabled = true;
                videoPlayer[1].clip = kurabeBtn.VideoClipLight;
                videoPlayer[1].Prepare();
            }
            else
            {
                videoPlayer[1].enabled = false;
                isVideoReady[1] = true;
                isVideoEnd[1] = true;
            }


            if (lastKurabeButton)
            {
                Debug.Log("lastKurabeButton:" + lastKurabeButton);

                ARKurabeUIDisp(lastKurabeButton,false);

                lastKurabeButton.ChangeNonActive();
            }
            Debug.Log("kurabeBtn:" + kurabeBtn);

            lastKurabeButton = kurabeBtn;

            return true;
        }

        private void ARKurabeUIDisp(KurabeButton kurabeBtn,bool isDisp)
        {
            ARWorld.SetActive(isDisp);
            ChangeToggle2();

            kurabeBtn.ARKurabeImageUI.Disp(isDisp);
        }
    }
}
