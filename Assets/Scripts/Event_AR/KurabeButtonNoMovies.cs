﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityEngine.Video;
using UnityEngine.Playables;
namespace ManabuDriveApp.ARKurabe
{
    public class KurabeButtonNoMovies : KurabeButton
    {
        override public void ClickButton()
        {
            if (((ARKurabeManagerNoMovies)ARKurabeManager.Instance).ShowStartingAnime(this) == false) return;

            //ボタン拡大アニメ
            this.GetComponent<Button>().interactable = false;
            //Animator anim = this.GetComponent<Animator>();
            //anim.enabled = true;
            //anim.Play("On",0,0f);
            //this.transform.Find("TextImage").GetComponent<Image>().color = ConstColor.GetColorFromHTMLString("#666666FF"); ;
        }
    }
}
