﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべっこ 疑似AR状態のラクダ
    /// タッチ・アニメの制御
    /// </summary>
    public class ImageUI_Rakuda : MonoBehaviour,IPointerClickHandler
    {
        /// <summary>
        /// ARくらべっこ ラクダ前面アニメ制御　への参照
        /// </summary>
        public Animator BigCamelAnime;

        private Animator thisAnime;
        private float yureWait;

        private enum enState { Non, Out, PlayBig,In };
        private enState State = enState.Non;

        void Start()
        {
            thisAnime = this.GetComponent<Animator>();
            yureWait = UnityEngine.Random.Range(3f,4f);
        }

        void OnEnable()
        {
            thisAnime = this.GetComponent<Animator>();
            thisAnime.enabled = false;
            BigCamelAnime.gameObject.SetActive(false);
        }

        void Update()
        {
            if (State == enState.Non)
            {
                yureWait -= Time.deltaTime;
                if (yureWait < 0f)
                {
                    //揺れアニメ開始
                    thisAnime.enabled = true;
                    thisAnime.Play("ImageUI_Rakuda_yure",0,0f);
                    SoundControl.PlaySound(11);
                    //次回揺れアニメ待ち時間決定
                    yureWait = UnityEngine.Random.Range(4f, 6f);
                }
            }
        }

        /// <summary>
        /// タッチ時処理
        /// 　退場アニメ開始
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            this.GetComponent<Image>().raycastTarget = false;
            SoundControl.PlaySound(12);
            State = enState.Out;
            thisAnime.enabled = true;
            thisAnime.Play("ImageUI_Rakuda_Out", 0, 0f);
        }

        /// <summary>
        /// 退場アニメ終了時に呼ばれる
        /// 　ラクダ前面アニメ再生開始
        /// </summary>
        public void OutAnimeEnd()
        {
            State = enState.PlayBig;
            BigCamelAnime.gameObject.SetActive(true);
            BigCamelAnime.Play("ImageUI_Camel", 0, 0f);
        }

        /// <summary>
        /// ラクダ前面アニメが終わった際にラクダ前面アニメから呼ばれる
        /// 　入場アニメを開始する
        /// </summary>
        internal void EndBigAnime()
        {
            State = enState.In;
            thisAnime.Play("ImageUI_Rakuda_In", 0, 0f);
        }

        /// <summary>
        /// 入場アニメ終了時に呼ばれる
        /// 　初期状態に戻す
        /// </summary>
        public void InAnimeEnd()
        {
            State = enState.Non;
            yureWait = UnityEngine.Random.Range(3f, 4f);
            this.GetComponent<Image>().raycastTarget = true;
        }


    }
}
