﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{

    /// <summary>
    /// ターゲットUIオブジェクトの座標・角度を、
    /// 3D空間上の本オブジェクトがカメラから見えている位置に合わせる
    /// </summary>
    public class ARMap : MonoBehaviour
    {
        public Transform ARCameraTran;
        public Transform ImageUIParent;

        private Camera ARCamera;
        private RectTransform UICanvasRect;
        private Camera uiCamera;

        private UIObjPosMarker[] markers;

        public void Init()
        {
            ARCamera = ARCameraTran.GetComponentInChildren<Camera>();
            Canvas uiCanvas = ImageUIParent.GetComponentInParent<Canvas>();
            UICanvasRect = uiCanvas.GetComponent<RectTransform>();
            uiCamera = uiCanvas.worldCamera;

            markers = this.GetComponentsInChildren<UIObjPosMarker>();
            foreach(UIObjPosMarker marker in markers)
            {
                marker.Init(ARCameraTran, ImageUIParent, ARCamera, UICanvasRect, uiCamera);
            }
        }

        public void Disp(bool value)
        {
            if (markers == null) Init();
            //ImageUIParent.gameObject.SetActive(true);
            this.gameObject.SetActive(value);
            if (value)
            {
                foreach (UIObjPosMarker marker in markers)
                {
                    marker.Show();
                }
            }
            else
            {
                foreach (UIObjPosMarker marker in markers)
                {
                    marker.Hide();
                }
            }
        }
        
    }
}
