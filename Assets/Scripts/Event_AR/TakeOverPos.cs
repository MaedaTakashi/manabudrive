﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{
    public class TakeOverPos : MonoBehaviour
    {
        public Transform SourceTran;
        
        public void GetAndSetPos()
        {
            //Debug.Log("GetAndSetPos " + this.gameObject.name);
            this.transform.position = SourceTran.position;
        }
    }
}
