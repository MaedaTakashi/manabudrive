﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;
using UnityEngine.Video;
using UnityEngine.Playables;

namespace ManabuDriveApp.ARKurabe
{
    /// <summary>
    /// ARくらべっこ　主処理
    /// </summary>
    public class ARKurabeManagerNoMovies : ARKurabeManager
    {

        [SerializeField]
        private Camera ARCamera;

        void Start()
        {
            RotRate = 1f;
            Panel_Kurabe.SetActive(false);
            Panel_Awase.SetActive(true);

            ARWorld.SetActive(false);
            debugDistInit();
            //            DebugDisp();

            //スクリーンサイズ比がipad（0.75:1）くらいではない場合、
            //AR空間のカメラのrectを調整する
            float rate = (float)Screen.width / (float)Screen.height;
            DebugManager.PutLog("Screen w:" + Screen.width + " h:" + Screen.height + " rate:" + rate);
            if ((rate < 0.7f)||(0.8f < rate))
            {
                float tmp = rate / 0.75f;
                ARCamera.rect = new Rect(0, (1f - tmp) / 2, 1f, tmp);
            }

        }


        void timerEnd(int rtn)
        {
            Debug.Log("タイマー終了");
            timerIsOn = false;
            GameManager.SaveData("Map");
            ScheneSwitcher.LoadScene("Map", 0.5f);
        }

        private bool playing = false;
        void Update()
        {
            if (playing)
            {
                //Debug.Log("PlayState.Playing:" + pd.state + " time:" + pd.time);
                //Debug.Log("pd.time:" + pd.time + " time:" + pd.playableAsset.duration);
                //if (pd.state != PlayState.Playing)
                if (pd.time >= pd.playableAsset.duration)
                {
                    //タイムラインがプレイ中ではなくなった

                    playing = false;
                    pd.gameObject.SetActive(false);
                    if (lastKurabeButton)
                    {
                        ARKurabeUIDisp(lastKurabeButton, true);
                    }
                    if (!timerIsOn)
                    {
                        LimitTimer.TimerStart(GameManager.Instance.GameSetting.EventLimitTime, timerEnd);
                        timerIsOn = true;
                    }
                }
            }
        }

        PlayableDirector pd;

        override public void ClickPittari()
        {
            SoundControl.PlaySound(10);

            Silhouette.GetComponent<Animator>().enabled = true;
            Panel_Awase.GetComponent<FadeEffect>().StartFadeOut(1f);
            Panel_Awase.transform.Find("Button").GetComponent<Button>().enabled = false;

            StartCoroutine(coWaitAndStartFirstKurabe());

            FadeEffect panel_KurabeFade = Panel_Kurabe.GetComponent<FadeEffect>();
            panel_KurabeFade.PreTime = 1f;
            panel_KurabeFade.EndTime = 1f;
            panel_KurabeFade.PreFadeInStart();
            //ARWorld.SetActive(true);
        }


        private IEnumerator coWaitAndStartFirstKurabe()
        {
            yield return new WaitForSeconds(1);
            Silhouette.SetActive(false);
            ((KurabeButtonNoMovies)FirstKurabe).ClickButton();
        }

        //private KurabeButtonNoMovies lastKurabeButton;
        internal bool ShowStartingAnime(KurabeButtonNoMovies kurabeBtn)
        {


            if (lastKurabeButton != null)
            {
                if (lastKurabeButton.ARKurabeImageUI.PlayableDirector.state == PlayState.Playing)
                {
                    DebugManager.PutLog("動画プレイ中によりキャンセル");
                    return false;
                }
            }

            ARWorld.SetActive(false);

            playing = true;
            pd= kurabeBtn.ARKurabeImageUI.PlayableDirector;
            pd.gameObject.SetActive(true);
            pd.Play();

            //Debug.Log("pd:" + pd);

            if (lastKurabeButton)
            {
                //Debug.Log("lastKurabeButton:" + lastKurabeButton);

                ARKurabeUIDisp(lastKurabeButton,false);

                lastKurabeButton.ChangeNonActive();
            }
            //Debug.Log("kurabeBtn:" + kurabeBtn);

            lastKurabeButton = kurabeBtn;

            return true;
        }


        private void ARKurabeUIDisp(KurabeButton kurabeBtn,bool isDisp)
        {
            ARWorld.SetActive(isDisp);

            kurabeBtn.ARKurabeImageUI.Disp(isDisp);

            if (UseARToggle.isOn)
            {
                ChangeToggle2();
            }
        }


//-----------デバッグ表示関係---------------------------------------------
        int debugDispState = 2;
        override public void DebugDisp()
        {
            switch (debugDispState)
            {
                case 0:
                    debugDispState = 1;
                    DebugItemPanel.SetActive(false);
                    break;
                case 1:
                    debugDispState = 2;
                    DebugItemPanel.SetActive(true);
                    DebugItemPanelChild1.SetActive(true);
                    DebugItemPanelChild2.SetActive(false);
                    break;
                case 2:
                    debugDispState = 0;
                    DebugItemPanel.SetActive(true);
                    DebugItemPanelChild1.SetActive(false);
                    DebugItemPanelChild2.SetActive(true);
                    break;
            }
        }

//---距離デバッグ補正計算---------------------------------------------------------------
        [SerializeField]
        private InputField debugDist1Input;
        [SerializeField]
        private InputField debugDist2Input;
        [SerializeField]
        private InputField debugDist3Input;

        class debugDistInfo
        {
            Transform transform;
            float zDiff;
            float xRate;

            public debugDistInfo(Transform tran,float baseZ)
            {
                this.transform = tran;
                zDiff = tran.localPosition.z - baseZ;
                xRate = tran.localPosition.x / tran.localPosition.z;
            }

            public void ConvPos(float newbaseZ)
            {
                if (transform == null) return;
                float z = newbaseZ + zDiff;
                transform.localPosition = new Vector3(z * xRate,0f,z);
            }
        }
        private debugDistInfo[] models1;
        private debugDistInfo[] models2;
        private float baseZ1;
        private float baseZ2;
        private float arcameraZ;
        private void debugDistInit()
        {
            if (models1 != null) return;

            models1 = new debugDistInfo[8];
            models2 = new debugDistInfo[1];
            Transform armap = ARWorld.transform.Find("ARMap_Pyramid");
            Transform models1base = armap.Find("Landmark");
            baseZ1 = models1base.localPosition.z;
            models1[0] = new debugDistInfo(models1base,baseZ1);
            models1[1] = new debugDistInfo(armap.Find("pyramid_6"), baseZ1);
            models1[2] = new debugDistInfo(armap.Find("pyramid_5"), baseZ1);
            models1[3] = new debugDistInfo(armap.Find("pyramid_4"), baseZ1);
            models1[4] = new debugDistInfo(armap.Find("pyramid_3"), baseZ1);
            models1[5] = new debugDistInfo(armap.Find("pyramid_2"), baseZ1);
            models1[6] = new debugDistInfo(armap.Find("pyramid_1"), baseZ1);
            models1[7] = new debugDistInfo(armap.Find("Ground"), baseZ1);

            Transform models2base = armap.Find("Rakuda");
            baseZ2 = models2base.localPosition.z;
            models2[0] = new debugDistInfo(models2base, baseZ2);

            debugDist1Input.text = baseZ1.ToString();
            debugDist2Input.text = baseZ2.ToString();

            Transform arcameraTran = GyroControl.transform.Find("ARCamera");
            arcameraZ = arcameraTran.localPosition.z;
            debugDist3Input.text = arcameraZ.ToString();
        }


        public void DebugChangeDist1()
        {
            Debug.Log("DebugChangeDist1");
            float tmpz;
            if(float.TryParse(debugDist1Input.text,out tmpz))
            {
                if (baseZ1 == tmpz) return;
                baseZ1 = tmpz;
                foreach (debugDistInfo info in models1)
                {
                    info.ConvPos(tmpz);
                }
                posReset();
            }
            else
            {
                debugDist1Input.text = baseZ1.ToString();
            }
        }

        public void DebugChangeDist2()
        {
            float tmpz;
            if (float.TryParse(debugDist2Input.text, out tmpz))
            {
                if (baseZ2 == tmpz) return;
                baseZ2 = tmpz;
                foreach (debugDistInfo info in models2)
                {
                    info.ConvPos(tmpz);
                }
                posReset();
            }
            else
            {
                debugDist2Input.text = baseZ2.ToString();
            }
        }

        public void DebugChangeDist3()
        {
            Debug.Log("DebugChangeDist1");
            float tmpFloat;
            if (float.TryParse(debugDist3Input.text, out tmpFloat))
            {
                if (arcameraZ == tmpFloat) return;
                arcameraZ = tmpFloat;

                Transform arcameraTran = GyroControl.transform.Find("ARCamera");
                arcameraTran.localPosition
                    = new Vector3(arcameraTran.localPosition.x, arcameraTran.localPosition.y, arcameraZ);

                posReset();
            }
            else
            {
                debugDist3Input.text = arcameraZ.ToString();
            }
        }

        private void posReset()
        {
            //ARWorld.SetActive(false);
            //lastKurabeButton.ARKurabeImageUI.Disp(false);
            //lastKurabeButton.ARMap.Disp(false);

            //ARWorld.SetActive(true);
            GyroControl.GyroInit();
            lastKurabeButton.ARKurabeImageUI.Disp(true);
            //lastKurabeButton.ARMap.Disp(true);
        }

        //public void DebugDistReset()
        //{
        //    debugDist1Input.text = "340";
        //    debugDist2Input.text = "25";
        //}

    }
}
