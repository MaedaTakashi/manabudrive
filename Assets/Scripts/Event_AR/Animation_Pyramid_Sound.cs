﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{
    public class Animation_Pyramid_Sound : MonoBehaviour
    {
        private Coroutine coroutine;

        void OnEnable()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
            coroutine = StartCoroutine(coStart());
        }

        IEnumerator coStart()
        {
            yield return new WaitForSeconds(0.2f);
            SoundControl.PlaySound(15, 3);
            yield return new WaitForSeconds(0.8f);
            SoundControl.PlaySound(3, 1);
            yield return new WaitForSeconds(1.2f);
            SoundControl.PlaySound(4, 2);
        }
    }
}
