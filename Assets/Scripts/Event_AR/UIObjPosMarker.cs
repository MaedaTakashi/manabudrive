﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.ARKurabe
{

    /// <summary>
    /// ターゲットUIオブジェクトの座標・角度を、
    /// 3D空間上の本オブジェクトがカメラから見えている位置に合わせる
    /// </summary>
    public class UIObjPosMarker : MonoBehaviour
    {
        private GameObject _TargetUIObj;

        private Camera _ARCamera;
        private Transform _ARCameraTran;
        private RectTransform _UICanvasRect;
        private Camera _UICamera;

        private RectTransform _TargetRectTran;
        //private Vector2 posDiff;

        //ターゲットの初期状態
        private Vector2 targetInitPos;
        private Vector2 targetInitPivot;
        


        public void Init(Transform arCameraTran, Transform imageUIParent, Camera arCamera, RectTransform uiCanvasRect, Camera uiCamera)
        {
            this._ARCameraTran = arCameraTran;
            this._ARCamera = arCamera;
            this._UICanvasRect = uiCanvasRect;
            this._UICamera = uiCamera;

            //ターゲットはimageUIParent下の自分と同名オブジェクトとする
            _TargetUIObj = imageUIParent.Find(this.gameObject.name).gameObject;
            //ターゲットの初期状態記憶
            _TargetRectTran = _TargetUIObj.GetComponent<RectTransform>();
            targetInitPos = _TargetRectTran.anchoredPosition;
            targetInitPivot = _TargetRectTran.pivot;
        }

        public void Show()
        {
            _TargetRectTran.pivot = targetInitPivot;
            _TargetRectTran.anchoredPosition = targetInitPos;

            this.gameObject.SetActive(true);
            _TargetUIObj.SetActive(true);

            //ターゲットのpivotを3Dオブジェクトが見えている位置に補正
            Vector2 tmpSize = _TargetRectTran.sizeDelta;
            Vector2 pos = getUICameraPos();
            Vector2 posDiff = targetInitPos - pos;
            Vector2 tmpPivot = new Vector2(posDiff.x / tmpSize.x, posDiff.y / tmpSize.y);
            _TargetRectTran.pivot = targetInitPivot - tmpPivot;
            //Debug.Log(gameObject.name + " size:" + tmpSize + " UIPos:" + targetInitPos + " 3Dpos:" + pos + " diff:" + posDiff + " pivot:" + tmpPivot);

            _TargetRectTran.anchoredPosition = pos;
            //if (gameObject.name == "Pyramid3")
            //{
            //    Debug.Log("Update Pyramid3:" + pos);
            //}
        }

        void Update()
        {
            if (_TargetRectTran)
            {
                Vector2 pos = getUICameraPos();
                //_TargetRectTran.anchoredPosition = pos + posDiff;
                _TargetRectTran.anchoredPosition = pos;
                _TargetRectTran.rotation = Quaternion.Euler(0f,0f,_ARCameraTran.rotation.eulerAngles.z * -1f);

                //if (gameObject.name == "Pyramid3")
                //{
                //    Debug.Log("Update Pyramid3:" + pos);
                //}
            }
        }

        private Vector2 getUICameraPos()
        {
            Vector3 screenPos = _ARCamera.WorldToScreenPoint(this.transform.position);
            var pos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_UICanvasRect, screenPos, _UICamera, out pos);
            return pos;
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
            if (_TargetUIObj)
            {
                //ターゲットを初期状態に戻す
                _TargetRectTran.pivot = targetInitPivot;
                _TargetRectTran.anchoredPosition = targetInitPos;
                _TargetRectTran.rotation = Quaternion.identity;
                _TargetUIObj.SetActive(false);
            }
        }
    }
}
