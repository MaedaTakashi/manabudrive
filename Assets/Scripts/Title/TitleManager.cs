﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ManabuDriveApp.Common;

namespace ManabuDriveApp.Title
{
    public class TitleManager : MonoBehaviour
    {

        #region Singleton
        private static TitleManager instance;
        public static TitleManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (TitleManager)FindObjectOfType(typeof(TitleManager));

                    if (instance == null)
                    {
                        //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                    }
                }

                return instance;
            }
        }

        public void Awake()
        {
            if (this != Instance)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        #endregion Singleton

        GameObject mainButtonsGO;
        GameObject logoGO;
        RectTransform maskRectTran;


        void Start()
        {
            Transform maskTran = GameObject.Find("Canvas").transform.Find("Mask");
            mainButtonsGO = maskTran.Find("Button").gameObject;
            logoGO = maskTran.Find("Logo").gameObject;
            mainButtonsGO.SetActive(false);

            maskRectTran = maskTran.GetComponent<RectTransform>();

            RectTransform buttonRectTran = mainButtonsGO.GetComponent<RectTransform>();
			LeanTween.size(buttonRectTran, buttonRectTran.sizeDelta * 1.05f, 0.7f).setEase(LeanTweenType.easeInOutSine).setLoopPingPong();

            mainButtonsGO.SetActive(false);
            logoGO.SetActive(false);

            ShowMain(0.05f);

            SoundControl.PlaySound(0);
            //SoundControl.PlayBGM("Testbgm");
        }

        public void ShowMain(float waitTime)
        {
            StartCoroutine(coShowMain(waitTime));
        }

        /// <returns></returns>
        IEnumerator coShowMain(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);

            logoGO.GetComponent<FadeEffect>().StartFadeIn();

            yield return new WaitForSeconds(0.2f);
            mainButtonsGO.GetComponent<FadeEffect>().StartFadeIn();
        }

        public void ClickButton(int value)
        {
            switch (value)
            {
                case 0:
                    SoundControl.PlaySound(1);
                    mainButtonsGO.GetComponent<Button>().interactable = false;
                    StartCoroutine(coGotoOP());
                    break;
            }
        }

        IEnumerator coGotoOP()
        {
			LeanTween.size(maskRectTran, new Vector2(1f, 1f), 0.7f).setEase(LeanTweenType.easeInSine);

            yield return new WaitForSeconds(0.7f);
            SoundControl.PlaySound(1,1);
            GameManager.SaveData("Opening");
            ScheneSwitcher.LoadScene("Opening");
        }

    }
}
