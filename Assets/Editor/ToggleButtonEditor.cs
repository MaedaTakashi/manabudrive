﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

namespace ManabuDriveApp.Common
{
    [CustomEditor(typeof(ToggleButton))]
    public class ToggleButtonEditor : ButtonEditor
    {
        public override void OnInspectorGUI()
        {
            var bh = target as ToggleButton;
            bh.OffSprite = EditorGUILayout.ObjectField("OffSprite", bh.OffSprite, typeof(Sprite), false) as Sprite;
            bh.OnSprite = EditorGUILayout.ObjectField("OnSprite", bh.OnSprite, typeof(Sprite), false) as Sprite;
            bh.isOn = EditorGUILayout.Toggle("isOn", bh.isOn);

            base.OnInspectorGUI();
        }
    }
}