//
//  MNBNativePlugin.cpp
//  AudioTest
//
//  Created by Seiya Sasaki on 2017/12/25.
//  Copyright © 2017年 kayac. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>

extern "C" {
    
    /*
     @param   mode (int) / 0: build-in speaker / 1: Bluetooth or A2DP
     @return  boolean whether the operation succeeded or not
    */
    bool SwitchSpeakerRoute(int mode) {
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *err;

        if (mode == 0) {
            [session setCategory:AVAudioSessionCategoryPlayAndRecord
                     withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                           error:&err];
        } else {
            [session setCategory:AVAudioSessionCategoryPlayAndRecord
                     withOptions:(AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP)
                           error:&err];
        }
        
        [session setActive:YES error:&err];
        
        return err == nil;
    }
}
